from django.shortcuts import render
from django.views import generic
from .models import Book, Author, BookInstance, Genre


class BookListView(generic.ListView):
    model = Book
    paginate_by = 2


class BookDetailView(generic.DetailView):
    model = Book
    paginate_by = 2


class AuthorListView(generic.ListView):
    model = Author
    paginate_by = 2


class AuthorDetailView(generic.DetailView):
    model = Author
    paginate_by = 2


def index(request):
    """ Function for visualization home page of our site """
    # Generate "quantities"for some main objects
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()
    # Available books (status = 'a')
    num_instances_available = BookInstance.objects.filter(status__exact='a').count()
    num_authors = Author.objects.count()    # Method 'all()' is applied by default
    num_tech_genre = Genre.objects.filter(name__contains='Tech').count()

    # Drawing HTML template index.html with data inside
    # context variable 'context'
    return render(
        request,
        'index.html',
        context={
            'num_books': num_books,
            'num_instances': num_instances,
            'num_instances_available': num_instances_available,
            'num_authors': num_authors,
            'num_tech_genre': num_tech_genre
        }
    )
