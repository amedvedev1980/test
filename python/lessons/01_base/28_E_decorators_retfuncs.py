def get_func(func_name):
    def func_1(word):
        return word.upper()

    def func_2(word):
        return word.lower()

    if func_name == '1':
        return func_1
    else:
        return func_2


def out_func(func, arg):
    print(func(arg))


print(get_func('1')('upper'))
print(get_func('2')('LOWER'))
out_func(get_func('1'), 'upper 2')
out_func(get_func('2'), 'LOWER 2')
