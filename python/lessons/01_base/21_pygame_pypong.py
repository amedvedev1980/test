import pygame, os, time
from pygame.colordict import THECOLORS


class Ball(pygame.sprite.Sprite):
    def __init__(self, location, speed):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(folder, 'images/small_ball.png'))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speed = speed

    def move(self):
        global score, lives
        self.rect = self.rect.move(self.speed)
        if self.rect.left < 0 or self.rect.right > screen.get_width():
            self.speed[0] = - self.speed[0]
        if self.rect.top <= 0:
            self.speed[1] = - self.speed[1]
            score = score + 1


class Paddle(pygame.sprite.Sprite):
    def __init__(self, location=None):
        if location is None:
            location = [0, 0]
        pygame.sprite.Sprite.__init__(self)
        image_surface = pygame.Surface([100, 20])
        image_surface.fill(THECOLORS['black'])
        self.image = image_surface.convert(image_surface)
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location


folder = os.path.dirname(os.path.realpath(__file__))

pygame.init()
size = width, height = 640, 480
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()
speed = [10, 5]
ball = Ball([50, 50], speed)
ballGroup = pygame.sprite.Group(ball)
paddle = Paddle([270, 400])

score = 0
lives = 3

score_font = pygame.font.Font(None, 50)
score_pos = [10, 10]

pygame.time.delay(1000)

pygame.mixer.music.load(os.path.join(folder, 'audio/track1.ogg'))
pygame.mixer.music.set_volume(0.3)
pygame.mixer.music.play()

hit = pygame.mixer.Sound(os.path.join(folder, 'audio/kickball.wav'))

done = False
running = True
while running:
    clock.tick(30)
    screen.fill(THECOLORS['white'])
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEMOTION:
            paddle.rect.centerx = event.pos[0]

    if pygame.sprite.spritecollide(paddle, ballGroup, False):
        hit.play()
        ball.speed[1] = -ball.speed[1]
    ball.move()
    if not done:
        screen.blit(ball.image, ball.rect)
        screen.blit(paddle.image, paddle.rect)
        score_surf = score_font.render(str(score), 1, THECOLORS['black'])
        screen.blit(score_surf, score_pos)
        for i in range(lives):
            screen.blit(ball.image, [width - 40 * i, 20])
        pygame.display.flip()
    if ball.rect.top >= screen.get_rect().bottom:
        lives = lives - 1
        if lives == 0:
            done = True
        else:
            pygame.time.delay(2000)
            ball.rect.top, ball.rect.left = [50, 50]
pygame.quit()
