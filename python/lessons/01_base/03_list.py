names = []
cmd = ''
while cmd != 'exit':
    cmd = input('name: ')
    if cmd != 'exit':
        if cmd == 'list':
            print(names[:])
        elif cmd == 'multi':
            names.extend(names)
        elif cmd == 'insert':
            pos = int(input('position to insert?:'))
            k = input('name to insert?:')
            names.insert(pos, k)
        elif cmd == 'remove':
            k = input('name to remove?:')
            if k in names:
                names.remove(k)
            else:
                print('no name ' + k + ' in list')
        elif cmd == 'del':
            pos = int(input('position to del?:'))
            if pos < len(names):
                del names[pos]
            else:
                print('position greater than list size')
        elif cmd == 'pop':
            pos = input('position to pop?:')
            if pos == '':
                print(names.pop())
            else:
                print(names.pop(int(pos)))
        elif cmd == 'index':
            k = input('name?:')
            if k in names:
                print('index is ' + str(names.index(k)))
            else:
                print('no name ' + k + ' in list')
        elif cmd == 'sort':
            names.sort()
            print(names[:])
        elif cmd == 'reverse':
            names.reverse()  # Переворачивает список
            print(names[:])
        elif cmd == 'sort_desc':
            names.sort(reverse=True)
            print(names[:])
        elif cmd == 'sorted':
            print(sorted(names))
        else:
            if cmd != '':
                names.append(cmd)
