import sys
from PyQt5 import QtWidgets
import pyqt_start_form


class ExampleApp(QtWidgets.QMainWindow, pyqt_start_form.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        q = self.pushButton
        q.clicked.connect(self.button_clicked)

    def button_clicked(self):
        x = self.pushButton.x()
        y = self.pushButton.y()
        x += 50
        y += 50
        self.pushButton.move(x, y)


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ExampleApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
