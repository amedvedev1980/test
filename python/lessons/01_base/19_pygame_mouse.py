import pygame, os
from pygame.colordict import THECOLORS

class Ball(pygame.sprite.Sprite):
    def __init__(self, location, speed):
        pygame.sprite.Sprite.__init__(self)
        folder = os.path.dirname(os.path.realpath(__file__))
        self.image = pygame.image.load(os.path.join(folder, 'images/ball.png'))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speed = speed

    def move(self):
        self.rect = self.rect.move(self.speed)
        if self.rect.left < 0 or self.rect.right > width or \
                self.rect.right >= screen.get_rect().right:
            self.speed[0] = -self.speed[0]
        newpos = self.rect.move(self.speed)
        self.rect = newpos

size = width, height = 640, 480
screen = pygame.display.set_mode(size)
#background = pygame.Surface(screen.get_size())
background = pygame.Surface(size)
background.fill(THECOLORS['white'])
#screen.fill(THECOLORS['white'])

clock = pygame.time.Clock()

ball = Ball([50, 50], [10, 0])

held_down = False
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            frame_rate = clock.get_fps()
            print('frame rate = ', frame_rate)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            held_down = True
        elif event.type == pygame.MOUSEBUTTONUP:
            held_down = False
        elif event.type == pygame.MOUSEMOTION:
            if held_down:
                ball.rect.center = event.pos
    clock.tick(20)
    screen.blit(background, (0, 0))
    ball.move()
    screen.blit(ball.image, ball.rect)
    pygame.display.flip()
pygame.quit()
