phones = {}
cmd = ''
while cmd != 'exit':
    cmd = input('cmd: ')
    if cmd != 'exit':
        if cmd == 'add':            # Добавляем в словарь телефон по имени
            name = input('name: ')
            phone = input('phone: ')
            phones[name] = phone
        elif cmd == 'print':
            print(phones)
        elif cmd == 'find':         # Ищем значение(телефон) в словаре по ключу(имени)
            name = input('name: ')
            if name in phones:
                print(phones[name])
            else:
                print('no name'+name+' in dict')
        elif cmd == 'keys':
            print(phones.keys())    # Выводим список ключей
        elif cmd == 'values':
            print(phones.values())  # Выводим список значений
        elif cmd == 'sort_name':    # Сортировка словаря по ключу
            for name in sorted(phones.keys()):
                print(name+'\t'+phones[name])
        elif cmd == 'del':          # Удаление по ключу
            name = input('name: ')
            if name in phones:
                del phones[name]
            else:
                print('no name '+name+' in dict')
        elif cmd == 'clear':        # Очистка всего словаря
            phones.clear()
