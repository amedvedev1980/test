class Card:
    def __init__(self, prof):
        self.paramX = 17
        self.paramY = 28
        self.prof = prof

    def format(self):
        if self.name != '':
            self.name = '[^'+self.name+']: '
        else:
            self.name = '[unknown]: '

    def __str__(self):
        msg = '====================\n'
        msg += 'paramX: ' + str(self.paramX) + '\n'
        msg += 'paramY: ' + str(self.paramY) + '\n'
        msg += 'prof: ' + self.prof
        return msg


card = Card('programmer')
card.name = 'Alex'
card.age = 38
card.address = 'Novosibirsk, Baltiyskaya str., 27'

card.format()
print("Name: ", card.name)
print("Age: ", str(card.age))
print("Address: ", card.address)
print(card)
