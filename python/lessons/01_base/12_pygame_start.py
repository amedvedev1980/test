import pygame
from pygame.color import THECOLORS
import random

pygame.init()
screen = pygame.display.set_mode([640, 480])

screen.fill([255, 255, 255])    # Заливаем окно белым цветом
# draw.circle ( цвет, X, Y, R, width) - нарисовать круг
# width = 0  - круг полностью залит цветом
pygame.draw.circle(screen, [255, 0, 0], [300, 100], 30, 0)
pygame.draw.circle(screen, THECOLORS['green'], [300, 200], 30, 0)
# Rect (left, top, width, height) - объект прямоугольник
# draw.rect ( ) - нарисовать прямоугольник
my_rect = pygame.Rect(300, 300, 100, 50)
pygame.draw.rect(screen, THECOLORS['cyan'], my_rect, 0)
print(THECOLORS.keys())     # Это итератор, а не список. Чтобы сделать из него список, надо list(iterator)
color_name = random.choice(list(THECOLORS.keys()))
print('COLOR NAME: ', color_name)
color = THECOLORS[color_name]
pygame.draw.rect(screen, color, [300, 350, 100, 50], 3)

pygame.display.flip()           # Применяем изменения из буфера

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
pygame.quit()
