import pygame, os
from pygame.colordict import THECOLORS

pygame.init()

screen = pygame.display.set_mode([640, 480])
screen.fill(THECOLORS['lightgreen'])

folder = os.path.dirname(os.path.realpath(__file__))
ball = pygame.image.load(os.path.join(folder, 'images/ball.png'))
#screen.blit(ball, [50, 50])
x = 50
y = 50
x_speed = 10
y_speed = 10
#pygame.display.flip()

runnig = True
while runnig:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            runnig = False
    pygame.time.delay(20)
    pygame.draw.rect(screen, THECOLORS['white'], [x, y, 40, 40], 0)
    x = x + x_speed
    y = y + y_speed
    if x > screen.get_width() - 40 or x < 0:
        x_speed = -x_speed
    if y > screen.get_height() - 40 or y < 0:
        y_speed = -y_speed
    screen.blit(ball, [x, y])
    pygame.display.flip()
pygame.quit()
