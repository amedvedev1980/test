class BaseObject:
    def __init__(self, name):
        self.name = name

    def move(self, x, y):
        # Перемещение объекта в координаты x, y
        pass


class Monster(BaseObject):
    def __init__(self, monster_name, dangerous, life):
        BaseObject.__init__(self, monster_name)
        self.dangerous = dangerous
        self.life = life

    def move(self, x, y):
        BaseObject.move(self, x, y)
        self.life = self.life - 1
        print('life\t', self.life)


m = Monster('cracozavr', 10, 100)
m.move(3, 4)
m.move(5, 4)
