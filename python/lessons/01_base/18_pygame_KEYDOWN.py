import pygame, os
from pygame.colordict import THECOLORS
from random import *


class Ball(pygame.sprite.Sprite):
    def __init__(self, location, speed):
        pygame.sprite.Sprite.__init__(self)
        folder = os.path.dirname(os.path.realpath(__file__))
        self.image = pygame.image.load(os.path.join(folder, 'images/ball.png'))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speed = speed

    def move(self):
        self.rect = self.rect.move(self.speed)
        if self.rect.left < 0 or self.rect.right > width or \
                self.rect.right >= screen.get_rect().right:
            self.speed[0] = -self.speed[0]
        #if self.rect.top < 0 or self.rect.bottom > height:
        #    self.speed[1] = -self.speed[1]
        newpos = self.rect.move(self.speed)
        self.rect = newpos

size = width, height = 640, 480
screen = pygame.display.set_mode(size)
#background = pygame.Surface(screen.get_size())
background = pygame.Surface(size)
background.fill(THECOLORS['white'])
#screen.fill(THECOLORS['white'])

clock = pygame.time.Clock()

ball = Ball([50, 50], [10, 0])

# для повтора при зажатой клавише нужно установить повтор нажатия
delay = 100
interval = 50
pygame.key.set_repeat(delay, interval)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            frame_rate = clock.get_fps()
            print('frame rate = ', frame_rate)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                ball.rect.top = ball.rect.top - 10
            elif event.key == pygame.K_DOWN:
                ball.rect.top = ball.rect.top + 10
    clock.tick(20)
    screen.blit(background, (0, 0))
    ball.move()
    screen.blit(ball.image, ball.rect)
    pygame.display.flip()
pygame.quit()
