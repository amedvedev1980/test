def init(name):
    print('Start function for initialization')
    print('Name: ', name)
    print('end')


def get_sum(price, num):
    global lastPrice
    lastPrice = price
    sum = price*num
    return sum


init('Alex')
s = get_sum(5, 3)
print('Sum: ', str(s))
print('Last price: ', str(lastPrice))
