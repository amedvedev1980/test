import pygame, os
from pygame.colordict import THECOLORS
from random import *

class Ball(pygame.sprite.Sprite):
    def __init__(self, location, speed):
        pygame.sprite.Sprite.__init__(self)
        folder = os.path.dirname(os.path.realpath(__file__))
        self.image = pygame.image.load(os.path.join(folder, 'images/ball.png'))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speed = speed

    def move(self):
        self.rect = self.rect.move(self.speed)
        if self.rect.left < 0 or self.rect.right > width:
            self.speed[0] = -self.speed[0]
        if self.rect.top < 0 or self.rect.bottom > height:
            self.speed[1] = -self.speed[1]


size = width, height = 640, 480
screen = pygame.display.set_mode(size)
screen.fill(THECOLORS['white'])

balls = []

for row in range(0, 5):
    for column in range(0, 7):
        location = [column * 90 + 10, row * 90 + 10]
        speed = [choice([-2, 2]), choice([-2, 2])]
        ball = Ball(location, speed)
        balls.append(ball)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    pygame.time.delay(20)
    screen.fill(THECOLORS['lightgreen'])
    for ball in balls:
        ball.move()
        screen.blit(ball.image, ball.rect)
    pygame.display.flip()
pygame.quit()

