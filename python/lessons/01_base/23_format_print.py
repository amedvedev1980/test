print("Привет", "мир")
print("Пишем"+"слитно")
print("Перевод\nстроки")
print("Tabbed\tcolumns")
print("12345678\ttext")
age = 38
name = 'Саша'
temp = 36.6
tax = 100000000
print("Мне %i лет" % age)
print("Мое имя %s" % name)
print("Температура %.1f по цельсию" % temp)
print("Все параметры: %i, %s, %.1f" % (age, name, temp))
print("format Параметры: {0:d}, {1:s}, {2:.1f} tax: {3:n}" . format(age, name, temp, tax))
names = 'Alex, Vika, Vera, Olga, Sasha'
arr = names.split(',')
print(arr)
print(';'.join(arr))
recipe = '''
    Шоколадный торт
    Ингедиенты:
    2 яйца
    75 г муки
    1 ч. л. пекарского порошка
    500 г шоколада
    Рецепт:
    Разогрейте духовку до 180 градусов
    Смешайте все ингедиенты
    Выпекайте 30 минут
'''

lines = recipe.split('\n    ')
recipe_out = False
for line in lines:
    if line.startswith('Рецепт') or recipe_out:
        recipe_out = True
        if 'духовку' in line:
            pos = line.index('духовку')
            print(line[:pos]+line[pos:].upper())
        elif 'минут' in line:
            print('ВНИМАНИЕ!', line)
        else:
            print(line)
