import pygame, os
from pygame.colordict import THECOLORS
from random import *


class Ball(pygame.sprite.Sprite):
    def __init__(self, location, speed):
        pygame.sprite.Sprite.__init__(self)
        folder = os.path.dirname(os.path.realpath(__file__))
        self.image = pygame.image.load(os.path.join(folder, 'images/ball.png'))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speed = speed

    def move(self):
        self.rect = self.rect.move(self.speed)
        if self.rect.left < 0 or self.rect.right > width:
            self.speed[0] = -self.speed[0]
        if self.rect.top < 0 or self.rect.bottom > height:
            self.speed[1] = -self.speed[1]


def animate(group):
    screen.fill(THECOLORS['white'])
    for ball in group:
        ball.move()
    for ball in group:
        group.remove(ball)  # Удаляем спрайт из группы
        if pygame.sprite.spritecollide(ball, group, False):  # Проверка столкновения спрайта и группы
            ball.speed[0] = -ball.speed[0]
            ball.speed[1] = -ball.speed[1]
        group.add(ball)     # Возвращаем мяч в группу
        #ball.move()
        screen.blit(ball.image, ball.rect)
    pygame.display.flip()

size = width, height = 640, 480
screen = pygame.display.set_mode(size)
screen.fill(THECOLORS['white'])

clock = pygame.time.Clock()

group = pygame.sprite.Group()

for row in range(0, 4):
    for column in range(0, 4):
        location = [column * 90 + 10, row * 90 + 10]
        speed = [choice([-2, 2]), choice([-2, 2])]
        ball = Ball(location, speed)
        group.add(ball)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            frame_rate = clock.get_fps()
            print('frame rate = ', frame_rate)
    animate(group)
    # Теперь частоту кадров(ограниченную скоростью компьютера) контролирует функция clock.tick
    clock.tick(60)
pygame.quit()
# object_speed = current_speed * (desired fps / actual fps)
