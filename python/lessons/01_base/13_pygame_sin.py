import pygame
import math
from pygame.colordict import THECOLORS

pygame.init()
screen = pygame.display.set_mode([640, 480])
screen.fill(THECOLORS['white'])

plotPoints = []
for x in range(0, 640):
    y = int(math.sin(x/640.0 * 4 * math.pi) * 200 + 240)
    plotPoints.append([x, y])

pygame.draw.lines(screen, THECOLORS['black'], False, plotPoints, 2)

for x in range(0, 640, 10):
    for y in range(0, 480, 10):
        screen.set_at([x, y], THECOLORS['red'])

pygame.display.flip()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
pygame.quit()
