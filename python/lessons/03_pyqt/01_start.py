from PyQt5 import QtWidgets
import sys


app = QtWidgets.QApplication(sys.argv)

window = QtWidgets.QWidget()
window.setWindowTitle('PyQT start')
window.resize(400, 300)
label1 = QtWidgets.QLabel('<center><b>Hello world</b></center>')
label2 = QtWidgets.QLabel('<center><i>some text</i></center>')
btnQuit = QtWidgets.QPushButton("&Close win")

vbox = QtWidgets.QVBoxLayout()
vbox.addWidget(label1)
vbox.addWidget(label2)
vbox.addWidget(btnQuit)

window.setLayout(vbox)
btnQuit.clicked.connect(app.quit)
window.show()
v = app.exec_()
print('exit!')
sys.exit(v)
