from PyQt5 import QtWidgets
import sys
import time
'''
    В этом примере длительная операция разбита на одинаковые этапы,
    после выполнения каждого из которых выполняется выход в основной
    цикл приложения. Теперь при перекрытии окна и повторном его 
    отображении окно будет перерисовано - таким образом, приложение
    по-прежнему будет взаимодействовать с системой, хотя и с некоторой
    задержкой 
'''


def on_clicked():
    btn.setDisabled(True)
    for i in range(1, 21):
        QtWidgets.qApp.processEvents()
        time.sleep(1)
        print('work step', i)
    btn.setDisabled(False)


app = QtWidgets.QApplication(sys.argv)
btn = QtWidgets.QPushButton('Запустить процесс')
btn.resize(200, 40)
btn.clicked.connect(on_clicked)
btn.show()
sys.exit(app.exec_())