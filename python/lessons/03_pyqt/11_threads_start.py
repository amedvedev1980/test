# page 418
from PyQt5 import QtCore, QtWidgets


class MyThread(QtCore.QThread):
    mysignal = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

    def run(self):
        for i in range(1, 21):
            '''
                Обратите внимание, что для имитации длительного процесса мы использовали статический метод sleep()
                из класса QThread, а не функцию sleep() из модуля time.
                Вообще, приостановить выполнение потока позволяют следующие статические методы QThread:
                1) sleeo(sec) - продолжительность в секунжах
                2) msleep(millsec) - в миллисекундах
                3) usleep(micro) - в микросекундах
                Еще один полезный статичный метод - yieldCurrentThread() - немедленно приостанавливает выполнение 
                текущего потока и передает управление следующему ожидающему выполнения потоку, если таковой есть.
            '''
            self.sleep(3)
            self.mysignal.emit('work step %s' % i)


class MyWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.label = QtWidgets.QLabel('Нажмите кнопку для запуска потока')
        self.label.setAlignment(QtCore.Qt.AlignHCenter)
        self.btn = QtWidgets.QPushButton('Запустить процесс')
        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.btn)
        self.setLayout(self.vbox)

        self.mythread = MyThread()
        self.btn.clicked.connect(self.on_clicked)
        self.mythread.started.connect(self.on_started)
        self.mythread.finished.connect(self.on_finished)
        # С помощью атрибута QueuedConnection указывается, что сигнал помещается в очередь обработки событий,
        # и обработчик должен выполняться в потоке приемника сигнала, т.е. в GUI-потоке
        self.mythread.mysignal.connect(self.on_signal, QtCore.Qt.QueuedConnection)

    def on_clicked(self):
        self.btn.setDisabled(True)
        self.mythread.start()

    def on_started(self):
        self.label.setText('Вызван метод on_started()')

    def on_finished(self):
        self.label.setText('Вызван метод on_finished()')
        self.btn.setDisabled(False)

    def on_signal(self, s):
        self.label.setText(s)


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.setWindowTitle('Thread using')
    window.resize(300, 70)
    window.show()
    sys.exit(app.exec_())
