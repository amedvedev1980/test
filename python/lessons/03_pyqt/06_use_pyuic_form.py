# pyuic5 MyForm.ui -o MyForm.py

from PyQt5 import QtWidgets
import sys
import MyForm

app = QtWidgets.QApplication(sys.argv)
window = QtWidgets.QWidget()
ui = MyForm.Ui_MyForm()
ui.setupUi(window)
ui.btnQuit.clicked.connect(QtWidgets.qApp.quit)
window.show()
sys.exit(app.exec_())
