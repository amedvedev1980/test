from PyQt5 import QtCore

# QByteArray - массив байтов. Преобразуется в тип bytes
arr = QtCore.QByteArray(bytes('str', 'cp1251'))
print(arr)
print(bytes(arr))

# out:
# b'str'
# b'str'

# QVariant - может хранить данные любого типа. Создать объект
# этого класса можно вызовом конструктора, передав ему нужное
# значение. А чтобы преобразовать данные, хранящиеся в объекте
# класса QVariant, в тип данных Python, нужно вызвать метод value()

n1 = QtCore.QVariant(10)
print(n1)
print(n1.value())
print(n1.typeName())
n2 = QtCore.QVariant()
print(n2.value())
print(n2.typeName())

# out:
# <PyQt5.QtCore.QVariant object at 0x7f6fcd91ec18>
# 10
# int
# None
# None

# QDate - значение даты
d = QtCore.QDate(1980, 5, 24)
print(d)
print(d.toPyDate())
print(d.currentDate().toPyDate())

# out:
# PyQt5.QtCore.QDate(1980, 5, 24)
# 1980-05-24
# 2019-01-15

# QTime - значение времени
# QDateTime - значение даты и времени
# QTextStream - текстовый поток
# QUrl - URL-адрес
# и некоторые другие



