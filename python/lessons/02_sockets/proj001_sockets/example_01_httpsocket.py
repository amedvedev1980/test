import socket
import time


def send_answer(conn, status='200 OK', typ='text/plain; charset=utf-8', data=''):
    data = data.encode('utf-8')
    conn.send(b'HTTP/1.1 '+status.encode('utf-8')+b'\r\n')
    conn.send(b'Server: simplehttp\r\n')
    conn.send(b'Connection: close\r\n')
    conn.send(b'Content-Type: '+typ.encode('utf-8')+b'\r\n')
    conn.send(b'Content-Length: '+bytes(len(data))+b'\r\n')
    conn.send(b'\r\n')  # После пустой строки в HTTP начинаются данные
    conn.send(data)


def parse(conn, addr):  # Обработка соединения в отдельной функции
    data = b''

    while not b'\r\n' in data:  # ждем первую строку
        tmp = conn.recv(1024)
        if not tmp:     # сокет закрыли, пустой объект
            break
        else:
            data += tmp
    if not data:    # данные не пришли
        return      # ничего не обрабатываем
    udata = data.decode('utf-8')
    # берем только первую строку
    udata = udata.split('\r\n', 1)[0]
    # разбиваем по пробелам нашу строку
    method, address, protocol = udata.split(' ', 2)

    if method != 'GET' or address != "/time.html":
        send_answer(conn, '404 Not Found', data='Не найдено')
        return

    answer = '''<!DOCTYPE html>'''
    answer += '''<html><head><title>Время</title></head><body><h1>'''
    answer += time.strftime('%H:%M:%S %d.%m.%Y')
    answer += '''</h1></body></html>'''
    send_answer(conn, typ='text/html; charset=utf-8', data=answer)


print("start OK")

sock = socket.socket()
print("socked created")
sock.bind(('', 14900))

''' Максимальное число соединений, которые будут находиться
    в очереди соединений до вызова функции accept, 
    максимальное число активных соединений этим не ограничивается:'''
sock.listen(10)

try:
    while 1:
        conn, addr = sock.accept()
        print('New connection from '+addr[0])
        try:
            parse(conn, addr)
        except:
            send_answer(conn, "500 Internal Server Error", data='Ошибка')
        finally:
            # сокет всегда закрываем корректно
            conn.close()
finally:
    sock.close()
