import socket


def parse(client, data):
    udata = b''
    tmp = client.recv(1024)
    #while tmp:
    udata += tmp
    #tmp = client.recv(1024)
    print(udata.decode('utf-8'))


conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.bind(('', 14903))
conn.listen(10)
conn.setblocking(0)

try:
    while 1:
        client = None
        try:
            client, addr = conn.accept()
        except socket.error:
            pass
        else:
            client.setblocking(0)
            parse(client, addr)
            client.send(b'Why?')
        finally:
            if client is not None:
                client.close()
except:
    conn.close()
    print('except')
finally:
    conn.close()
    print('finally')
print('server end')
