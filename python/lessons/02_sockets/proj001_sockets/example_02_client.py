import socket

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect(('127.0.0.1', 14903))
conn.setblocking(0)

try:
    conn.send(b'Hello!\n')

    while 1:
        data = b''
        try:
            tmp = conn.recv(1024)
        except socket.error:
            pass
        else:
            print(tmp.decode('utf-8'))
            break
except:
    print('except')
finally:
    conn.close()
print('client end')
