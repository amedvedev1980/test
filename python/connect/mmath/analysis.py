import copy
from numpy import rot90
from mmath.rotate import *
from itertools import groupby


def mark_hvecs(ctype, size, array):
    """
    поиск векторов по горизонтали
    :param ctype: тип элементов вектора(число)
    :param size: размер вектора
    :param array: массив содержащий вектора
    :return: новый массив, список кортежей начальных координат найденных векторов [(x1, y1) ... (xn, yn)]
    """
    vecs = []
    n_array = None
    for i in range(len(array)):
        pos = 0
        for key, group in groupby(array[i]):
            t = list(group)
            tlen = len(t)
            if tlen == size and t[0] == ctype:  # найдена ненулевая горизонтальная линия с размером size
                n_array = copy.deepcopy(array)
                for d in range(pos, pos + tlen):
                    if n_array[i][d] < 3:  # повторно не маркируем
                        n_array[i][d] = n_array[i][d] + 2
                vecs.append((pos, i))  # x,y
                break
            pos += tlen

    return n_array, vecs


def vertical(func):
    def wrapped(ctype, size, array):
        return func(ctype, size, rot90(array),
                    lambda x, y: tuple([len(array) - y - 1, x]),
                    lambda x, y: (x, y - 1) if y > 0 else None,
                    lambda x, y: (x, y + size) if y + size < len(array) else None)

    return wrapped


def diagonal_slash(func):
    def wrapped(ctype, size, array):
        return func(ctype, size, rot45(array),
                    lambda x, y: tuple([x, y - x]),
                    lambda x, y: (x - 1, y + 1) if x > 0 and y + 1 < len(array) else None,
                    lambda x, y: (x + size, y - size) if x + size < len(array) and y - size >= 0 else None)

    return wrapped


def diagonal_back_slash(func):
    def wrapped(ctype, size, array):
        return func(ctype, size, rrot45(array),
                    lambda x, y: tuple([x, x + y - len(array) + 1]),
                    lambda x, y: (x - 1, y - 1) if x > 0 and y > 0 else None,
                    lambda x, y: (x + size, y + size) if x + size < len(array) and y + size < len(array) else None)

    return wrapped


def find_vecs_horiz(ctype, size, array, tfunc=None, left_top=None, right_bottom=None):
    """
    поиск векторов по горизонтали
    :param ctype: тип элементов вектора(число)
    :param size: размер вектора
    :param array: массив содержащий вектора
    :return: список кортежей начальных координат найденных векторов [(x1, y1) ... (xn, yn)]
    """
    vecs = []
    # ищем вектора по горизонтали
    m_array, p_vecs = mark_hvecs(ctype, size, array)
    for vec in p_vecs:
        if tfunc is not None:
            vec = tfunc(*vec)
            v0 = left_top(*vec)
            v1 = right_bottom(*vec)
        else:
            x, y = vec
            v0 = (x - 1, y) if x > 0 else None
            v1 = (x + size, y) if x + size < len(array) else None
        vecs.append(tuple([*vec, v0, v1]))

    return vecs


@vertical
def find_vecs_vert(ctype, size, array, tfunc, top, bottom):
    """
    поиск векторов по вертикалям
    :param ctype: тип элементов вектора(число)
    :param size: размер вектора
    :param array: массив содержащий вектора
    :return: список кортежей начальных координат найденных векторов [(x1, y1) ... (xn, yn)]
    """
    return find_vecs_horiz(ctype, size, array, tfunc, top, bottom)


@diagonal_slash
def find_vecs_diag_slash(ctype, size, array, tfunc, left, right):
    """
    поиск векторов по диагоналям: '/'
    :param ctype: тип элементов вектора(число)
    :param size: размер вектора
    :param array: массив содержащий вектора
    :return: список кортежей начальных координат найденных векторов [(x1, y1) ... (xn, yn)]
    """
    return find_vecs_horiz(ctype, size, array, tfunc, left, right)


@diagonal_back_slash
def find_vecs_diag_bslash(ctype, size, array, tfunc, left, right):
    """
    поиск векторов по диагоналям: '\'
    :param ctype: тип элементов вектора(число)
    :param size: размер вектора
    :param array: массив содержащий вектора
    :return: список кортежей начальных координат найденных векторов [(x1, y1) ... (xn, yn)]
    """
    return find_vecs_horiz(ctype, size, array, tfunc, left, right)


def find_vecs(ctype, size, array):
    vecs = find_vecs_horiz(ctype, size, array) + \
           find_vecs_vert(ctype, size, array) + \
           find_vecs_diag_slash(ctype, size, array) + \
           find_vecs_diag_bslash(ctype, size, array)

    return vecs


def get_cell(array, x0, y0, x1, y1, d):
    cell = None
    if x0 == x1:  # vertical
        ys = y0 - d if d < 0 else y1 + d
        if ys >= 0 and ys < len(array):
            cell = (x0, ys)
    elif y0 == y1:  # horizontal
        xs = x0 - d if d < 0 else x1 + d
        if xs >= 0 and xs < len(array):
            cell = (xs, y0)
    elif y0 > y1:  # slash
        xs = x0 - d if d < 0 else x1 + d
        ys = y0 + d if d < 0 else y1 - d
        if xs >= 0 and xs < len(array) and ys >= 0 and ys < len(array):
            cell = (xs, ys)
    elif y0 < y1:  # bslash
        xs = x0 - d if d < 0 else x1 + d
        ys = y0 - d if d < 0 else y1 + d
        if xs >= 0 and xs < len(array) and ys >= 0 and ys < len(array):
            cell = (xs, ys)
    if cell is not None:
        x, y = cell
        if array[y][x] != 0:
            cell = None
    return cell


def test_cells(array, x0, y0, x1, y1, num):
    result = True
    ds = range(num) if num > 0 else range(-1, num - 1, -1)
    for d in ds:
        cell = get_cell(array, x0, y0, x1, y1, d)
        if cell is None:
            result = False
            break
    return result


'''
    Анализ:
        1. Ищем опасные пятерки: любая пятерка, которую можно дополнить до 6ти
        Если найденных пятерок больше одной, то закрываем две пятерки по одному на каждую
        Если найденная пятерка - одна, блокируем ее одним или двумя

        2. Ищем опасные четверки: любая четверка, которую можно дополнить до 6ти
        Если найденных четверок больше одной, в приоритете четверка свободная с обоих концов
        ...

        3. Ищем опасные тройки: любая тройка, которую можно дополнить до 6ти
        Если найденных троек больше одной, блокируем две
        Если одна, то один конец блокируем, а второй нет

        4. Ищем опасные парные двойки: те которые образуют галку Г 
        Ставим одну на каждую Г, перекрывая одно из направлений

'''

'''
опасные 6ки:
XXX_XXX

опасные 5ки:
XX_XX_X
X_XX_XX
XX_X_XX
опасные 4ки:
_XX_XX_
X_X_XX
XX_X_X



XX_X_X_X_XX

X_X_X_X
'''
