import queue
import defs
from PyQt5.QtCore import QThread, pyqtSignal


class Session(QThread):

    def __init__(self, tboard, players):
        super().__init__()
        self.tboard = tboard
        player1, player2 = players

        self.cur_player = None
        self.myq = queue.Queue()

        if player1.figure == defs.figure['cross']:
            player1.moves = 1
            self.myq.put(player1)
            self.myq.put(player2)
        else:
            player2.moves = 1
            self.myq.put(player2)
            self.myq.put(player1)

        self.is_end = False
        self.total = 0

    def __del__(self):
        print('session destroyed')

    def reset(self):
        if self.cur_player is not None:
            self.cur_player.end_moves.disconnect()
            self.cur_player = None
        self.myq = None

    def run(self):
        if not self.tboard.end_game:
            if self.myq.empty() != True:
                self.cur_player = self.myq.get()
                self.cur_player.end_moves.connect(self.on_end_moves)
                self.cur_player.start()  # передали ход игроку

    def on_end_moves(self):
        if not self.tboard.end_game:
            # игрок или АИ сделал свои ходы
            self.cur_player.end_moves.disconnect()
            self.myq.put(self.cur_player)   # ставим его обратно в очередь
            self.start()    # перезапускаем себя
