from itertools import groupby
import copy


def rot45(array):
    rot = []
    alen = len(array)
    for i in range(alen * 2 - 1):
        rot.append([0] * alen)
    for i in range(alen):
        for j in range(alen):
            rot[int(i + j)][j] = array[i][j]
    return rot


def unrot45(array):
    rot = []
    alen = len(array[0])
    for i in range(alen):
        rot.append([0] * alen)
        for j in range(alen):
            rot[i][j] = array[int(i+j)][j]
    return rot


def rrot45(array):
    rot = []
    alen = len(array)
    for i in range(alen * 2 - 1):
        rot.append([0] * alen)
    for i in range(alen):
        for j in range(alen, 0, -1):
            r = alen - j
            rot[int(i+j-1)][r] = array[i][r]
    return rot


def unrrot45(array):
    rot = []
    alen = len(array[0])
    for i in range(alen):
        rot.append([0] * alen)
        for j in range(alen):
            r = alen - j - 1
            rot[i][j] = array[int(r+i)][j]
    return rot


def check_horiz(array):
    is_win = False
    n_array = None
    for i in range(len(array)):
        pos = 0
        for key, group in groupby(array[i]):
            t = list(group)
            tlen = len(t)
            if tlen > 5 and t[0] != 0:  # найдена ненулевая горизонтальная линия >5
                is_win = True
                n_array = copy.deepcopy(array)
                for d in range(pos, pos+tlen):
                    if n_array[i][d] < 3:     # проверка для отладки, в продакшене она не нужна, потому как второго захода не будет
                        n_array[i][d] = n_array[i][d] + 2
                break
            pos += tlen
        if is_win:
            break
    return is_win, n_array
