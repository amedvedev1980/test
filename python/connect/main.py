#!/usr/bin/python3
# @copyright Alexander Medvedev, 2019
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QHBoxLayout, QWidget, QMenuBar, QMessageBox
from gui.board import Board
from gui.info import Info
from mmath.session import Session
import functools
import time
import random
from mnet.echoserver import EchoServer
from dialogs.dlg_local_net import DlgLocalNet
from players.human import Human
from players.ai import Ai


class Main(QMainWindow):

    def __init__(self):
        super().__init__()
        self.ai = None
        self.sess = None
        self.tboard = Board(self)
        self.info = []
        self.info.append(Info(self, 'Anonymous', 'anonymous.png'))
        self.info.append(Info(self, 'AI', 'ai_face.png'))
        self.menu()
        self.central_widget()
        self.players = []
        self.myname = 'unknown' + str(random.randint(10, 99))

    def center(self):
        pass

    def menu(self):
        # set MenuBoard
        menu_bar = QMenuBar(self)
        self.setMenuBar(menu_bar)
        game_menu = menu_bar.addMenu('Game')
        game_menu.addAction('Single player', functools.partial(self.play, ('Human', 'Ai')))
        game_menu.addAction('Two players', functools.partial(self.play, ('Human', 'Human')))
        # game_menu.addAction('AI vs AI', functools.partial(self.play, ('Ai', 'Ai')))
        game_menu.addAction('Local network', self.local_net)

    def local_net(self):
        DlgLocalNet(self, self.myname)

    def central_widget(self):
        widget = QWidget(self)
        self.setCentralWidget(widget)
        widget.hide()
        box = QHBoxLayout(widget)
        box.addWidget(self.info[0])
        box.addWidget(self.tboard)
        box.addWidget(self.info[1])
        width = self.info[0].width + self.tboard.width + self.info[1].width
        height = self.tboard.height
        self.resize(width, height)
        self.show()

    def play(self, players):
        print(self.players)
        if self.sess is not None:
            self.tboard.end_game = True
            self.sess.reset()
            for player in self.players:
                player.reset()
            self.players = []
            self.sess = None
            time.sleep(1)
        print('=' * 40, 'START PLAY')
        i = 0
        for player in players:
            self.players.append(globals().get(player)(self.tboard, self.info[i], i + 1))
            self.tboard.pcnt += 1
            i += 1
        print(self.players)
        self.tboard.reset()
        self.centralWidget().show()

        self.sess = Session(self.tboard, self.players)
        self.sess.start()


if __name__ == '__main__':
    app = QApplication([])
    gomoku = Main()
    sys.exit(app.exec_())
