from PyQt5.QtWidgets import QWidget, QHBoxLayout, QTableWidget, QTableWidgetItem, \
    QPushButton
from PyQt5.QtCore import Qt
from mnet.mudpserver import MUdpServer
from mnet.mudprequest import MUdpRequest
from mnet.mudpclient import MUdpClient
import time


class DlgLocalNet(QWidget):
    def __init__(self, parent, myname):
        super().__init__(parent, Qt.Window)
        self.setWindowTitle('Local Area Network')
        self.resize(500, 400)
        self.move(300, 300)

        box = QHBoxLayout(self)
        self.table = QTableWidget(self)
        self.table.setColumnCount(4)
        # self.table.setRowCount(2)
        self.table.setHorizontalHeaderLabels(['NAME', 'IP ADDRESS', 'STATUS', 'JOIN'])

        self.table.resizeColumnsToContents()
        box.addWidget(self.table)
        self.show()

        self.players = {}

        # стартуем сервер, на который будут приходить udp-запросы
        # self.udp_srv = MUdpServer('', 5007, MUdpRequest.Creator(self.process))
        self.udp_srv = MUdpServer('', 5007)
        self.udp_srv.start()
        self.udp_srv.in_data.connect(self.process)

        # отправляем броадкасты
        self.udp_cli = MUdpClient('192.168.0.255', 5007)
        self.my_ip = self.udp_cli.get_self_ip()
        print('My IP:', self.my_ip)
        self.udp_cli.broadcast(-1, 1, 'connect6|'+myname, 1)

    def process(self, in_data):
        data, ip = in_data
        if ip != self.my_ip:
            if data.startswith('connect6'):
                if ip not in self.players:
                    name = data.split('|')[1]
                    ind = self.add_player(name, ip, 'ready')
                    self.players[ip] = [ind, name, time.time()]
                else:
                    self.players[ip][2] = time.time()
        else:
            for ip in list(self.players):
                player = self.players[ip]
                if (time.time() - player[2]) > 3:     # больше трех секунд нет сигнала
                    self.table.removeRow(player[0])
                    self.table.resizeColumnsToContents()
                    self.players.pop(ip)

    def add_cell(self, row, col, val):
        if isinstance(val, QPushButton):
            self.table.setCellWidget(row, col, val)
        else:
            item = QTableWidgetItem(val)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.table.setItem(row, col, item)

    def add_player(self, name, ip, status):
        ind = self.table.rowCount()
        self.table.insertRow(ind)
        self.add_cell(ind, 0, name)
        self.add_cell(ind, 1, ip)
        self.add_cell(ind, 2, status)
        button = QPushButton('join', self)
        self.table.setCellWidget(ind, 3, button)
        # self.add_cell(ind, 3, btn)
        self.table.resizeColumnsToContents()
        return ind

    def closeEvent(self, event):
        self.udp_cli.stop()
