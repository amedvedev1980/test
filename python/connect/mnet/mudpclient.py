import socket
import functools
import netifaces as ni
from PyQt5.QtCore import QThread


class MUdpClient(QThread):

    def __init__(self, ip, port):
        super().__init__()
        self.ip = ip
        self.port = port
        self.running = 0
        self.ready = True
        self.process = None

    def run(self):
        self.ready = False
        if self.process is not None:
            while self.running > 0 or self.running < 0:
                self.process()
                if self.running > 0:
                    self.running -= 1
        self.ready = True
        print('stop udpclient')

    def broadcast(self, num, delay, msg, timeout=None):
        print('start broadcast')
        if self.running > 0:
            self.running = 0
        while not self.ready:
            pass
        self.running = num
        self.process = functools.partial(self.do_broadcast, delay, msg, timeout)
        self.start()

    def do_broadcast(self, delay, msg, timeout):
        udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            udp_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            if timeout is not None:
                udp_sock.settimeout(timeout)
            udp_sock.sendto(msg.encode('utf-8'), (self.ip, self.port))
        finally:
            udp_sock.close()
        self.sleep(delay)

    def stop(self):
        self.running = 0
        while not self.ready:
            pass

    def get_self_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

    def get_self_ip2(self):
        interfaces = ni.interfaces()

        for iface in interfaces:
            print(iface)
        return socket.gethostbyname(socket.getfqdn())
