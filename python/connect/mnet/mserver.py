import time
import socket
import threading
import socketserver
from queue import LifoQueue


# класс наследованный от миксина(каждый ответ в отдельном потоке) и TCP сервера
# соотв. при создании на вход адрес и класс с обработчиком ответа
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


# класс обработки ответа
class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        # пришли данные
        data = self.request.recv(1024)
        # отправим их в очередь сообщений
        self.server.queue.put_nowait(data)


# очередь сообщений от сервера
class MServer(LifoQueue):
    def __init__(self, ip, port):
        super().__init__()
        self.addr = ip, port
        self.server = ThreadedTCPServer((ip, port), ThreadedTCPRequestHandler)
        self.server.queue = self
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server_thread.daemon = True

    def start_server(self):
        self.server_thread.start()

    def stop_server(self):
        self.server.shutdown()
        self.server.server_close()

    def loop(self):
        while True:
            time.sleep(1)
            while not self.empty():
                self.handle(self.get_nowait())

    def send(self, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(self.addr)
        try:
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

    def handle(self, message):
        ''' Prototype '''
        pass
