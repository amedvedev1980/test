import socketserver
from PyQt5.QtWidgets import QMessageBox


class EchoServer(socketserver.DatagramRequestHandler):
    def handle(self):
        data = self.request[0].strip().decode('utf-8')
        client_ip = self.client_address[0]
        if data.startswith('connect6'):
            print('connect6 server ping myself')
        return data
