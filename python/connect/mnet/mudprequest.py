import socketserver


class MUdpRequest(socketserver.DatagramRequestHandler):
    def __init__(self, request, client_address, server, process):
        self.process = process
        super().__init__(request, client_address, server)

    def handle(self):
        data = self.request[0].strip().decode('utf-8')
        client_ip = self.client_address[0]
        self.process(data, client_ip)

    @classmethod
    def Creator(cls, *args, **kwargs):
        def _HandlerCreator(request, client_address, server):
            cls(request, client_address, server, *args, **kwargs)
        return _HandlerCreator
