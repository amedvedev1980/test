import socketserver
from PyQt5.QtCore import QThread, pyqtSignal
from mnet.mudprequest import MUdpRequest


class MUdpServer(QThread):
    in_data = pyqtSignal(tuple)

    def __init__(self, ip, port):
        super().__init__()
        self._is_running = False
        self.addr = ip, port
        self.handler = MUdpRequest.Creator(self.process)
        self.server = None
        pass

    def run(self):
        self._is_running = True
        self.server = socketserver.UDPServer(self.addr, self.handler)
        self.server.serve_forever()
        while self._is_running:
            pass
        self.server.shutdown()
        pass

    def stop(self):
        self._is_running = False
        pass

    def process(self, data, ip):
        self.in_data.emit((data, ip))

