def test_find_vecs_horizontal():
    v = [
        [1, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 1],
        [0, 1, 1, 1]]
    result = find_vecs_horiz(1, 2, v)
    assert result == [(0, 0, None, (2, 0)), (1, 1, (0, 1), (3, 1)), (2, 2, (1, 2), None)]


def test_find_vecs_vertical():
    v = [
        [1, 1, 0, 1],
        [0, 1, 0, 1],
        [1, 0, 1, 1],
        [1, 0, 1, 1]]
    result = find_vecs_vert(1, 2, v)
    assert result == [(2, 2, (2, 1), None), (1, 0, None, (1, 2)), (0, 2, (0, 1), None)]


def test_find_vecs_diag_slash():
    v = [
        [0, 1, 0, 0],
        [1, 0, 0, 1],
        [0, 1, 1, 1],
        [1, 0, 1, 0]]
    result = find_vecs_diag_slash(1, 2, v)
    assert result == [(0, 1, None, None), (0, 3, None, (2, 1)), (2, 2, (1, 3), None), (2, 3, None, None)]


def test_find_vecs_diag_bslash():
    v = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0]]
    result = find_vecs_diag_bslash(1, 2, v)
    assert result == [(1, 2, (0, 1), None)]

def test_get_cell_vertical():
    v = [
        [0, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0]]
    result = get_cell(v, 1, 1, 1, 2, 1)
    assert result == (1, 3)
'''
0(y), 4 - (x)1 - 1
y = nx
N-x-1 = ny


x = N-1-ny
y = 0
'''