import os
from PyQt5.QtWidgets import QFrame, QVBoxLayout, QLabel, QScrollArea
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap

root_dir = os.path.dirname(os.path.realpath(__file__))


class Info(QFrame):

    def __init__(self, parent, name, image_name):
        super().__init__(parent)
        self.width = 200

        vbox = QVBoxLayout(self)
        vbox.setAlignment(Qt.AlignTop)
        face = QPixmap(os.path.join(root_dir, 'images', image_name))
        face_label = QLabel()
        face_label.setPixmap(face)
        face_label.resize(face.width(), face.height())
        vbox.addWidget(face_label)
        vbox.addWidget(QLabel(name))

        self.logs = QFrame()

        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        scroll.setWidgetResizable(False)
        scroll.setWidget(self.logs)

        vbox.addWidget(scroll)
        self.show()
