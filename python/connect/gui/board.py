import os
from PyQt5.QtWidgets import QFrame, QPushButton, QGridLayout, QLabel
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QIcon
from functools import partial
from mmath import rotate
import numpy as np

root_dir = os.path.dirname(os.path.realpath(__file__))


class Board(QFrame):

    def __init__(self, parent):
        super().__init__(parent)
        self.ctype = 0
        self.moves = 0
        self.end_moves = False
        self.end_game = False
        self.pcnt = 0
        self.cnt = 0

        self.c_size = 20
        self.c_width, self.c_height = 19, 19
        self.width, self.height = (self.c_width + 1) * self.c_size, (self.c_height + 1) * self.c_size

        icon_cross = QIcon(os.path.join(root_dir, 'images', 'cross.png'))
        icon_zero = QIcon(os.path.join(root_dir, 'images', 'zero.png'))
        icon_cross_win = QIcon(os.path.join(root_dir, 'images', 'cross_win.png'))
        icon_zero_win = QIcon(os.path.join(root_dir, 'images', 'zero_win.png'))
        self.icons = {0: None, 1: icon_cross, 2: icon_zero, 3: icon_cross_win, 4: icon_zero_win}
        self.icons_size = QSize(20, 20)

        self.cvals = []
        self.cbtns = []
        for y in range(0, self.c_height):
            self.cvals.append([])
            self.cbtns.append([])
            for x in range(0, self.c_width):
                self.cvals[y].append(0)
                btn = QPushButton('')
                btn.setFixedSize(self.c_size, self.c_size)
                self.cbtns[y].append(btn)
        self.grid = None
        self.paint()
        self.refresh()

    def reset(self):
        self.ctype = 0
        self.moves = 0
        self.end_moves = False
        self.end_game = False
        self.cnt = 0
        for y in range(0, self.c_height):
            for x in range(0, self.c_width):
                self.cvals[y][x] = 0
        self.refresh()

    def paint(self):
        self.grid = QGridLayout()
        for y in range(0, self.c_height + 1):
            for x in range(0, self.c_width + 1):
                if x > 0 and y > 0:
                    cbtn = self.cbtns[y - 1][x - 1]
                    self.grid.addWidget(cbtn, y, x)
                    cbtn.clicked.connect(partial(self.on_click, x - 1, y - 1))
                else:
                    label = QLabel()
                    label.setFixedSize(self.c_size, self.c_size)
                    if x > 0 or y > 0:
                        label.setText(str(x if x > 0 else y))
                        label.setAlignment(Qt.AlignHCenter)
                    self.grid.addWidget(label, y, x)

        self.grid.setContentsMargins(0, 0, 0, 0)
        self.grid.setSpacing(0)
        self.setLayout(self.grid)

    def refresh(self):
        for y in range(0, self.c_height):
            for x in range(0, self.c_width):
                cbtn = self.cbtns[y][x]
                cval = self.cvals[y][x]
                if self.icons[cval] is not None:
                    cbtn.setIcon(self.icons[cval])
                    cbtn.setIconSize(self.icons_size)
                else:
                    cbtn.setIcon(QIcon())

    def on_click(self, x, y):
        if not self.end_game:
            if self.moves > 0 and self.cvals[y][x] == 0:
                self.cvals[y][x] = self.ctype
                btn = self.sender()
                btn.setIcon(self.icons[self.ctype])
                self.moves -= 1
                self.cnt += 1
                self.check_board()
            if self.moves == 0:
                self.end_moves = True

    def on_put(self, data):
        move, figure = data
        x, y = move
        self.cnt += 1
        self.cvals[y][x] = figure
        self.refresh()
        self.check_board()

    def do_moves(self, data):
        self.moves, self.ctype = data
        pass

    def check_board(self):  # проверка на выигрыш
        cvals = self.cvals

        while 1:
            is_win, mod_cvals = rotate.check_horiz(cvals)
            if is_win:
                break

            is_win, mod_cvals = rotate.check_horiz(np.rot90(cvals))
            if is_win:
                for r in range(3):
                    mod_cvals = np.rot90(mod_cvals)
                break

            is_win, mod_cvals = rotate.check_horiz(rotate.rot45(cvals))
            if is_win:
                mod_cvals = rotate.unrot45(mod_cvals)
                break

            is_win, mod_cvals = rotate.check_horiz(rotate.rrot45(cvals))
            if is_win:
                mod_cvals = rotate.unrrot45(mod_cvals)

            break  # stop while
        if is_win:
            self.cvals = mod_cvals
            self.refresh()
            self.end_game = True
        elif self.cnt > 19*19-1:
            self.end_game = True

