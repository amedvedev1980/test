from PyQt5.QtCore import QThread, pyqtSignal
from mnet.mserver import MServer


class HumanNet(QThread):
    do_moves = pyqtSignal(tuple)
    end_moves = pyqtSignal(bool)

    def __init__(self, tboard, p_info, figure):
        super().__init__()
        self.tboard = tboard
        self.p_info = p_info
        self.figure = figure
        self.moves = 2
        # self.do_moves.connect(self.tboard.do_moves)
        self.addr = 'localhost', 8999
        self.mserv = MServer(*self.addr)

    def __del__(self):
        self.tboard.pcnt -= 1
        print('human_net destroyed')

    def reset(self):
        # self.do_moves.disconnect()
        pass

    def run(self):
        # нужно передать по сети, что ждем два хода, но игрок на той стороне, получив о нас уже два хода
        # знает, что надо нам прислать его ходы
        pass
