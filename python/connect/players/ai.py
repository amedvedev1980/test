import random
from PyQt5.QtCore import QThread, pyqtSignal
from mmath.analysis import *
import defs


class Ai(QThread):
    act = pyqtSignal(tuple)
    end_moves = pyqtSignal(bool)

    def __init__(self, tboard, p_info, figure):
        super().__init__()
        self.tboard = tboard
        self.p_info = p_info
        self.figure = figure
        self.enemy = defs.figure['cross'] if self.figure == defs.figure['zero'] else defs.figure['zero']
        self.moves = 2
        self.act.connect(self.tboard.on_put)

    def __del__(self):
        self.tboard.pcnt -= 1
        print('ai destroed')

    def reset(self):
        self.act.disconnect()


    def run(self):
        if not self.tboard.end_game:
            if self.moves > 0:  # нужно сделать ход AI
                self.make_moves()

            self.moves = 2
            self.end_moves.emit(True)

    def do_move(self, move):
        if not self.tboard.end_game:
            if self.moves and move is not None:
                x, y = move
                if self.tboard.cvals[y][x] == 0:
                    self.act.emit((move, self.figure))
                    self.moves -= 1
                    return True
        return False

    def make_moves(self):
        vals = self.tboard.cvals

        # Ищем свои победные ходы
        vecs = find_vecs(self.figure, 5, vals)
        for vec in vecs:
            x, y, left, right = vec
            self.do_move(left)
            self.do_move(right)
            if not self.moves:
                break
        # Ищем четверку, которую можно достроить до 6ки
        moves = []
        vecs = find_vecs(self.figure, 4, vals)
        for vec in vecs:
            x, y, left, right = vec
            for i in range(0, 2):
                cell = None
                if right is not None:
                    cell = get_cell(vals, x, y, *right, i)   #########
                if left is not None:
                    cell = get_cell(vals, x, y, *left, -i-1)
                if cell is not None:
                    moves.append(cell)
                else:
                    moves = []
                    break
        for move in moves:
            self.do_move(move)

        # Ищем пятерки, и закрываем любые, потому как любая пятерка на следующем ходе наш проигрыш
        vecs = find_vecs(self.enemy, 5, vals)
        for vec in vecs:
            x, y, left, right = vec
            self.do_move(left)
            self.do_move(right)
            if not self.moves:
                break
        # Ищем четверки, закрывать надо сначало ту у которой есть и left и right
        vecs = find_vecs(self.enemy, 4, vals)
        last_left = None
        last_right = None
        for vec in vecs:
            x, y, left, right = vec
            if left is not None and right is not None:
                self.do_move(left)
                self.do_move(right)
            elif left is not None:
                last_left = left
            elif right is not None:
                last_right = right

            if not self.moves:
                break
        self.do_move(last_left)
        self.do_move(last_right)
        # Закрываем и тройки
        vecs = find_vecs(self.enemy, 3, vals)
        for vec in vecs:
            x, y, left, right = vec
            if left is not None and right is not None:
                v0 = random.choice((left, right))
                v1 = left if v0 == right else right
                if not self.do_move(v0):
                    self.do_move(v1)
        # Ходим
        # ищем тройку
        vecs = find_vecs(self.figure, 3, vals)
        for vec in vecs:
            x, y, left, right = vec
            if left is not None and right is not None:
                #if test_cells(vals, x, y, *right, )
                self.do_move(left)
                self.do_move(right)
                break

        # Остались ходы, ходим от балды
        while self.moves:
            self.do_move(self.random_move())

    def random_move(self):
        while 1:
            x = random.randint(0, 18)
            y = random.randint(0, 18)
            if self.tboard.cvals[y][x] == 0:
                return x, y
