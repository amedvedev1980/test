import time
from PyQt5.QtCore import QThread, pyqtSignal


class Human(QThread):
    do_moves = pyqtSignal(tuple)
    end_moves = pyqtSignal(bool)

    def __init__(self, tboard, p_info, figure):
        super().__init__()
        self.tboard = tboard
        self.p_info = p_info
        self.figure = figure
        self.moves = 2
        self.do_moves.connect(self.tboard.do_moves)

    def __del__(self):
        self.tboard.pcnt -= 1
        print('human destroyed')

    def reset(self):
        self.do_moves.disconnect()

    def run(self):
        # Передаем сигнал на поле, что ждем от игрока ходов
        self.do_moves.emit((self.moves, self.figure))
        # ждем пока ходы не кончатся
        while not self.tboard.end_moves and not self.tboard.end_game:
            self.sleep(1)
            pass
        self.tboard.end_moves = False
        # Передаем ход другому игроку
        self.moves = 2
        self.end_moves.emit(True)
