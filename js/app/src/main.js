import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import store from './store'
import router from './router'

Vue.use(Vuetify);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
