import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentFile: null,
    loadingFile: -1,     /* -1    - �������� �� �������������
                            0     - �������� ���������� �� ���������� 0%
                            1..99 - �������� ����������,
                            100   - ���������� ��������� �������,
                            101   - ���������� ��������� � �������
                        */
  },
  getters: {
    CURRENTFILE: state => {
      return state.currentFile;
    },
    LOADINGFILE: state => {
      return state.loadingFile;
    }
  },
  mutations: {
    SET_CURRENTFILE: (state, currentFile) => {
      state.currentFile = currentFile;
    },
    SET_LOADINGFILE: (state, status) => {
      state.loadingFile = status
    }
  },
  actions: {
    SET_CURRENTFILE: (state, currentFile) => {
      state.commit('SET_CURRENTFILE', currentFile);
    },
    async DO_LOADFILE ({ getters, commit }) {
      commit('SET_LOADINGFILE', 0)
      const currentFile = getters.CURRENTFILE;
      await axios.post('../?action=load_file', {name: currentFile})
        .then( (response) => {
          commit('SET_LOADINGFILE', 100);
        })
        .catch( (error) => {
          console.log(error);
          commit('SET LOADINGFILE', 101);
        });
    },
    async DO_PROCESS ({ dispatch, commit}){
      await dispatch('DO_LOADFILE')
    },
  }
})
