/**
 *	Тестовая версия. 
 */
window.addEventListener('DOMContentLoaded', function() {
  var vk_status = false;
  var action_on = false;
  var is_period_add = false;
  var root_url = window.location.origin + '/' + window.location.pathname.split('\/')[1];
  var indexUrl = root_url + "/sicklist/index.php";

  var pData;

  var mData = document.getElementById('main_data');
  var idUser = mData.getAttribute('data-user');
  var has_not_sended_period = mData.getAttribute('data-has-not-sended_period');
  if (has_not_sended_period == 1){
    is_period_add = true;
  }
  var idUserRole = mData.getAttribute('data-user-role');
  var startDate = moment(mData.getAttribute('data-start-date'));
  var rowIndex = 0;
  var form = document.getElementById('sicklist'); // форма электронного листка нетрудоспособности
  var table = document.getElementById('periods'); // таблица с периодами нетрудоспособности

  var curr_sign;
  var curr_sign_user;

  var show_data = function (table, data) {
    // reset
    console.log('show_date:');
    [].forEach.call(table.querySelectorAll('[data-column]'), function (td) {
      td.innerText = '';
    });
    [].forEach.call(table.querySelectorAll('tbody > tr > td > div'), function (div) {
      addClass(div, 'd-none');
    });

    // show
    for (var i = 0; i < data.length; i++) {
      var row = data[i];
      var tr = table.querySelector('tbody > tr[data-row="' + i + '"]');
      if (tr) {
        // SET ROW ID
        var rowId = (row.ID_ITEM > 0 ) ? row.ID_ITEM : 0;
        //if (tr.dataset['id']) {
        //  rowId = row[ tr.dataset['id'] ];
        //}
        var inputs = [];
        inputs[0] = document.createElement('input');
        inputs[0].setAttribute('type', 'hidden');
        inputs[0].setAttribute('id', 'ID_ITEM'+i);
        inputs[0].setAttribute('name', 'ID_ITEM['+i+']');
        inputs[0].setAttribute('value', rowId);

        //console.log( row );
        var ipnut;
        for (var key in row) {
          if (row.hasOwnProperty(key)) {
            var td = tr.querySelector('td[data-column="' + key + '"]');
            if (td) {
              td.innerText = row[key];
              input = document.createElement("input");
              input.setAttribute('type', 'hidden');
              if (td.dataset['send']){
                console.log('dataset data-send: '+td.dataset['send']);
                console.log('MAKE ID');
                var key_id = td.dataset['send'];
                input.setAttribute('name', key_id + '[' + i + ']');
                input.setAttribute('value', row[key_id]);
              }
              else {
                input.setAttribute('name', key + '[' + i + ']');
                input.setAttribute('value', row[key]);
              }
              td.appendChild(input);
              while(hinput=inputs.pop()){
                td.appendChild(hinput);
              }
            }
          }
        }

        if (row.PIN !== undefined) {
          if ((row.PIN !== '') && (row.LN_STATE != 1)) {
            var td = tr.querySelector('td[data-column="USER_NAME"]');
            td.innerHTML = td.innerHTML + "<br><span style='color:red; font-style: italic;'>подписано</span>";
          }
        }
        if (row.VK_STATUS ==2 && row.VK_PIN) {
          var td = tr.querySelector('td[data-column="USER_VK"]');
          td.innerHTML = td.innerHTML + "<br><span style='color:red; font-style: italic;'>подписано</span>";
        }

        if (row.LN_STATE == 1 || row.FROM_FSS) {
          var div = tr.querySelector('td[data-status="USER_LOCK"] > div');
          removeClass(div, 'd-none');
          if (row.VK_STATUS == 2){
            div = tr.querySelector('td[data-status="USER_VK_LOCK"] > div');
            removeClass(div, 'd-none');
          }
        }


        if (input = document.getElementById('hidden_pin_'+i)){
          if (row.PIN !== undefined) {
            input.setAttribute('value', row.PIN);
          }
        }
        else {
          input = document.createElement("input");
          input.setAttribute('id', 'hidden_pin_'+i);
          input.setAttribute('type', 'hidden');
          input.setAttribute('name', 'PIN[' + i + ']');
          if (row.PIN) {
            input.setAttribute('value', row.PIN);
          }
          form.appendChild(input);
        }

        [].forEach.call(tr.querySelectorAll('td > div.btn-group'), function (div) {
          removeClass(div, 'd-none');
        });

        var cancel_vk = document.querySelector('button[data-nav="cancel_vk"]');
        if (row.VK_STATUS == 1) {
          document.getElementById('head_mess').innerHTML = "<span style='color:red; '>ожидает подписи ВК</span>";
          vk_status = true;
          removeClass(cancel_vk, 'd-none');
        }
        else{
          addClass(cancel_vk, 'd-none');
        }
      }
      if (row.LN_STATE == 1 || row.FROM_FSS || row.VK_STATUS==1){
        [].forEach.call(tr.querySelectorAll('.btn'), function (button){
          button.disabled = true;
        });
      }
    }
    if (vk_status) {
      eln_disable(vk_status);
    }
  };
  var prepare_sign_modal = function(){
    document.getElementById('SIGN_ROW_ID_USER').value = idUser;
  }
  var prepare_modal = function (row) {
    if (row > 0) {
      row = row - 1;
      document.getElementById('ID_ITEM').value = pData[row].ID_ITEM;
      document.getElementById('ROW_DATE_BEGIN').value =pData[row].DATE_BEGIN;
      document.getElementById('ROW_DATE_END').value = pData[row].DATE_END;

      document.getElementById('ROW_ID_USER_ROLE').value = pData[row].ID_USER_ROLE;
      document.getElementById('ROW_ID_USER').value = pData[row].ID_USER;
      document.getElementById('ROW_ID_USER_VK').value = pData[row].ID_USER_VK;
      document.getElementById('PIN').value = pData[row].PIN;
    }
    else {
      var issueDate = document.getElementById('LN_DATE').value;
      document.getElementById('ID_ITEM').value = 0;
      var startPeriod = startDate.format('YYYY-MM-DD');
      if (document.getElementById('LN_DATE').value != ''){
        startPeriod = document.getElementById('LN_DATE').value;
      }
      document.getElementById('ROW_DATE_BEGIN').value = startPeriod;
      document.getElementById('ROW_DATE_END').value = '';

      document.getElementById('ROW_ID_USER_ROLE').value = idUserRole;
      document.getElementById('ROW_ID_USER').value = idUser;
      document.getElementById('ROW_ID_USER_VK').value = -1;
      document.getElementById('PIN').value = '';
    }
    document.getElementById('ROW_DATE_BEGIN').focus();
  };
  var block_disable = function (id, mode)
  {
    var fset;
    if (fset = document.querySelector('fieldset.multi-collapse#' + id) )
    {
      fset.disabled = mode;
    }
  };
  var memblocks= [];
  var eln_disable = function(mode){
    if (mode) {
      [].forEach.call(document.querySelectorAll('fieldset.collapse'), function (item) {
        if (!item.classList.contains('show')) {
          item.classList.add('show');
        }
      });
    }
    [].forEach.call(document.querySelectorAll('.btn, input, select'), function (item) {
      var toggle = item.dataset['toggle'];
      if (!item.classList.contains('super-enabled')) {
        item.disabled = mode;
      }
    });
  }
  var all_disable = function (mode)
  {
    memblocks = [];
    [].forEach.call(document.querySelectorAll('fieldset.multi-collapse'), function(fset) {
      if (fset.disabled != mode)
      {
        memblocks.push( fset.id );
      }
      fset.disabled = mode;
    });
  };
  var mem_blocks = function (mode)
  {
    for (var i = 0, len = memblocks.length; i < len; i++) {
      block_disable(memblocks[i], mode);
    }
  }

  var nav_disable = function (mode)
  {
    [].forEach.call(document.querySelectorAll('button[data-nav]'), function(button) {
      button.disabled = mode;
    });
  };
  var btn_nav_disable = function (btn_name, mode)
  {
    [].forEach.call(document.querySelectorAll('button[data-nav="' + btn_name + '"]'), function(button) {
      button.disabled = mode;
    });
  };

  // КНОПКИ УПРАВЛЕНИЯ (navbar)
  [].forEach.call(document.querySelectorAll('button[data-nav]'), function (button) {
    var action = button.dataset.nav;
    switch(action){
      case 'save':
      case 'send':
      case 'next':
        button.onclick = function(){
          if (action_on === false) {
            action_on = true;
            
            form.action = action;
            document.getElementById('form-submit').click();
            nav_disable(false);
          }
        };
        break;
      case 'cancel_vk':
      case 'del_db':
      case 'del':
        button.onclick = function(){
          if (action_on === false) {
            action_on = true;
            fetch(indexUrl + '?action=' + action)
              .then(function (response) {
                return response.json();
              })
              .then(function (result) {
                if (result.error)
                  alert(result.error);
                if (result.message) {
                  alert(result.message);
                  location.reload();
                }
                action_on = false;
               })
              .catch(function (error) {
                console.log(error);
                action_on = false;
              });
          }
        };
        break;
      case 'load_prev':
      case 'load_next':
        button.onclick = function() {
          var url = location.href.split('?')[0];
          var params = 'elncnt=' + eln_cnt + '&elnpos=' + eln_pos + '&view=' + action;
          var _url = ( url.indexOf('?') !== -1 ) ? url + '&' + params : url + '?' + params;

          window.location = _url;
        }
        break;
      case 'load_fss':
        button.onclick = function () {

          var fssloadModal = document.getElementById('load_fss_win');
          var fssloadModalInstance = new Modal(fssloadModal, {
            backdrop: true,
            keyboard: false
          });
          fssloadModalInstance.show();
        }
        break;
    }
  });

  // ТАБЛИЦА
  // начальная загрузка данных в таблицу периодов нетрудоспособности:
  fetch(root_url + "/core/models/newgen/disabilityitem.api.php", {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      key: 'ID_DSBL',
      keyVal: document.getElementById('ID_DSBL').value,
      method: 'get_items'
    })
  })
    .then(function (response) {
      return response.json();
    })
    .then(function (result) {
      pData = result;
      console.log(pData);
      show_data(table, pData);
    })
    .catch(function (error) {
      console.log('Request failed', error);
    });

  document.getElementById('add_period').onclick = function (event) {
    if(pData.length < 3) {
      if (!is_period_add) {
        prepare_modal(0);
        periodModalInstance.show();
      }
      else {
        alert("Нельзя добавить новый период не отправив предыдущий в ФСС.");
      }
    }
    else{
      alert("В одном ЭЛН можно добавить максимум три периода нетрудоспособности.");
    }
  };
  // нажатие кнопок [Редактировать][Удалить] в строке с периодом нетрудоспособности:
  [].forEach.call(table.querySelectorAll('tbody > tr'), function (row) {
    row.querySelector('td > div > button[name="del_row"]').onclick = function (e) {
      var index = row.rowIndex-1;
      console.log(pData[index]);
      if (pData[index].ID_ITEM > 0){
        fetch(root_url + "/core/models/newgen/disabilityitem.api.php", {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            ID_ITEM: pData[index].ID_ITEM,
            method: 'del'
          })
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (result) {
            if (result.success) {
              location.reload();
            }
          })
          .catch(function (error) {
            console.log('Request failed', error);
          });
      }
      else{
        pData.splice(index, 1);
        is_period_add = false;
        show_data(table, pData);
      }
    }
    row.querySelector('td > div > button[name="edit_row"]').onclick = function (e) {
      rowIndex = row.rowIndex;
      prepare_modal(row.rowIndex);
      periodModalInstance.show();
    }
  });

  document.getElementById('do_load_fss').onclick = function (event) {
    if (action_on === false) {
      action_on = true;
      fetch(indexUrl + '?action=load_fss&eln_num='
        + document.getElementById('REQ_ELN_NUM').value + '&snils=' + document.getElementById('REQ_SNILS').value)
        .then(function (response) {
          return response.json();
        })
        .then(function (result) {
          if (result.error)
            alert(result.error);
          if (result.message) {
            alert(result.message);
            location.reload();
          }
          action_on = false;
        })
        .catch(function (error) {
          console.log(error);
          action_on = false;
        });
    }
  }

  // МОДАЛЬНОЕ ОКНО "Подпись"
  var signModal = document.getElementById('sign_doc');
  var signModalInstance = new Modal(signModal, {
    backdrop: true,
    keyboard: true
  });
  signModal.addEventListener('show.bs.modal', function (event){
    // событие: открытие модального окна
  }, false);
  signModal.addEventListener('hidden.bs.modal', function (event) {
    // событие: закрытие модального окна
    rowIndex = 0;
  }, false);
  var pin_status = function (name) {
    var el_pin;
    if (el_pin = document.getElementById(name+'_PIN')){
      var status = document.getElementById(name+'_STATUS');
      if (el_pin.value.length > 0){
        status.innerText = 'подписано';
      }
      else{
        status.innerText = 'не подписано';
      }
    }
  };
  document.getElementById('do_sign').onclick = function (event) {
    var input = document.getElementById(curr_sign);
    var val = document.getElementById('SIGN_PIN_DOC').value;   
    input.setAttribute('value', val);
    val = document.getElementById('SIGN_ROW_ID_USER').value;
    input = document.getElementById(curr_sign_user);
    input.setAttribute('value', val);

    pin_status('CLOSE_DOC');
    pin_status('VIOL');
  }
  pin_status('CLOSE_DOC');
  pin_status('VIOL');

  //SIGN_VIOL_BTN
  document.getElementById('SIGN_VIOL_BTN').onclick = function (event) {
    if(pData.length > 0 && pData.length <= 3) {
      curr_sign = 'VIOL_PIN';
      curr_sign_user = 'VIOL_SIGN';
      prepare_sign_modal();
      signModalInstance.show();
    }
    else{
      alert("Нет периодов.");
    }
  };
  document.getElementById('CLOSE_DOC_ID_USER_BTN').onclick = function (event) {
    if(pData.length > 0 && pData.length <= 3) {
      curr_sign = 'CLOSE_DOC_PIN';
      curr_sign_user = 'CLOSE_DOC_ID_USER';
      prepare_sign_modal();
      signModalInstance.show();
    }
    else{
      alert("Закрыть можно ЭЛН с отправленными периодами нетрудоспособности.");
    }
  };
  // МОДАЛЬНОЕ ОКНО "Периоды"
  // добавление/редактирование периода нетрудоспособности:
  var periodModal = document.getElementById('edit_period');
  var periodModalInstance = new Modal(periodModal, {
    backdrop: true,
    keyboard: true
  });
  periodModal.addEventListener('show.bs.modal', function (event) {
    // событие: открытие модального окна
  }, false);
  periodModal.addEventListener('hidden.bs.modal', function (event) {
    // событие: закрытие модального окна
    rowIndex = 0;
  }, false);
  document.getElementById('save_period').onclick = function (event) {
    var i = pData.length;
    var dateEnd = document.getElementById('ROW_DATE_END').value;
    var item = {
      "ID_ITEM": document.getElementById('ID_ITEM').value,
      "DATE_BEGIN": document.getElementById('ROW_DATE_BEGIN').value,
      "DATE_END": dateEnd,
      "ID_USER_ROLE": document.getElementById('ROW_ID_USER_ROLE').value,
      "USER_ROLE": document.getElementById('ROW_ID_USER_ROLE').selectedOptions[0].text,
      "ID_USER": document.getElementById('ROW_ID_USER').value,
      "USER_NAME": document.getElementById('ROW_ID_USER').selectedOptions[0].text,
      "ID_USER_VK": document.getElementById('ROW_ID_USER_VK').value,
      "USER_VK": document.getElementById('ROW_ID_USER_VK').selectedOptions[0].text,
      "PIN": document.getElementById('PIN').value
    };
    console.log(item);
    if (rowIndex) {
      pData[rowIndex - 1] = item;
      rowIndex = 0;
      show_data(table, pData);
    }
    else {
      if (i <= 3) {
        is_period_add = true;
        pData.push(item);
        console.log(pData);
        show_data(table, pData);
        if (dateEnd) {
          startDate = moment(dateEnd).add(1, 'days');
        }
      }
    }
  };

  // ФОРМА
  // отправка данных электронного листка нетрудоспособности:
  form.addEventListener('submit', function (evt) {
    all_disable(false);
    evt.preventDefault();
    var formData = new FormData(form);

    var url = indexUrl + "?action=" + this.getAttribute("action");

    fetch(url, {
      method: 'post',
      body: formData
    })
      .then(function(response){
        return response.json();
      })
      .then(function(result){
        if (result.error){
          alert('Ошибка сохранения данных: ' + result.error);
        }
        else{
          if (result.ID_DSBL) {
            document.getElementById('ID_DSBL').value = result.ID_DSBL;
            for (var key in result.ID_ITEM) {
              if (result.ID_ITEM.hasOwnProperty(key)) {
                pData[key].ID_ITEM = result.ID_ITEM[key];
              }
            }
          }
          show_data(table, pData);

          if (result.request_vk){
            if (confirm('Отправить на подпись ВК? ' + result.request_vk)){
              fetch(root_url + "/core/models/newgen/disabilityitem.api.php", {
                method: 'post',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  method: 'set_vk_status',
                  ID_DSBL: document.getElementById('ID_DSBL').value,
                  value: 1
                })
              })
                .then(function (response) {
                  return response.json();
                })
                .then(function (result) {
                  if (result.error) {
                    alert(result.error);
                  }
                  else if (result.message){
                    alert('Запрос на подпись ВК отправлен.');
                  }
                })
                .catch(function (error) {
                  console.log('Request failed', error);
                });

            }
            else{

            }
          }
          else if (result.send_error){
            alert('Ошибка отправки данных: ' + result.send_error);
          }
          else {
            is_period_add = false;
            if (result.message) {
              alert(result.message);
            }
            for(var key in result.blocks)
            {
              if (result.blocks.hasOwnProperty(key)){
                block_disable( result.blocks[key], true );
              }
            }
            if (result.status){
              var statusItem = document.getElementById('LN_STATE');
              statusItem.innerText = result.status;
              set_ref_value(statusItem);
            }
          }
          location.reload();
        }
        action_on = false;
        nav_disable( false );
      })
      .catch(function(error){
        console.log('error:' + error);
        action_on = false;
        nav_disable( false );
      });
    mem_blocks(true);
  }, false);

  // кнопка: Получить номер ЭЛН
  document.getElementById('get_id').onclick = function(){
    if (action_on === false) {
      action_on = true;
      var idNum = document.getElementById('ID');
      if (idNum.value) {
        alert('Номер ЭЛН уже получен!');
        action_on = false;
      }
      else {
        fetch(indexUrl + '?action=get_num')
          .then(function (response) {
            return response.json();
          })
          .then(function (result) {
            if (result.error)
              alert(result.error);
            if (result.message) {
              idNum.value = result.message;
            }
            action_on = false;
          })
          .catch(function (error) {
            console.log(error);
            action_on = false;
          });
      }
    }
  };

  all_disable( false );
  var close_status = mData.getAttribute('data-close-status');
  var maininfo_status = mData.getAttribute('data-maininfo-status');
  var patient_status = mData.getAttribute('data-patient-status');
  var requisites_status = mData.getAttribute('data-requisites-status');
  var sanatorium_status = mData.getAttribute('data-sanatorium-status');
  var caring_status = mData.getAttribute('data-caring-status');
  var gestation_status = mData.getAttribute('data-gestation-status');
  var hospital_status = mData.getAttribute('data-hospital-status');
  var violation_status = mData.getAttribute('data-violations-status');
  var expertise_status = mData.getAttribute('data-expertise-status');
  var eln_cnt = mData.getAttribute('data-eln-cnt');
  var eln_pos = mData.getAttribute('data-eln-pos');

  if ((Number(eln_pos) + 1) < eln_cnt){
    btn_nav_disable('load_next', false);
  }
  else{
    btn_nav_disable('load_next', true);
  }
  if (eln_pos > 0){
    btn_nav_disable('load_prev', false);
  }
  else{
    btn_nav_disable('load_prev', true);
  }

  if (close_status == 1 || vk_status){
    block_disable('close', true);
    [].forEach.call(document.querySelectorAll('fieldset.collapse'), function(item) {
      if (!item.classList.contains('show')){
        item.classList.add('show');
      }
    });
    [].forEach.call(document.querySelectorAll('.btn, input, select'), function (item) {
      var toggle = item.dataset['toggle'];
      if (!item.classList.contains('super-enabled')) {
        item.disabled = true;
      }
    });

  }

  if (maininfo_status == 1) block_disable('main_data', true);
  if (patient_status == 1) block_disable('patient', true);
  if (requisites_status == 1) block_disable('requisites', true);
  if (sanatorium_status == 1) block_disable('sanatorium', true);
  if (caring_status == 1) block_disable('caring', true);
  if (gestation_status == 1) block_disable('gestation', true);
  if (hospital_status == 1) block_disable('hospital', true);
  if (violation_status == 1) block_disable('violation', true);
  if (expertise_status == 1)  block_disable('expertise', true);
});
