SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS homeorg CHARACTER SET utf8 COLLATE utf8_general_ci;

USE homeorg;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `passw` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=innoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_user`(IN sLogin VARCHAR(20), IN sEmail VARCHAR(250), OUT sPassw VARCHAR(32), OUT sId INT)
BEGIN
  DECLARE rId INT;
  DECLARE rPassw VARCHAR(32);
  
  SELECT `id`, `passw` INTO rId, rPassw FROM `users` WHERE `login` = sLogin OR `email` = sEmail;
  IF rPassw IS NOT NULL THEN
    SET sId = rId;
	SET sPassw = rPassw;
  ELSE
    SET sId = 0;
	SET sPassw = '';
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_user`(IN sLogin VARCHAR(20), IN sEmail VARCHAR(250), IN sPassw VARCHAR(32), OUT sId INT)
BEGIN
  DECLARE next_id INT;
  DECLARE rLogin VARCHAR(20);
  IF sLogin = 'noname' OR sLogin IS NULL THEN
    SET next_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'users');
    SET rLogin = CONCAT('noname', next_id);
  ELSE
    SET rLogin = sLogin;
  END IF;
  INSERT INTO `users` (`id`, `login`, `passw`, `email`) VALUES (next_id, rLogin, sPassw, sEmail);
  SET sId = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_user_login`(sId INT) RETURNS varchar(20) CHARSET cp1251
BEGIN
  RETURN (SELECT `login` FROM `users` WHERE `id` = sId);
END$$

DELIMITER ;
