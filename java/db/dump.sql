-- MySQL dump 10.13  Distrib 5.5.29, for Win32 (x86)
--
-- Host: localhost    Database: homeorg
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор счета',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'группа к которой относится счет',
  `user_id` int(11) DEFAULT NULL COMMENT 'пользователь который является владельцем счета',
  `name` varchar(255) DEFAULT NULL COMMENT 'название счета',
  `amount` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT 'сумма денег  на счете',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания счета',
  `currency_id` int(11) DEFAULT NULL COMMENT 'валюта счета',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'если NULL Значит это мастер счет группы',
  PRIMARY KEY (`id`),
  KEY `FK_accounts_users_group_id` (`users_group_id`),
  KEY `FK_accounts_user_id` (`user_id`),
  KEY `FK_accounts_currency_id` (`currency_id`),
  KEY `FK_accounts_users_session_id` (`users_session_id`),
  CONSTRAINT `FK_accounts_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_accounts_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_accounts_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,12,10,NULL,0.00,'2013-01-26 09:33:08',1,NULL),(2,13,11,NULL,0.00,'2013-01-26 09:34:01',1,NULL),(3,14,12,NULL,0.00,'2013-01-26 09:34:16',1,NULL),(4,15,13,NULL,0.00,'2013-01-26 09:34:40',1,NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classifiers`
--

DROP TABLE IF EXISTS `classifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор классификатора',
  `is_global` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'глобальный классификатор (виден всем)',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользовательской группы владельца (для локальных классификаторов)',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'юзер создавший классификатор',
  `name` varchar(255) DEFAULT NULL COMMENT 'название классификатора',
  `language_id` int(11) DEFAULT NULL COMMENT 'идентификатор оригинального языка на котором составлен классификатор',
  `def_products_group_id` int(11) DEFAULT NULL COMMENT 'группа продуктов по умолчанию',
  PRIMARY KEY (`id`),
  KEY `FK_classifiers_user_id` (`users_session_id`),
  KEY `FK_classifiers_users_group_id` (`users_group_id`),
  KEY `FK_classifiers_language_id` (`language_id`),
  KEY `FK_classifiers_products_group_id` (`def_products_group_id`),
  CONSTRAINT `FK_classifiers_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_classifiers_products_group_id` FOREIGN KEY (`def_products_group_id`) REFERENCES `products_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_classifiers_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_classifiers_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='классификаторы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classifiers`
--

LOCK TABLES `classifiers` WRITE;
/*!40000 ALTER TABLE `classifiers` DISABLE KEYS */;
INSERT INTO `classifiers` VALUES (1,0,15,NULL,'default',1,1),(2,0,12,NULL,'default',1,2),(3,0,13,NULL,'default',1,3),(4,0,14,NULL,'default',1,4);
/*!40000 ALTER TABLE `classifiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор страны',
  `name` varchar(255) NOT NULL COMMENT 'название страны',
  `language_id` int(11) DEFAULT NULL COMMENT 'идентфикатор национального языка',
  `iname` varchar(255) NOT NULL DEFAULT ' ' COMMENT 'интернациональное название',
  `currency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_countries_language_id` (`language_id`),
  KEY `FK_countries_currency_id` (`currency_id`),
  CONSTRAINT `FK_countries_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_countries_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='страны';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Россия',1,'russia',1),(2,'USA',2,'usa',3),(3,'England',2,'england',7),(4,'France',3,'france',5);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор валюты',
  `parent_id` int(11) DEFAULT NULL COMMENT 'родительская денежная единица',
  `symbol_translations_group_id` int(11) DEFAULT NULL COMMENT 'перевод на другие языки для поля symbol',
  `name_translations_group_id` int(11) DEFAULT NULL COMMENT 'перевод на другие языки для name',
  `iso_code` varchar(20) DEFAULT NULL COMMENT 'код ISO',
  `iso_code_num` int(11) DEFAULT NULL COMMENT 'числовой код ISO',
  `country_id` int(11) DEFAULT NULL COMMENT 'идентификатор страны где используется валюта как национальная',
  `parent_factor` int(11) DEFAULT NULL COMMENT 'множитель  (т.е. child*factor = parent или 1коп. * 100 = 1 рубль)',
  PRIMARY KEY (`id`),
  KEY `FK_currency_name_translations_group_id` (`name_translations_group_id`),
  KEY `FK_currency_symbol_translations_group_id` (`symbol_translations_group_id`),
  KEY `FK_currency_country_id` (`country_id`),
  CONSTRAINT `FK_currency_name_translations_group_id` FOREIGN KEY (`name_translations_group_id`) REFERENCES `translations_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_currency_symbol_translations_group_id` FOREIGN KEY (`symbol_translations_group_id`) REFERENCES `translations_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276 COMMENT='справочник национальных валют';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,NULL,NULL,NULL,'RUB',643,1,NULL),(2,1,NULL,NULL,NULL,NULL,1,100),(3,NULL,NULL,NULL,'USD',840,2,NULL),(4,3,NULL,NULL,NULL,NULL,2,100),(5,NULL,NULL,NULL,'EUR',978,4,NULL),(6,5,NULL,NULL,NULL,NULL,4,100),(7,NULL,NULL,NULL,'GBP',826,3,NULL),(8,7,NULL,NULL,NULL,NULL,3,100);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency_countries`
--

DROP TABLE IF EXISTS `currency_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency_countries` (
  `currency_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `symbol` varchar(20) NOT NULL,
  `add_symbol` varchar(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `rule_writing` tinyint(1) NOT NULL DEFAULT '0',
  KEY `FK_currency_countries_currency_id` (`currency_id`),
  KEY `FK_currency_countries_country_id` (`country_id`),
  CONSTRAINT `FK_currency_countries_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_currency_countries_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency_countries`
--

LOCK TABLES `currency_countries` WRITE;
/*!40000 ALTER TABLE `currency_countries` DISABLE KEYS */;
INSERT INTO `currency_countries` VALUES (1,1,'руб.','р.','рубль',0),(2,1,'коп.',NULL,'копейка',0),(3,2,'$','US$','U.S. dollar',1),(4,2,'¢',NULL,'cent',0),(5,4,'€',NULL,'euro',0),(6,4,'sent',NULL,'sent',0),(7,3,'£','#','pound',1),(8,3,'p',NULL,'penny',0);
/*!40000 ALTER TABLE `currency_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор языка',
  `name` varchar(255) NOT NULL COMMENT 'название языка (в его раскладке)',
  `iname` varchar(255) NOT NULL COMMENT 'интернациональное название',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'русский','russian'),(2,'english','english'),(3,'français','french');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_units`
--

DROP TABLE IF EXISTS `m_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор единицы измерения',
  `parent_id` int(11) DEFAULT NULL COMMENT 'родительская единица измерения',
  `m_unit_group_id` int(11) NOT NULL COMMENT 'принадлежность к группе',
  `symbol` varchar(20) NOT NULL COMMENT 'условное обозначение (для национальных единиц на национальном языке, иначе интернациональное)',
  `name` varchar(255) NOT NULL COMMENT 'наименование единицы измерения (для национальных единиц на национальном языке, иначе интернациональное)',
  `is_old` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'запись предварительно удалена',
  `multiplier` decimal(18,9) NOT NULL DEFAULT '1.000000000' COMMENT 'коэффициент умножения (чтобы получить из родительского)',
  `country_id` int(11) DEFAULT NULL COMMENT 'идентификатор страны (для национальных единиц измерения)',
  `is_international` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'интернациональная единица измерения',
  `code` varchar(20) NOT NULL COMMENT 'кодовое буквенное обозначение (для национальных единиц на национальном языке, иначе интернациональное)',
  PRIMARY KEY (`id`),
  KEY `FK_m_units_m_units_group_id` (`m_unit_group_id`),
  KEY `FK_m_units_countrie_id` (`country_id`),
  CONSTRAINT `FK_m_units_countrie_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_m_units_m_units_group_id` FOREIGN KEY (`m_unit_group_id`) REFERENCES `m_units_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276 COMMENT='единицы измерения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_units`
--

LOCK TABLES `m_units` WRITE;
/*!40000 ALTER TABLE `m_units` DISABLE KEYS */;
INSERT INTO `m_units` VALUES (1,NULL,1,'м','метр',0,1.000000000,NULL,1,'MTR'),(2,1,1,'мм','миллиметр',0,0.001000000,NULL,1,'MMT'),(3,1,1,'см','сантиметр',0,0.010000000,NULL,1,'CMT'),(4,1,1,'дм','дециметр',0,0.100000000,NULL,1,'DMT'),(5,1,1,'км','километр',0,1000.000000000,NULL,1,'KMT'),(6,1,1,'Мм','мегаметр',0,1000000.000000000,NULL,1,'MAM'),(7,1,1,'дюйм','дюйм',0,0.025400000,NULL,1,'INH'),(8,1,1,'фут','фут',0,0.304800000,NULL,1,'FOT'),(9,1,1,'миля','морская миля',0,1852.000000000,NULL,1,'NMI'),(10,NULL,1,'пог. м','погонный метр',0,1.000000000,NULL,0,'ПОГ М'),(11,NULL,1,'усл. м','условный метр',0,1.000000000,NULL,0,'УСЛ М'),(12,1,1,'ярд','ярд',0,0.914400000,NULL,1,'YRD'),(13,NULL,2,'м2','квадратный метр',0,1.000000000,NULL,1,'MTK'),(14,13,2,'мм2','квадратный миллиметр',0,0.000001000,NULL,1,'MMK'),(15,13,2,'см2','квадратный сантиметр',0,0.000010000,NULL,1,'CMK'),(16,13,2,'дм2','квадратный дециметр',0,0.010000000,NULL,1,'DMK'),(17,13,2,'км2','квадратный километр',0,1000000.000000000,NULL,1,'KMK'),(18,13,2,'га','гектар',0,10000.000000000,NULL,1,'HAR'),(19,13,2,'а','ар',0,100.000000000,NULL,1,'ARE'),(20,NULL,2,'усл. м2','условный квадратный метр',0,1.000000000,NULL,0,'УСЛ М2'),(21,NULL,2,'м2 общ. пл','квадратный метр общей площади',0,1.000000000,NULL,0,'М2 ОБЩ ПЛ'),(22,NULL,2,'м2 жил. пл','квадратный метр жилой площади',0,1.000000000,NULL,0,'М2 ЖИЛ ПЛ'),(23,NULL,3,'л','литр',0,1.000000000,NULL,1,'LTR'),(24,23,3,'дм3','кубический дециметр',0,1.000000000,NULL,1,'DMQ'),(25,23,3,'мм3','кубический миллиметр',0,0.000001000,NULL,1,'MMQ'),(26,23,3,'см3','кубический сантиметр',0,0.000010000,NULL,1,'CMQ'),(27,23,3,'мл','миллилитр',0,0.000010000,NULL,1,'MLT'),(28,23,3,'м3','кубический метр',0,1000.000000000,NULL,1,'MTQ'),(29,23,3,'дл','децилитр',0,0.100000000,NULL,1,'DLT'),(30,23,3,'гл','гектолитр',0,100.000000000,NULL,1,'HLT'),(31,23,3,'Мл','мегалитр',0,1000000.000000000,NULL,1,'MAL'),(32,NULL,7,'шт','штука',0,1.000000000,NULL,1,'PCE'),(33,NULL,4,'кг','килограмм',0,1.000000000,NULL,1,'KGM');
/*!40000 ALTER TABLE `m_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_units_countries`
--

DROP TABLE IF EXISTS `m_units_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_units_countries` (
  `country_id` int(11) NOT NULL COMMENT 'идентификатор страны',
  `m_unit_id` int(11) NOT NULL COMMENT 'идентификатор единицы измерения',
  `nat_symbol` varchar(20) NOT NULL COMMENT 'национальное условное обозначение',
  `nat_code` varchar(20) NOT NULL COMMENT 'национальное кодовое буквенное обозначение',
  `nat_name` varchar(255) NOT NULL COMMENT 'национальное наименование единицы измерения',
  UNIQUE KEY `UK_m_units_countries` (`country_id`,`m_unit_id`),
  KEY `FK_m_units_countries_m_unit_id` (`m_unit_id`),
  CONSTRAINT `FK_m_units_countries_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_m_units_countries_m_unit_id` FOREIGN KEY (`m_unit_id`) REFERENCES `m_units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='национальные параметры единиц измерения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_units_countries`
--

LOCK TABLES `m_units_countries` WRITE;
/*!40000 ALTER TABLE `m_units_countries` DISABLE KEYS */;
INSERT INTO `m_units_countries` VALUES (1,1,'м','М','метр'),(1,2,'мм','ММ','миллиметр'),(1,3,'см','СМ','сантиметр'),(1,4,'дм','ДМ','дециметр'),(1,5,'км','КМ','километр'),(1,6,'Мм','МЕГАМ','мегаметр'),(1,7,'дюйм','ДЮЙМ','дюйм'),(1,8,'фут','ФУТ','фут'),(1,9,'миля','МИЛЬ','морская миля'),(1,10,'пог. м','ПОГ М','погонный метр'),(1,11,'усл. м','УСЛ М','условный метр'),(1,12,'ярд','ЯРД','ярд'),(1,13,'м2','М2','квадратный метр'),(1,14,'мм2','ММ2','квадратный миллиметр'),(1,15,'см2','СМ2','квадратный сантиметр'),(1,16,'дм2','ДМ2','квадратный дециметр'),(1,17,'км2','КМ2','квадратный километр'),(1,18,'га','ГА','гектар'),(1,19,'а','ар','ар'),(1,20,'усл. м2','УСЛ М2','условный квадратный метр'),(1,21,'м2 общ. пл','М2 ОБЩ ПЛ','квадратный метр общей площади'),(1,22,'м2 жил. пл','М2 ЖИЛ ПЛ','квадратный метр жилой площади'),(1,23,'л','Л','литр'),(1,32,'шт','ШТ','штука'),(1,33,'кг','КГ','килограмм');
/*!40000 ALTER TABLE `m_units_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_units_groups`
--

DROP TABLE IF EXISTS `m_units_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_units_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор группы единиц измерения',
  `parent_id` int(11) DEFAULT NULL COMMENT 'идентификатор родительской группы',
  `iname` varchar(255) NOT NULL COMMENT 'название группы единиц измерения (интернациональное)',
  `is_old` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'запись предварительно удалена',
  `iname_translations_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор группы перевода на другие языки для iname',
  PRIMARY KEY (`id`),
  KEY `FK_m_units_groups_iname_translations_group_id` (`iname_translations_group_id`),
  CONSTRAINT `FK_m_units_groups_iname_translations_group_id` FOREIGN KEY (`iname_translations_group_id`) REFERENCES `translations_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='группировка единиц измерения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_units_groups`
--

LOCK TABLES `m_units_groups` WRITE;
/*!40000 ALTER TABLE `m_units_groups` DISABLE KEYS */;
INSERT INTO `m_units_groups` VALUES (1,NULL,'Unit of length',0,1),(2,NULL,'Units of area',0,2),(3,NULL,'Units of volume',0,3),(4,NULL,'Units of mass',0,4),(5,NULL,'Engineering unit',0,5),(6,NULL,'Time units',0,6),(7,NULL,'Economic units',0,7);
/*!40000 ALTER TABLE `m_units_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_groups`
--

DROP TABLE IF EXISTS `products_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор группы продуктов',
  `parent_id` int(11) DEFAULT NULL COMMENT 'идентификатор родительской группы',
  `name` varchar(255) NOT NULL COMMENT 'название группы на оригинальном языке для классификатора (указан в classifier)',
  `is_old` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'запись предварительно удалена',
  `is_has_childs` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'есть ли потомки у группы',
  `m_unit_id` int(11) DEFAULT NULL COMMENT 'идентификатор основной единицы измерения',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'создатель записи',
  `classifier_id` int(11) NOT NULL COMMENT 'идентификатор классификатора',
  `name_translations_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор группы перевода для name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_products_groups` (`classifier_id`,`name`),
  KEY `FK_products_groups_m_unit_id` (`m_unit_id`),
  KEY `FK_products_groups_classifier_id` (`classifier_id`),
  KEY `FK_products_groups_user_id` (`users_session_id`),
  KEY `FK_products_groups_name_translations_group_id` (`name_translations_group_id`),
  CONSTRAINT `FK_products_groups_classifier_id` FOREIGN KEY (`classifier_id`) REFERENCES `classifiers` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_m_unit_id` FOREIGN KEY (`m_unit_id`) REFERENCES `m_units` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_name_translations_group_id` FOREIGN KEY (`name_translations_group_id`) REFERENCES `translations_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='группы продуктов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_groups`
--

LOCK TABLES `products_groups` WRITE;
/*!40000 ALTER TABLE `products_groups` DISABLE KEYS */;
INSERT INTO `products_groups` VALUES (1,NULL,'',0,0,NULL,NULL,1,NULL),(2,NULL,'',0,0,NULL,NULL,2,NULL),(3,NULL,'',0,0,NULL,NULL,3,NULL),(4,NULL,'',0,0,NULL,NULL,4,NULL),(27,2,'молоко',0,0,23,128,2,NULL),(28,2,'пакет майка',0,0,32,130,2,NULL),(29,2,'кисломолочный продукт',0,0,32,130,2,NULL),(30,2,'кисель',0,0,32,130,2,NULL),(31,2,'десерт',0,0,32,130,2,NULL),(32,2,'сок',0,0,32,130,2,NULL),(33,2,'орех',0,0,32,130,2,NULL),(34,2,'гранат',0,0,33,130,2,NULL),(35,2,'гранат без косточек',0,0,33,130,2,NULL),(36,2,'огурец',0,0,33,130,2,NULL),(37,2,'сервелат',0,0,33,130,2,NULL),(38,2,'крылья куринные',0,0,33,130,2,NULL),(39,2,'сыр',0,0,33,130,2,NULL),(40,2,'тесто',0,0,33,130,2,NULL),(41,2,'капуста белокочанная',0,0,33,130,2,NULL),(42,2,'мясо',0,0,33,334,2,NULL),(43,2,'хлеб',0,0,33,465,2,NULL),(44,2,'овощи',0,0,33,465,2,NULL),(45,2,'салат',0,0,33,465,2,NULL),(46,2,'паштет',0,0,33,465,2,NULL),(47,2,'яйцо',0,0,32,465,2,NULL);
/*!40000 ALTER TABLE `products_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_groups_ext`
--

DROP TABLE IF EXISTS `products_groups_ext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_groups_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор свойств группы товаров',
  `products_group_id` int(11) NOT NULL COMMENT 'идентификатор группы товаров',
  `country_id` int(11) NOT NULL COMMENT 'идентификатор страны',
  `storage_id` int(11) DEFAULT NULL COMMENT 'идентификатор хранилища дополнительных параметров',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_product_groups_ext_group_id` (`products_group_id`),
  KEY `FK_products_groups_ext_country_id` (`country_id`),
  KEY `FK_products_groups_ext_storage_id` (`storage_id`),
  CONSTRAINT `FK_products_groups_ext_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_ext_products_group_id` FOREIGN KEY (`products_group_id`) REFERENCES `products_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_ext_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='дополнительные свойства группы товаров для конкретной страны';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_groups_ext`
--

LOCK TABLES `products_groups_ext` WRITE;
/*!40000 ALTER TABLE `products_groups_ext` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_groups_ext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_groups_synonims`
--

DROP TABLE IF EXISTS `products_groups_synonims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_groups_synonims` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор синонима',
  `products_group_id` int(11) NOT NULL COMMENT 'идентификатор группы для которой действует синоним',
  `synonim` varchar(255) NOT NULL COMMENT 'синоним',
  `language_id` int(11) NOT NULL COMMENT 'идентификатор языка',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_products_groups_synonims_synonim` (`synonim`),
  KEY `FK_products_groups_synonims_products_group_id` (`products_group_id`),
  KEY `FK_products_groups_synonims_language_id` (`language_id`),
  CONSTRAINT `FK_products_groups_synonims_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_products_groups_synonims_products_group_id` FOREIGN KEY (`products_group_id`) REFERENCES `products_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='синонимы для названий групп товаров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_groups_synonims`
--

LOCK TABLES `products_groups_synonims` WRITE;
/*!40000 ALTER TABLE `products_groups_synonims` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_groups_synonims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage`
--

DROP TABLE IF EXISTS `storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='хранилище произвольных параметров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage`
--

LOCK TABLES `storage` WRITE;
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage_int11`
--

DROP TABLE IF EXISTS `storage_int11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_int11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор параметра',
  `storage_id` int(11) NOT NULL COMMENT 'идентификатор хранилища',
  `value` int(11) DEFAULT NULL COMMENT 'хранимое значение',
  PRIMARY KEY (`id`),
  KEY `FK_storage_int11_storage_id` (`storage_id`),
  CONSTRAINT `FK_storage_int11_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='хранение параметров INT(11)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage_int11`
--

LOCK TABLES `storage_int11` WRITE;
/*!40000 ALTER TABLE `storage_int11` DISABLE KEYS */;
/*!40000 ALTER TABLE `storage_int11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage_varchar255`
--

DROP TABLE IF EXISTS `storage_varchar255`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_varchar255` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `storage_id` int(11) NOT NULL COMMENT 'идентификатор хранилища',
  `value` varchar(255) DEFAULT NULL COMMENT 'хранимое значение',
  PRIMARY KEY (`id`),
  KEY `FK_storage_varchar255_storage_id` (`storage_id`),
  CONSTRAINT `FK_storage_varchar255_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='хранилище для VARCHAR(255)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage_varchar255`
--

LOCK TABLES `storage_varchar255` WRITE;
/*!40000 ALTER TABLE `storage_varchar255` DISABLE KEYS */;
/*!40000 ALTER TABLE `storage_varchar255` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `language_id` int(11) NOT NULL COMMENT 'идентификатор языка для построения перевода',
  `translation` varchar(255) DEFAULT NULL COMMENT 'перевод наименования iname',
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор записи',
  `translations_group_id` int(11) NOT NULL COMMENT 'идентификатор группы перевода',
  PRIMARY KEY (`id`),
  KEY `IX_translations_language_id` (`language_id`),
  KEY `FK_translations_translations_group_id` (`translations_group_id`),
  CONSTRAINT `FK_translations_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_translations_translations_group_id` FOREIGN KEY (`translations_group_id`) REFERENCES `translations_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='переводы наименований';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'Единицы измерения длины',1,1),(1,'Единицы измерения площади',2,2),(1,'Единицы измерения объема',3,3),(1,'Единицы измерения массы',4,4),(1,'Технические единицы измерения',5,5),(1,'Единицы измерения времени',6,6),(1,'Экономические единицы измерения',7,7);
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations_groups`
--

DROP TABLE IF EXISTS `translations_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор группы',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='группы языкового перевода для наименований';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations_groups`
--

LOCK TABLES `translations_groups` WRITE;
/*!40000 ALTER TABLE `translations_groups` DISABLE KEYS */;
INSERT INTO `translations_groups` VALUES (1),(2),(3),(4),(5),(6),(7);
/*!40000 ALTER TABLE `translations_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `units_russia`
--

DROP TABLE IF EXISTS `units_russia`;
/*!50001 DROP VIEW IF EXISTS `units_russia`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `units_russia` (
  `id` tinyint NOT NULL,
  `parent_id` tinyint NOT NULL,
  `m_unit_group_id` tinyint NOT NULL,
  `multiplier` tinyint NOT NULL,
  `is_international` tinyint NOT NULL,
  `code` tinyint NOT NULL,
  `symbol` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `login` varchar(20) NOT NULL COMMENT 'логин пользователя для аутентификации в системе',
  `passw` varchar(32) NOT NULL COMMENT 'md5 хэш пароль пользователя для аутентификации в системе',
  `email` varchar(255) NOT NULL COMMENT 'e-mail пользователя',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания записи',
  `is_old` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'запись предварительно удалена',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор группы пользователя',
  `date_update` timestamp NULL DEFAULT NULL COMMENT 'дата последнего изменения',
  `date_group_update` timestamp NULL DEFAULT NULL COMMENT 'дата последней смены группы',
  `country_id` int(11) NOT NULL COMMENT 'идентификатор страны',
  `language_id` int(11) NOT NULL COMMENT 'идентификатор языка',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `login` (`login`),
  KEY `FK_users_users_group_id` (`users_group_id`),
  KEY `FK_users_country_id` (`country_id`),
  KEY `FK_users_language_id` (`language_id`),
  CONSTRAINT `FK_users_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_users_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_users_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='регистрационные данные пользователей системы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (10,'noname10','202cb962ac59075b964b07152d234b70','alexmd_2004@mail.ru','0000-00-00 00:00:00',0,12,'2013-01-13 18:47:13','0000-00-00 00:00:00',1,1),(11,'noname11','202cb962ac59075b964b07152d234b70','a@a','0000-00-00 00:00:00',0,13,'2013-01-13 18:47:20','0000-00-00 00:00:00',1,1),(12,'noname12','202cb962ac59075b964b07152d234b70','b@b','0000-00-00 00:00:00',0,14,'2013-01-14 03:50:16','0000-00-00 00:00:00',1,1),(13,'noname13','202cb962ac59075b964b07152d234b70','d@d','2013-01-25 17:11:14',0,15,NULL,NULL,1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_checks`
--

DROP TABLE IF EXISTS `users_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор чека',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'идентификатор сессии ввода, в которой был введен чек',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользовательской группы',
  `byer_user_id` int(11) DEFAULT NULL COMMENT 'кто купил',
  `users_place_id` int(11) DEFAULT NULL COMMENT 'место покупки (может быть не указано)',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания',
  `date_update` timestamp NULL DEFAULT NULL COMMENT 'дата последней правки',
  `date_purchase` varchar(255) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'дата покупки',
  PRIMARY KEY (`id`),
  KEY `FK_users_checks_users_input_id` (`users_session_id`),
  KEY `FK_users_checks_users_group_id` (`users_group_id`),
  KEY `FK_users_checks_users_place_id` (`users_place_id`),
  KEY `FK_users_checks_byer_user_id` (`byer_user_id`),
  CONSTRAINT `FK_users_checks_byer_user_id` FOREIGN KEY (`byer_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_checks_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_checks_users_place_id` FOREIGN KEY (`users_place_id`) REFERENCES `users_places` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_checks_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='чеки пользовательской группы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_checks`
--

LOCK TABLES `users_checks` WRITE;
/*!40000 ALTER TABLE `users_checks` DISABLE KEYS */;
INSERT INTO `users_checks` VALUES (98,428,12,10,23,'2013-01-30 09:42:37',NULL,'1970-01-17 00:38:58'),(99,429,12,10,23,'2013-01-30 09:43:54',NULL,'1970-01-17 00:38:59'),(100,465,12,10,24,'2013-02-03 09:23:38',NULL,'1970-01-17 00:44:43'),(101,465,12,10,24,'2013-02-03 09:25:16',NULL,'1970-01-17 00:44:43'),(102,465,12,10,24,'2013-02-03 09:25:43',NULL,'1970-01-17 00:44:43'),(103,465,12,10,24,'2013-02-03 09:26:43',NULL,'1970-01-17 00:44:43');
/*!40000 ALTER TABLE `users_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор группы пользователя',
  `user_id` int(11) NOT NULL COMMENT 'создатель группы',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания группы',
  `date_update` timestamp NULL DEFAULT NULL COMMENT 'дата изменения записи',
  `is_old` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'запись предварительно удалена',
  `classifier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_users_groups_user_id` (`user_id`),
  KEY `FK_users_groups_classifier_id` (`classifier_id`),
  CONSTRAINT `FK_users_groups_classifier_id` FOREIGN KEY (`classifier_id`) REFERENCES `classifiers` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='группы пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (12,10,'0000-00-00 00:00:00','2013-01-13 18:47:13',0,2),(13,11,'0000-00-00 00:00:00','2013-01-13 18:47:20',0,3),(14,12,'0000-00-00 00:00:00','2013-01-14 03:50:16',0,4),(15,13,'2013-01-25 17:11:14',NULL,0,1);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_places`
--

DROP TABLE IF EXISTS `users_places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_places` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор места',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользовательской группы',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'владелец записи',
  `name` varchar(255) NOT NULL COMMENT 'наименование места',
  `parent_id` int(11) DEFAULT NULL COMMENT 'родительская запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_users_places` (`users_group_id`,`name`),
  KEY `FK_users_places_users_group_id` (`users_group_id`),
  KEY `FK_users_places_user_id` (`users_session_id`),
  CONSTRAINT `FK_users_places_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_places_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=862 COMMENT='места покупок или предоставления услуг';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_places`
--

LOCK TABLES `users_places` WRITE;
/*!40000 ALTER TABLE `users_places` DISABLE KEYS */;
INSERT INTO `users_places` VALUES (21,12,128,'Сантимо',NULL),(22,12,130,'',NULL),(23,12,139,'Сибириада',NULL),(24,12,465,'Продсиб(торговый)',NULL);
/*!40000 ALTER TABLE `users_places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_products`
--

DROP TABLE IF EXISTS `users_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентфикатор продукта',
  `name` varchar(255) NOT NULL COMMENT 'наименование',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользовательской группы',
  `users_session_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользователя добавившего запись',
  `products_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор группы продукта из основного классификатора',
  `m_unit_id` int(11) DEFAULT NULL COMMENT 'идентификатор единицы измерения',
  `last_price` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT 'последняя актуальная стоимость',
  `currency_id` int(11) DEFAULT NULL COMMENT 'актуальная валюта для продукта',
  PRIMARY KEY (`id`),
  KEY `FK_users_products_users_group_id` (`users_group_id`),
  KEY `FK_users_products_user_id` (`users_session_id`),
  KEY `FK_users_products_currency_id` (`currency_id`),
  KEY `FK_users_products_m_unit_id` (`m_unit_id`),
  KEY `FK_users_products_products_group_id` (`products_group_id`),
  CONSTRAINT `FK_users_products_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_products_m_unit_id` FOREIGN KEY (`m_unit_id`) REFERENCES `m_units` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_products_products_group_id` FOREIGN KEY (`products_group_id`) REFERENCES `products_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_products_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_products_users_session_id` FOREIGN KEY (`users_session_id`) REFERENCES `users_sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='продукты и услуги для пользовательской группы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_products`
--

LOCK TABLES `users_products` WRITE;
/*!40000 ALTER TABLE `users_products` DISABLE KEYS */;
INSERT INTO `users_products` VALUES (24,'для всей семьи',12,128,27,23,36.00,1),(25,'веселый молочник',12,128,27,23,41.00,1),(26,'42*59',12,130,28,32,4.60,1),(27,'ИМУНЕЛЕ',12,130,29,32,14.50,1),(28,'фрутоняня вишня',12,130,30,32,14.40,1),(29,'РИЧ груша и яблоко',12,130,31,32,37.60,1),(30,'агуша груша с мякотью',12,130,32,32,16.10,1),(31,'НАТСИ арахис обжаренный',12,130,33,32,70.50,1),(32,'без косточек',12,130,34,33,170.00,1),(33,'ДВС',12,130,27,23,36.00,1),(34,'РИЧ груша и яблоки',12,130,31,32,37.60,1),(35,'',12,130,35,33,170.00,1),(36,'ташкент',12,130,36,33,173.00,1),(37,'СПК',12,130,37,33,294.00,1),(38,'',12,130,38,33,125.50,1),(39,'ламбер',12,130,39,33,349.00,1),(40,'БХК пирожковое',12,130,40,33,29.80,1),(41,'',12,130,41,33,17.60,1),(42,'баранина',12,334,42,33,150.00,1),(43,'бхк пшеничный',12,465,43,33,18.80,1),(44,'восход гречишный',12,465,43,33,37.40,1),(45,'есть идея мекс',12,465,44,33,45.20,1),(46,'сангусто морская капуста',12,465,45,33,41.30,1),(47,'хаме гус. печень',12,465,46,33,23.70,1),(48,'хаме гус. с ша',12,465,46,33,25.40,1),(49,'хаме мясо птицы',12,465,46,33,23.70,1),(50,'НС 1С пл/конт.',12,465,47,32,39.60,1);
/*!40000 ALTER TABLE `users_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_purchases`
--

DROP TABLE IF EXISTS `users_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор покупки',
  `users_check_id` int(11) NOT NULL COMMENT 'идентификатор чека',
  `users_group_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользовательской группы',
  `quantity` decimal(6,3) DEFAULT NULL COMMENT 'количество  (для услуг может быть не указано)',
  `price` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT 'цена',
  `currency_id` int(11) DEFAULT NULL COMMENT 'идентификатор валюты',
  `m_unit_id` int(11) DEFAULT NULL COMMENT 'единица измерения',
  `users_product_id` int(11) DEFAULT NULL COMMENT 'наименование ',
  PRIMARY KEY (`id`),
  KEY `FK_users_purchases_users_check_id` (`users_check_id`),
  KEY `FK_users_purchases_users_group_id` (`users_group_id`),
  KEY `FK_users_purchases_m_unit_id` (`m_unit_id`),
  KEY `FK_users_purchases_currency_id` (`currency_id`),
  KEY `FK_users_purchases_users_product_id` (`users_product_id`),
  CONSTRAINT `FK_users_purchases_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_purchases_m_unit_id` FOREIGN KEY (`m_unit_id`) REFERENCES `m_units` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_purchases_users_check_id` FOREIGN KEY (`users_check_id`) REFERENCES `users_checks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_users_purchases_users_group_id` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_users_purchases_users_product_id` FOREIGN KEY (`users_product_id`) REFERENCES `users_products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COMMENT='продукты и услуги добавленные в чек пользователем';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_purchases`
--

LOCK TABLES `users_purchases` WRITE;
/*!40000 ALTER TABLE `users_purchases` DISABLE KEYS */;
INSERT INTO `users_purchases` VALUES (87,98,12,1.000,23.00,1,23,33),(88,99,12,1.000,36.00,1,23,33),(89,100,12,0.400,18.80,1,33,43),(90,101,12,0.400,18.80,1,33,43),(91,101,12,0.300,37.40,1,33,44),(92,101,12,0.400,45.20,1,33,45),(93,101,12,0.400,41.30,1,33,46),(94,102,12,0.117,23.70,1,33,47),(95,103,12,0.117,25.40,1,33,48),(96,103,12,0.117,23.70,1,33,49),(97,103,12,10.000,39.60,1,32,50);
/*!40000 ALTER TABLE `users_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_sessions`
--

DROP TABLE IF EXISTS `users_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор сессии',
  `user_id` int(11) DEFAULT NULL COMMENT 'идентификатор пользователя',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания записи',
  `date_update` timestamp NULL DEFAULT NULL COMMENT 'дата обновления сессии',
  PRIMARY KEY (`id`),
  KEY `FK_users_inputs_user_id` (`user_id`),
  CONSTRAINT `FK_users_sessions_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 COMMENT='пользовательские сессии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_sessions`
--

LOCK TABLES `users_sessions` WRITE;
/*!40000 ALTER TABLE `users_sessions` DISABLE KEYS */;
INSERT INTO `users_sessions` VALUES (2,11,'2013-01-22 14:07:35',NULL),(3,10,'2013-01-22 14:10:10',NULL),(4,10,'2013-01-22 14:18:38',NULL),(5,10,'2013-01-22 14:20:00',NULL),(6,10,'2013-01-22 14:20:06',NULL),(7,10,'2013-01-22 14:20:12',NULL),(8,10,'2013-01-22 17:08:08',NULL),(9,10,'2013-01-22 17:17:21',NULL),(10,10,'2013-01-22 17:17:41',NULL),(11,10,'2013-01-22 17:17:50',NULL),(12,10,'2013-01-22 17:18:05',NULL),(13,10,'2013-01-22 17:18:48',NULL),(14,10,'2013-01-23 07:13:51',NULL),(15,10,'2013-01-23 09:40:44',NULL),(16,10,'2013-01-23 17:47:00',NULL),(17,12,'2013-01-23 17:47:46',NULL),(18,12,'2013-01-23 17:50:23',NULL),(19,12,'2013-01-23 17:55:28',NULL),(20,12,'2013-01-23 17:58:12',NULL),(21,12,'2013-01-23 17:58:58',NULL),(22,12,'2013-01-23 18:00:08',NULL),(23,12,'2013-01-23 18:02:18',NULL),(24,12,'2013-01-23 18:04:04',NULL),(25,12,'2013-01-23 18:05:34',NULL),(26,12,'2013-01-23 18:07:55',NULL),(27,10,'2013-01-23 18:08:36',NULL),(28,10,'2013-01-23 18:09:58',NULL),(29,10,'2013-01-23 18:11:48',NULL),(30,10,'2013-01-23 18:12:31',NULL),(31,10,'2013-01-23 18:14:23',NULL),(32,10,'2013-01-23 18:16:27',NULL),(33,10,'2013-01-24 04:07:33',NULL),(34,10,'2013-01-24 04:08:22',NULL),(35,10,'2013-01-24 04:10:01',NULL),(36,10,'2013-01-24 04:11:19',NULL),(37,10,'2013-01-24 04:13:02',NULL),(38,10,'2013-01-24 04:14:07',NULL),(39,10,'2013-01-24 04:14:13',NULL),(40,10,'2013-01-24 04:16:04',NULL),(41,10,'2013-01-24 04:17:15',NULL),(42,10,'2013-01-24 04:20:13',NULL),(43,10,'2013-01-24 04:20:36',NULL),(44,10,'2013-01-24 04:23:32',NULL),(45,10,'2013-01-24 04:24:42',NULL),(46,10,'2013-01-24 04:38:33',NULL),(47,10,'2013-01-24 04:41:49',NULL),(48,10,'2013-01-24 05:33:22',NULL),(49,10,'2013-01-24 05:40:28',NULL),(50,10,'2013-01-24 05:41:11',NULL),(51,10,'2013-01-24 07:06:45',NULL),(52,10,'2013-01-24 07:14:59',NULL),(53,10,'2013-01-24 07:17:41',NULL),(54,10,'2013-01-24 07:27:10',NULL),(55,10,'2013-01-24 07:42:43',NULL),(56,10,'2013-01-24 08:41:23',NULL),(57,10,'2013-01-24 08:41:41',NULL),(58,10,'2013-01-24 08:42:17',NULL),(59,10,'2013-01-24 08:43:19',NULL),(60,10,'2013-01-24 08:53:13',NULL),(61,10,'2013-01-24 08:53:20',NULL),(62,10,'2013-01-24 08:54:00',NULL),(63,10,'2013-01-24 08:55:17',NULL),(64,10,'2013-01-24 08:56:58',NULL),(65,10,'2013-01-24 09:27:25',NULL),(66,10,'2013-01-24 09:56:17',NULL),(67,10,'2013-01-24 10:00:16',NULL),(68,10,'2013-01-24 10:02:08',NULL),(69,10,'2013-01-24 10:05:57',NULL),(70,10,'2013-01-24 10:18:15',NULL),(71,10,'2013-01-24 10:20:22',NULL),(72,10,'2013-01-24 14:15:58',NULL),(73,10,'2013-01-24 15:25:08',NULL),(74,10,'2013-01-24 15:37:26',NULL),(75,10,'2013-01-24 15:37:45',NULL),(76,10,'2013-01-24 15:38:10',NULL),(77,10,'2013-01-24 15:38:27',NULL),(78,10,'2013-01-24 15:41:26',NULL),(79,10,'2013-01-24 15:43:12',NULL),(80,10,'2013-01-24 15:44:36',NULL),(81,10,'2013-01-24 15:47:57',NULL),(82,10,'2013-01-24 16:05:12',NULL),(83,10,'2013-01-24 16:20:26',NULL),(84,11,'2013-01-24 16:35:13',NULL),(85,10,'2013-01-25 04:21:32',NULL),(86,10,'2013-01-25 09:21:38',NULL),(87,10,'2013-01-25 09:31:10',NULL),(88,10,'2013-01-25 09:31:38',NULL),(89,10,'2013-01-25 09:32:34',NULL),(90,10,'2013-01-25 09:35:15',NULL),(91,10,'2013-01-25 09:40:30',NULL),(92,10,'2013-01-25 09:40:59',NULL),(93,10,'2013-01-25 09:44:07',NULL),(94,10,'2013-01-25 09:44:46',NULL),(95,10,'2013-01-25 09:59:35',NULL),(96,10,'2013-01-25 10:04:36',NULL),(97,10,'2013-01-25 10:12:26',NULL),(98,10,'2013-01-25 13:15:20',NULL),(99,10,'2013-01-25 18:26:50',NULL),(100,10,'2013-01-25 19:23:54',NULL),(101,10,'2013-01-25 19:25:42',NULL),(102,10,'2013-01-25 19:26:30',NULL),(103,10,'2013-01-25 19:27:01',NULL),(104,10,'2013-01-25 19:28:27',NULL),(105,10,'2013-01-25 19:31:30',NULL),(106,10,'2013-01-25 19:35:42',NULL),(107,10,'2013-01-25 19:40:33',NULL),(108,10,'2013-01-25 19:41:21',NULL),(109,10,'2013-01-25 19:41:55',NULL),(110,10,'2013-01-25 19:42:38',NULL),(111,10,'2013-01-25 19:42:59',NULL),(112,10,'2013-01-26 03:30:55',NULL),(113,10,'2013-01-26 03:41:26',NULL),(114,10,'2013-01-26 07:39:14',NULL),(115,10,'2013-01-26 10:05:33',NULL),(116,10,'2013-01-26 14:19:35',NULL),(117,10,'2013-01-26 15:45:07',NULL),(118,10,'2013-01-26 17:32:45',NULL),(119,10,'2013-01-26 18:06:07',NULL),(120,10,'2013-01-26 18:07:22',NULL),(121,10,'2013-01-26 18:16:37',NULL),(122,10,'2013-01-26 18:17:20',NULL),(123,10,'2013-01-26 18:32:08',NULL),(124,10,'2013-01-26 18:50:20',NULL),(125,10,'2013-01-27 06:55:13',NULL),(126,10,'2013-01-27 07:22:02',NULL),(127,10,'2013-01-27 08:56:29',NULL),(128,10,'2013-01-27 09:15:01',NULL),(129,10,'2013-01-27 09:53:48',NULL),(130,10,'2013-01-27 10:03:54',NULL),(131,10,'2013-01-27 10:50:56',NULL),(132,10,'2013-01-27 14:24:46',NULL),(133,10,'2013-01-27 14:26:49',NULL),(134,10,'2013-01-27 15:14:08',NULL),(135,10,'2013-01-27 15:22:46',NULL),(136,10,'2013-01-27 15:22:59',NULL),(137,10,'2013-01-27 15:27:00',NULL),(138,10,'2013-01-27 15:32:32',NULL),(139,10,'2013-01-27 15:34:23',NULL),(140,10,'2013-01-28 06:48:05',NULL),(141,10,'2013-01-28 07:07:11',NULL),(142,10,'2013-01-28 07:13:58',NULL),(143,10,'2013-01-28 07:25:28',NULL),(144,10,'2013-01-28 07:26:10',NULL),(145,10,'2013-01-28 07:26:34',NULL),(146,10,'2013-01-28 07:28:42',NULL),(147,10,'2013-01-28 07:32:51',NULL),(148,10,'2013-01-28 07:33:29',NULL),(149,10,'2013-01-28 07:50:17',NULL),(150,10,'2013-01-28 07:51:06',NULL),(151,10,'2013-01-28 07:51:32',NULL),(152,10,'2013-01-28 07:59:33',NULL),(153,10,'2013-01-28 08:02:09',NULL),(154,10,'2013-01-28 08:03:18',NULL),(155,10,'2013-01-28 08:07:55',NULL),(156,10,'2013-01-28 08:15:10',NULL),(157,10,'2013-01-28 08:15:35',NULL),(158,10,'2013-01-28 08:17:36',NULL),(159,10,'2013-01-28 08:17:50',NULL),(160,10,'2013-01-28 08:18:52',NULL),(161,10,'2013-01-28 08:19:24',NULL),(162,10,'2013-01-28 08:19:50',NULL),(163,10,'2013-01-28 08:22:08',NULL),(164,10,'2013-01-28 08:22:40',NULL),(165,10,'2013-01-28 08:23:01',NULL),(166,10,'2013-01-28 08:27:49',NULL),(167,10,'2013-01-28 08:28:37',NULL),(168,10,'2013-01-28 08:29:11',NULL),(169,10,'2013-01-28 08:29:55',NULL),(170,10,'2013-01-28 08:42:13',NULL),(171,10,'2013-01-28 08:42:44',NULL),(172,10,'2013-01-28 08:43:56',NULL),(173,10,'2013-01-28 08:54:34',NULL),(174,10,'2013-01-28 09:00:14',NULL),(175,10,'2013-01-28 09:01:54',NULL),(176,10,'2013-01-28 09:03:18',NULL),(177,10,'2013-01-28 09:18:16',NULL),(178,10,'2013-01-28 09:21:46',NULL),(179,10,'2013-01-28 09:39:27',NULL),(180,10,'2013-01-28 09:50:38',NULL),(181,10,'2013-01-28 10:07:25',NULL),(182,10,'2013-01-28 10:11:14',NULL),(183,10,'2013-01-28 10:12:30',NULL),(184,10,'2013-01-28 10:14:04',NULL),(185,10,'2013-01-28 10:15:13',NULL),(186,10,'2013-01-28 10:16:16',NULL),(187,10,'2013-01-28 10:17:36',NULL),(188,10,'2013-01-28 10:19:57',NULL),(189,10,'2013-01-28 13:47:35',NULL),(190,10,'2013-01-28 14:11:43',NULL),(191,10,'2013-01-28 14:14:16',NULL),(192,10,'2013-01-28 14:16:01',NULL),(193,10,'2013-01-28 14:17:22',NULL),(194,10,'2013-01-28 14:25:53',NULL),(195,10,'2013-01-28 14:27:13',NULL),(196,10,'2013-01-28 14:30:20',NULL),(197,10,'2013-01-28 15:24:58',NULL),(198,10,'2013-01-28 15:26:18',NULL),(199,10,'2013-01-28 15:26:47',NULL),(200,10,'2013-01-28 15:27:17',NULL),(201,10,'2013-01-28 15:28:47',NULL),(202,10,'2013-01-28 15:29:19',NULL),(203,10,'2013-01-28 15:30:49',NULL),(204,10,'2013-01-28 15:31:57',NULL),(205,10,'2013-01-28 15:39:26',NULL),(206,10,'2013-01-28 15:39:59',NULL),(207,10,'2013-01-28 15:41:44',NULL),(208,10,'2013-01-28 15:42:36',NULL),(209,10,'2013-01-28 15:56:56',NULL),(210,10,'2013-01-28 16:00:40',NULL),(211,10,'2013-01-28 16:02:14',NULL),(212,10,'2013-01-28 16:02:50',NULL),(213,10,'2013-01-28 16:03:44',NULL),(214,10,'2013-01-28 16:29:26',NULL),(215,10,'2013-01-29 03:19:59',NULL),(216,10,'2013-01-29 03:30:28',NULL),(217,10,'2013-01-29 03:30:58',NULL),(218,10,'2013-01-29 03:31:53',NULL),(219,10,'2013-01-29 03:33:18',NULL),(220,10,'2013-01-29 03:34:14',NULL),(221,10,'2013-01-29 03:34:59',NULL),(222,10,'2013-01-29 03:35:48',NULL),(223,10,'2013-01-29 03:37:00',NULL),(224,10,'2013-01-29 03:43:14',NULL),(225,10,'2013-01-29 03:45:50',NULL),(226,10,'2013-01-29 04:01:02',NULL),(227,10,'2013-01-29 04:04:11',NULL),(228,10,'2013-01-29 04:04:29',NULL),(229,10,'2013-01-29 04:24:11',NULL),(230,10,'2013-01-29 04:33:17',NULL),(231,10,'2013-01-29 04:37:55',NULL),(232,10,'2013-01-29 04:39:15',NULL),(233,10,'2013-01-29 04:47:11',NULL),(234,10,'2013-01-29 04:51:36',NULL),(235,10,'2013-01-29 04:52:14',NULL),(236,10,'2013-01-29 04:52:47',NULL),(237,10,'2013-01-29 04:53:35',NULL),(238,10,'2013-01-29 05:01:09',NULL),(239,10,'2013-01-29 05:04:08',NULL),(240,10,'2013-01-29 05:23:19',NULL),(241,10,'2013-01-29 05:23:42',NULL),(242,10,'2013-01-29 05:26:03',NULL),(243,10,'2013-01-29 05:35:25',NULL),(244,10,'2013-01-29 05:38:39',NULL),(245,10,'2013-01-29 05:41:42',NULL),(246,10,'2013-01-29 05:44:16',NULL),(247,10,'2013-01-29 05:44:46',NULL),(248,10,'2013-01-29 05:45:10',NULL),(249,10,'2013-01-29 05:48:12',NULL),(250,10,'2013-01-29 05:52:40',NULL),(251,10,'2013-01-29 05:53:48',NULL),(252,10,'2013-01-29 05:54:28',NULL),(253,10,'2013-01-29 05:55:14',NULL),(254,10,'2013-01-29 05:55:43',NULL),(255,10,'2013-01-29 05:57:19',NULL),(256,10,'2013-01-29 05:58:33',NULL),(257,10,'2013-01-29 05:59:15',NULL),(258,10,'2013-01-29 06:48:26',NULL),(259,10,'2013-01-29 06:48:46',NULL),(260,10,'2013-01-29 06:49:18',NULL),(261,10,'2013-01-29 06:50:30',NULL),(262,10,'2013-01-29 06:52:13',NULL),(263,10,'2013-01-29 06:53:57',NULL),(264,10,'2013-01-29 06:58:01',NULL),(265,10,'2013-01-29 06:59:59',NULL),(266,10,'2013-01-29 07:01:09',NULL),(267,10,'2013-01-29 07:03:43',NULL),(268,10,'2013-01-29 07:04:04',NULL),(269,10,'2013-01-29 07:07:48',NULL),(270,10,'2013-01-29 07:08:10',NULL),(271,10,'2013-01-29 07:09:51',NULL),(272,10,'2013-01-29 07:11:27',NULL),(273,10,'2013-01-29 07:17:05',NULL),(274,10,'2013-01-29 07:17:22',NULL),(275,10,'2013-01-29 07:18:03',NULL),(276,10,'2013-01-29 07:22:28',NULL),(277,10,'2013-01-29 07:22:43',NULL),(278,10,'2013-01-29 07:23:05',NULL),(279,10,'2013-01-29 07:25:23',NULL),(280,10,'2013-01-29 07:28:22',NULL),(281,10,'2013-01-29 07:28:37',NULL),(282,10,'2013-01-29 07:29:17',NULL),(283,10,'2013-01-29 07:29:42',NULL),(284,10,'2013-01-29 07:33:56',NULL),(285,10,'2013-01-29 07:34:39',NULL),(286,10,'2013-01-29 07:35:14',NULL),(287,10,'2013-01-29 07:35:59',NULL),(288,10,'2013-01-29 07:36:32',NULL),(289,10,'2013-01-29 07:36:46',NULL),(290,10,'2013-01-29 07:37:02',NULL),(291,10,'2013-01-29 07:37:47',NULL),(292,10,'2013-01-29 07:55:19',NULL),(293,10,'2013-01-29 07:55:38',NULL),(294,10,'2013-01-29 07:58:47',NULL),(295,10,'2013-01-29 07:59:30',NULL),(296,10,'2013-01-29 08:00:03',NULL),(297,10,'2013-01-29 08:00:23',NULL),(298,10,'2013-01-29 08:03:13',NULL),(299,10,'2013-01-29 08:03:51',NULL),(300,10,'2013-01-29 08:11:02',NULL),(301,10,'2013-01-29 08:11:21',NULL),(302,10,'2013-01-29 08:13:27',NULL),(303,10,'2013-01-29 08:15:44',NULL),(304,10,'2013-01-29 08:16:16',NULL),(305,10,'2013-01-29 08:19:22',NULL),(306,10,'2013-01-29 08:40:42',NULL),(307,10,'2013-01-29 08:41:46',NULL),(308,10,'2013-01-29 08:42:12',NULL),(309,10,'2013-01-29 08:44:01',NULL),(310,10,'2013-01-29 08:48:41',NULL),(311,10,'2013-01-29 08:54:19',NULL),(312,10,'2013-01-29 08:58:01',NULL),(313,10,'2013-01-29 08:59:41',NULL),(314,10,'2013-01-29 09:01:13',NULL),(315,10,'2013-01-29 09:02:42',NULL),(316,10,'2013-01-29 09:08:23',NULL),(317,10,'2013-01-29 09:10:08',NULL),(318,10,'2013-01-29 09:12:27',NULL),(319,10,'2013-01-29 09:14:29',NULL),(320,10,'2013-01-29 09:22:47',NULL),(321,10,'2013-01-29 09:28:19',NULL),(322,10,'2013-01-29 09:29:08',NULL),(323,10,'2013-01-29 09:32:40',NULL),(324,10,'2013-01-29 09:36:02',NULL),(325,10,'2013-01-29 09:36:59',NULL),(326,10,'2013-01-29 09:39:48',NULL),(327,10,'2013-01-29 09:49:22',NULL),(328,10,'2013-01-29 09:50:39',NULL),(329,10,'2013-01-29 09:53:01',NULL),(330,10,'2013-01-29 09:53:29',NULL),(331,10,'2013-01-29 09:54:23',NULL),(332,10,'2013-01-29 09:54:39',NULL),(333,10,'2013-01-29 09:55:00',NULL),(334,10,'2013-01-29 09:56:57',NULL),(335,10,'2013-01-29 09:58:15',NULL),(336,10,'2013-01-29 10:01:45',NULL),(337,10,'2013-01-29 10:02:09',NULL),(338,10,'2013-01-29 10:02:45',NULL),(339,10,'2013-01-29 10:05:10',NULL),(340,10,'2013-01-29 10:05:41',NULL),(341,10,'2013-01-30 03:32:17',NULL),(342,10,'2013-01-30 04:00:20',NULL),(343,10,'2013-01-30 04:01:13',NULL),(344,10,'2013-01-30 04:06:54',NULL),(345,10,'2013-01-30 04:07:39',NULL),(346,10,'2013-01-30 04:08:25',NULL),(347,10,'2013-01-30 04:10:24',NULL),(348,10,'2013-01-30 04:23:57',NULL),(349,10,'2013-01-30 04:26:54',NULL),(350,10,'2013-01-30 04:27:29',NULL),(351,10,'2013-01-30 04:28:29',NULL),(352,10,'2013-01-30 04:29:21',NULL),(353,10,'2013-01-30 04:31:33',NULL),(354,10,'2013-01-30 04:31:54',NULL),(355,10,'2013-01-30 04:36:29',NULL),(356,10,'2013-01-30 04:37:05',NULL),(357,10,'2013-01-30 04:38:14',NULL),(358,10,'2013-01-30 04:46:14',NULL),(359,10,'2013-01-30 04:47:52',NULL),(360,10,'2013-01-30 05:03:19',NULL),(361,10,'2013-01-30 05:08:13',NULL),(362,10,'2013-01-30 05:43:31',NULL),(363,10,'2013-01-30 05:45:29',NULL),(364,10,'2013-01-30 05:45:57',NULL),(365,10,'2013-01-30 05:50:17',NULL),(366,10,'2013-01-30 06:02:41',NULL),(367,10,'2013-01-30 06:06:43',NULL),(368,10,'2013-01-30 06:09:22',NULL),(369,10,'2013-01-30 07:44:18',NULL),(370,10,'2013-01-30 07:47:10',NULL),(371,10,'2013-01-30 07:51:16',NULL),(372,10,'2013-01-30 07:52:45',NULL),(373,10,'2013-01-30 07:59:01',NULL),(374,10,'2013-01-30 08:04:12',NULL),(375,10,'2013-01-30 08:04:29',NULL),(376,10,'2013-01-30 08:05:19',NULL),(377,10,'2013-01-30 08:07:21',NULL),(378,10,'2013-01-30 08:08:57',NULL),(379,10,'2013-01-30 08:11:11',NULL),(380,10,'2013-01-30 08:12:20',NULL),(381,10,'2013-01-30 08:13:33',NULL),(382,10,'2013-01-30 08:15:22',NULL),(383,10,'2013-01-30 08:16:18',NULL),(384,10,'2013-01-30 08:16:38',NULL),(385,10,'2013-01-30 08:20:46',NULL),(386,10,'2013-01-30 08:24:08',NULL),(387,10,'2013-01-30 08:24:36',NULL),(388,10,'2013-01-30 08:26:04',NULL),(389,10,'2013-01-30 08:26:37',NULL),(390,10,'2013-01-30 08:30:49',NULL),(391,10,'2013-01-30 08:32:28',NULL),(392,10,'2013-01-30 08:33:10',NULL),(393,10,'2013-01-30 08:36:09',NULL),(394,10,'2013-01-30 08:36:36',NULL),(395,10,'2013-01-30 08:38:41',NULL),(396,10,'2013-01-30 08:39:07',NULL),(397,10,'2013-01-30 08:40:11',NULL),(398,10,'2013-01-30 08:41:21',NULL),(399,10,'2013-01-30 08:41:38',NULL),(400,10,'2013-01-30 08:42:15',NULL),(401,10,'2013-01-30 08:43:17',NULL),(402,10,'2013-01-30 08:46:11',NULL),(403,10,'2013-01-30 08:52:32',NULL),(404,10,'2013-01-30 08:53:29',NULL),(405,10,'2013-01-30 08:54:21',NULL),(406,10,'2013-01-30 08:59:12',NULL),(407,10,'2013-01-30 09:01:07',NULL),(408,10,'2013-01-30 09:02:32',NULL),(409,10,'2013-01-30 09:05:30',NULL),(410,10,'2013-01-30 09:06:24',NULL),(411,10,'2013-01-30 09:08:15',NULL),(412,10,'2013-01-30 09:08:41',NULL),(413,10,'2013-01-30 09:16:05',NULL),(414,10,'2013-01-30 09:16:23',NULL),(415,10,'2013-01-30 09:20:01',NULL),(416,10,'2013-01-30 09:21:26',NULL),(417,10,'2013-01-30 09:22:32',NULL),(418,10,'2013-01-30 09:27:25',NULL),(419,10,'2013-01-30 09:30:40',NULL),(420,10,'2013-01-30 09:32:51',NULL),(421,10,'2013-01-30 09:33:39',NULL),(422,10,'2013-01-30 09:34:52',NULL),(423,10,'2013-01-30 09:35:52',NULL),(424,10,'2013-01-30 09:36:08',NULL),(425,10,'2013-01-30 09:37:31',NULL),(426,10,'2013-01-30 09:38:59',NULL),(427,10,'2013-01-30 09:40:26',NULL),(428,10,'2013-01-30 09:42:22',NULL),(429,10,'2013-01-30 09:43:37',NULL),(430,10,'2013-01-30 12:54:18',NULL),(431,10,'2013-01-30 13:09:41',NULL),(432,10,'2013-01-30 13:10:23',NULL),(433,10,'2013-01-30 13:11:09',NULL),(434,10,'2013-01-30 13:11:49',NULL),(435,10,'2013-01-30 13:12:02',NULL),(436,10,'2013-01-30 13:12:13',NULL),(437,10,'2013-01-30 13:12:35',NULL),(438,10,'2013-01-30 15:32:09',NULL),(439,10,'2013-01-30 15:34:16',NULL),(440,10,'2013-01-30 15:34:51',NULL),(441,10,'2013-01-30 15:39:17',NULL),(442,10,'2013-01-30 15:39:43',NULL),(443,10,'2013-01-30 15:40:07',NULL),(444,10,'2013-01-31 06:51:14',NULL),(445,10,'2013-01-31 15:00:51',NULL),(446,10,'2013-02-02 07:17:25',NULL),(447,10,'2013-02-02 07:42:29',NULL),(448,10,'2013-02-02 07:42:47',NULL),(449,10,'2013-02-02 07:44:13',NULL),(450,10,'2013-02-02 07:44:36',NULL),(451,10,'2013-02-02 07:50:22',NULL),(452,10,'2013-02-02 07:50:56',NULL),(453,10,'2013-02-02 07:52:11',NULL),(454,10,'2013-02-02 07:53:52',NULL),(455,10,'2013-02-02 07:54:26',NULL),(456,10,'2013-02-02 07:54:38',NULL),(457,10,'2013-02-02 07:56:50',NULL),(458,10,'2013-02-02 07:58:44',NULL),(459,10,'2013-02-02 07:59:53',NULL),(460,10,'2013-02-02 08:01:42',NULL),(461,10,'2013-02-03 08:06:57',NULL),(462,10,'2013-02-03 08:07:30',NULL),(463,10,'2013-02-03 08:09:15',NULL),(464,10,'2013-02-03 08:09:40',NULL),(465,10,'2013-02-03 09:23:06',NULL),(466,10,'2013-02-03 10:01:34',NULL),(467,10,'2013-02-03 10:02:31',NULL),(468,10,'2013-02-03 10:03:00',NULL),(469,10,'2013-02-03 10:03:35',NULL),(470,10,'2013-02-03 10:11:29',NULL),(471,10,'2013-02-03 10:56:37',NULL),(472,10,'2013-02-03 10:59:30',NULL);
/*!40000 ALTER TABLE `users_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'homeorg'
--
/*!50003 DROP FUNCTION IF EXISTS `get_user_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`homeorg`@`localhost`*/ /*!50003 FUNCTION `get_user_login`(sId INT) RETURNS varchar(20) CHARSET utf8
    READS SQL DATA
BEGIN

  RETURN (SELECT `login`

          FROM

            `users`

          WHERE

            `id` = sId);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `check_user`(IN in_login VARCHAR(20), IN in_email VARCHAR(250), OUT out_passw VARCHAR(32), OUT out_user_id INT(11), OUT out_users_group_id INT(11))
    READS SQL DATA
BEGIN
  DECLARE sUserId          INT(11);
  DECLARE sUsersGroupId    INT(11);
  DECLARE sPassw            VARCHAR(32);

  SELECT `id`
       , `passw`
       , `users_group_id`
  INTO
    sUserId, sPassw, sUsersGroupId
  FROM
    `users`
  WHERE
    `login` = in_login
    OR `email` = in_email;
  IF sPassw IS NOT NULL THEN
  SET out_user_id = sUserId;
  SET out_passw = sPassw;
  SET out_users_group_id = sUsersGroupId;
ELSE
  SET out_user_id = 0;
  SET out_passw = '';
  SET out_users_group_id = 0;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_account`(IN in_users_session_id INT(11), IN in_users_group_id INT(11), IN in_user_id INT(11), IN in_currency_id INT(11), IN in_name VARCHAR(255), IN in_amount DECIMAL(18,2), OUT out_account_id INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE sId INT(11);
  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'accounts');
  INSERT INTO `accounts` (`id`, `users_session_id`, `users_group_id`, `user_id`, `currency_id`, `name`, `amount`)
    VALUES (sId, in_users_session_id, in_users_group_id, in_user_id, in_currency_id, in_name, in_amount);
  SET out_account_id = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_classifier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_classifier`(IN in_users_session_id INT(11), IN in_users_group_id INT(11), IN in_language_id INT(11), IN in_name VARCHAR(255), OUT out_classifier_id INT(11))
    MODIFIES SQL DATA
    COMMENT 'создание классификатора'
BEGIN
  DECLARE sId INT(11);
  DECLARE sPGId INT(11);
  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'classifiers');
  INSERT INTO `classifiers` (`id`, `is_global`, `users_group_id`, `users_session_id`, `language_id`, `name`) 
    VALUES (sId, 0, in_users_group_id, in_users_session_id, in_language_id, in_name);
  SET out_classifier_id = sId;
  CALL create_product_group(NULL, sId, NULL, '', NULL, sPGId);
  UPDATE `classifiers` SET `def_products_group_id` = sPGId WHERE `id` = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_product_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_product_group`(IN in_users_session_id INT(11), IN in_classifier_id INT(11), IN in_parent_id INT(11), IN in_name VARCHAR(255), IN in_unit_id INT(11), OUT out_products_group_id INT(11))
BEGIN
  DECLARE sId INT(11);
  DECLARE sParentId INT(11);
  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'products_groups');
  IF in_parent_id IS NULL THEN
    SET sParentId = (SELECT `def_products_group_id` FROM `classifiers` WHERE `id` = in_classifier_id);
  ELSE
    SET sParentId = in_parent_id;
  END IF;
  INSERT INTO `products_groups` (`id`, `parent_id`, `name`, `m_unit_id`, `users_session_id`, `classifier_id`)
    VALUES (sId, sParentId, in_name, in_unit_id, in_users_session_id, in_classifier_id);
  SET out_products_group_id = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_user`(IN in_login VARCHAR(20), IN in_email VARCHAR(250), IN in_passw VARCHAR(32), IN in_country_id INT(11), IN in_language_id INT(11), IN in_currency_id INT(11), OUT sId INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE sLogin VARCHAR(20);
  DECLARE sGId   INT(11);
  DECLARE sCId INT(11);
  DECLARE sAId INT(11);
  SET sId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = database() AND TABLE_NAME = 'users');
  IF in_login = 'noname' OR in_login IS NULL THEN
    SET sLogin = concat('noname', sId);
  ELSE
    SET sLogin = in_login;
  END IF;
  INSERT INTO `users` (`id`, `login`, `passw`, `email`, `country_id`, `language_id`)
    VALUES (sId, sLogin, in_passw, in_email, in_country_id, in_language_id);
  SET sGId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = database() AND TABLE_NAME = 'users_groups');
  INSERT INTO `users_groups` (`id`, `user_id`) VALUES (sGId, sId);
  UPDATE `users` SET `users_group_id` = sGId WHERE `id` = sId;
  CALL create_classifier(NULL, sGId, in_language_id, 'default', sCId);
  UPDATE `users_groups` SET `classifier_id` = sCId WHERE `id` = sGId;
  CALL create_account(NULL, sGId, sId, in_currency_id, '', 0, sAId);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_users_product` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_users_product`(IN in_users_session_id INT(11), IN in_users_group_id INT(11), IN in_products_group_id INT(11), IN in_name VARCHAR(255), IN in_unit_id INT(11), IN in_last_price DECIMAL(18,2), IN in_currency_id INT(11), OUT out_users_product_id INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE sId       INT(11);

  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'users_products');
  INSERT INTO `users_products` (`id`, `name`, `users_group_id`, `users_session_id`, `products_group_id`, `m_unit_id`, `last_price`, `currency_id`)
    VALUES (sId, in_name, in_users_group_id, in_users_session_id, in_products_group_id, in_unit_id, in_last_price, in_currency_id);
  SET out_users_product_id = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user_check` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_user_check`(IN in_users_session_id INT(11), IN in_users_group_id INT(11), IN in_byer_user_id INT(11), IN in_users_place_id INT(11), IN in_users_place_name VARCHAR(255), IN in_date_purchase TIMESTAMP, IN in_xml TEXT, OUT out_users_check_id INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE iter          INT(11) DEFAULT 1;
  DECLARE sId           INT(11);
  DECLARE pId           INT(11);
  DECLARE sUsersGroupId INT(11);
  DECLARE sDatePurchase TIMESTAMP;
  DECLARE sPlaceId      INT(11);
  DECLARE groupName     VARCHAR(255);
  DECLARE groupId       INT(11);
  DECLARE sGroupID INT(11);
  DECLARE productId     INT(11);
  DECLARE brandName     VARCHAR(255);
  DECLARE brandId       INT(11);
  DECLARE unitId        INT(11) DEFAULT NULL;
  DECLARE productPrice  DECIMAL(18, 2);
  DECLARE currencyID    INT(11);
  DECLARE unitsInStock DECIMAL(6, 3);
  DECLARE sClassifierId INT(11);

  IF in_users_group_id IS NULL THEN
    SET sUsersGroupId = (SELECT `users_group_id`
                         FROM
                           `users`
                         WHERE
                           `id` = (SELECT `user_id`
                                   FROM
                                     `users_sessions`
                                   WHERE
                                     `id` = in_users_session_id));
  ELSE
    SET sUsersGroupId = in_users_group_id;
  END IF;
  IF in_date_purchase IS NULL THEN
    SET sDatePurchase = now();
  ELSE
    SET sDatePurchase = in_date_purchase;
  END IF;
  IF in_users_place_id = 0 THEN
    SET sPlaceId = (SELECT `id` FROM `users_places` WHERE `users_group_id` = in_users_group_id AND `name` = in_users_place_name);
    IF sPlaceId IS NULL THEN
      CALL create_user_place(in_users_session_id, in_users_group_id, in_users_place_name, NULL, sPlaceId);
    END IF;
  ELSE
    SET sPlaceId = in_users_place_id;
  END IF;
  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'users_checks');
  INSERT INTO `users_checks`
    (`id`, `users_session_id`, `users_group_id`, `byer_user_id`, `users_place_id`, `date_purchase`) 
    VALUES (sId, in_users_session_id, sUsersGroupId, in_byer_user_id, sPlaceId, sDatePurchase);
  SET out_users_check_id = sId;
  REPEAT
  SET groupName = extractvalue(in_xml, '/ch/r[$iter]/g/text()');
  IF groupName != '' THEN
    SET groupId = extractvalue(in_xml, '/ch/r[$iter]/gi');
    SET brandName = extractvalue(in_xml, '/ch/r[$iter]/b/text()');
    SET brandId = extractvalue(in_xml, '/ch/r[$iter]/bi');
    SET unitId = extractvalue(in_xml, '/ch/r[$iter]/ui');
    SET productPrice = extractvalue(in_xml, '/ch/r[$iter]/pr');
    SET currencyID = extractvalue(in_xml, '/ch/r[$iter]/ci');
    SET unitsInStock = extractvalue(in_xml, '/ch/r[$iter]/s');
    SET iter = iter + 1;

    
    IF groupId = 0 THEN
      SET sClassifierId = (SELECT `classifier_id`
                           FROM
                             `users_groups`
                           WHERE
                             `id` = in_users_group_id);
      SET sGroupId = (SELECT `id` FROM `products_groups` WHERE `classifier_id` = sClassifierId AND `name` = groupName);
      IF sGroupID IS NULL THEN
        CALL create_product_group(in_users_session_id, sClassifierId, NULL, groupName, unitId, sGroupId);
      END IF;
    ELSE
      SET sGroupId = groupId;
    END IF;
    IF brandId = 0 THEN
      SET brandId = (SELECT `id` FROM `users_products` WHERE `users_group_id` = in_users_group_id AND `products_group_id` = sGroupId AND `name` = brandName);
      IF brandId IS NULL THEN
        CALL create_users_product(in_users_session_id, in_users_group_id, sGroupId, brandName, unitId, productPrice, currencyID, brandId);
      END IF;
    ELSE
      UPDATE `users_products` SET `last_price` = productPrice, `currency_id` = currencyID, `m_unit_id` = unitId WHERE `id` = brandId;
    END IF;
    SET pId = (SELECT AUTO_INCREMENT
               FROM
                 information_schema.TABLES
               WHERE
                 TABLE_SCHEMA = database()
                 AND TABLE_NAME = 'users_purchases');
    INSERT INTO `users_purchases`
      (`id`, `users_check_id`, `users_group_id`, `quantity`, `price`, `currency_id`, `m_unit_id`, `users_product_id`) VALUES (pId, sId, sUsersGroupId, unitsInStock, productPrice, currencyID, unitId, brandId);
  END IF;
UNTIL groupName = ''
END REPEAT;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user_check2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_user_check2`(IN xml_rows TEXT)
BEGIN 
  DECLARE iter INT(11) DEFAULT 1;
  DECLARE pname VARCHAR(255);

  REPEAT
    SET pname = extractvalue(xml_rows, '/ch/r[$iter]/n/text()');
    SET iter = iter + 1;
    IF pname != '' THEN
      SELECT pname;
    END IF;
  UNTIL pname = ''
  END REPEAT;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user_place` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_user_place`(IN in_users_session_id INT(11), IN in_users_group_id INT(11), IN in_name VARCHAR(255), IN in_parent_id INT(11), OUT out_users_place_id INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE sId           INT(11);
  DECLARE sUsersGroupId INT(11);
  DECLARE sParentId     INT(11);

  IF in_users_group_id IS NULL THEN
    SET sUsersGroupId = (SELECT `users_group_id` FROM `users` WHERE `id` = (SELECT `user_id` FROM `users_sessions` WHERE `id` = in_users_session_id));
  ELSE
    SET sUsersGroupId = in_users_group_id;
  END IF;

  IF in_parent_id IS NOT NULL THEN
    IF EXISTS(SELECT `id` FROM `users_places` WHERE `id` = in_parent_id) THEN
      SET sParentId = in_parent_id;
    ELSE
      SET sParentId = NULL;
    END IF;
  ELSE
    SET sParentId = NULL;
  END IF;

  SET sId = (SELECT AUTO_INCREMENT
             FROM
               information_schema.TABLES
             WHERE
               TABLE_SCHEMA = database()
               AND TABLE_NAME = 'users_places');
  INSERT INTO `users_places`
  (`id`, `users_session_id`, `users_group_id`, `name`, `parent_id`) VALUES (sId, in_users_session_id, sUsersGroupId, in_name, sParentId);
  SET out_users_place_id = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `create_user_session`(IN in_user_id INT(11), OUT out_users_session_id INT(11))
    MODIFIES SQL DATA
BEGIN
  DECLARE sId INT(11);
  SET sId = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = database() AND TABLE_NAME = 'users_sessions');
  INSERT INTO `users_sessions` (`id`, `user_id`) VALUES (sId, in_user_id);
  SET out_users_session_id = sId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_products_groups` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_products_groups`(IN in_users_group_id INT(11), IN in_filter VARCHAR(255))
    READS SQL DATA
BEGIN
  DECLARE sCId INT(11);
  SET sCId = (SELECT `classifier_id`
              FROM
                `users_groups`
              WHERE
                `id` = in_users_group_id);
  SET @sql = concat('SELECT id, name FROM products_groups WHERE classifier_id = ', sCId, ' AND name LIKE(\'', in_filter, '%\')');
  PREPARE squery FROM @sql;
  EXECUTE squery;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_session_checks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_session_checks`(IN in_users_session_id INT(11))
    READS SQL DATA
    COMMENT 'список чеков за сеанс'
BEGIN
  SELECT `id`, `users_place_id`, `date_purchase`
FROM
  `users_checks`
WHERE
  `users_session_id` = in_users_session_id
ORDER BY
  `date_create` DESC;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_units` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_units`(IN in_country_id INT(11), IN in_symbol_filter VARCHAR(20))
    READS SQL DATA
BEGIN
  DECLARE sCountryIName VARCHAR(255);
  DECLARE sSymbolFilter VARCHAR(20);

  SET sCountryIName = (SELECT `iname`
                       FROM
                         `countries`
                       WHERE
                         `id` = in_country_id);
  IF in_symbol_filter IS NULL THEN
    SET sSymbolFilter = "";
  ELSE
    SET sSymbolFilter = in_symbol_filter;
  END IF;
  SET @sql = concat('SELECT id, parent_id, m_unit_group_id, multiplier, is_international, code, symbol, name FROM units_', sCountryIName,' WHERE symbol LIKE(\'', sSymbolFilter,'%\')');
  PREPARE squery FROM @sql;
  EXECUTE squery;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_units_groups` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_units_groups`(IN in_country_id INT(11), IN in_language_id INT(11))
BEGIN
  DECLARE sLanguageId INT(11);

  SET sLanguageId = in_language_id;
  IF in_language_id IS NULL THEN
    SET sLanguageId = (SELECT `language_id`
                   FROM
                     `countries` WHERE `id` = in_country_id);
  END IF;
  SELECT u.`id`
     , u.`parent_id`
     , u.`iname`
     , t.`translation` as name
FROM
  `m_units_groups` u
LEFT JOIN `translations` t
ON t.`translations_group_id` = u.`iname_translations_group_id` AND t.`language_id` = sLanguageId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_user_info`(IN in_user_id INT(11), OUT out_login VARCHAR(20), OUT out_email VARCHAR(255))
    READS SQL DATA
BEGIN
  SELECT `login`
       , `email`
  INTO
    out_login, out_email
  FROM
    `users`
  WHERE
    `id` = in_user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_places` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_user_places`(IN in_users_group_id INT(11), IN in_filterText VARCHAR(255))
    READS SQL DATA
BEGIN
  SET @sql = concat('SELECT id, parent_id, name FROM users_places WHERE users_group_id = ', in_users_group_id,' AND name LIKE(''', in_filterText, '%'')');
  PREPARE squery FROM @sql;
  EXECUTE squery;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_products` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_user_products`(IN in_users_group_id INT(11), IN in_products_group_id INT(11), IN in_filter VARCHAR(255))
    READS SQL DATA
BEGIN
  DECLARE sCId INT(11);
  SET sCId = (SELECT `classifier_id` FROM `users_groups` WHERE `id` = in_users_group_id);
  SET @sql = concat('SELECT id, name FROM users_products WHERE users_group_id = ', in_users_group_id, ' AND products_group_id = ', in_products_group_id, ' AND name LIKE(\'', in_filter, '%\')');
  PREPARE squery FROM @sql;
  EXECUTE squery;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `test` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `test`(IN value VARCHAR(255))
BEGIN
IF concat(value + 0) = value THEN
  SELECT "число";
ELSE
  SELECT "строка";
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `units_russia`
--

/*!50001 DROP TABLE IF EXISTS `units_russia`*/;
/*!50001 DROP VIEW IF EXISTS `units_russia`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `units_russia` AS select `u`.`id` AS `id`,`u`.`parent_id` AS `parent_id`,`u`.`m_unit_group_id` AS `m_unit_group_id`,`u`.`multiplier` AS `multiplier`,`u`.`is_international` AS `is_international`,`u`.`code` AS `code`,`u`.`symbol` AS `symbol`,`u`.`name` AS `name` from `m_units` `u` where ((`u`.`is_old` = 0) and (`u`.`country_id` = (select `countries`.`id` from `countries` where (`countries`.`name` = 'Россия')))) union select `u`.`id` AS `id`,`u`.`parent_id` AS `parent_id`,`u`.`m_unit_group_id` AS `m_unit_group_id`,`u`.`multiplier` AS `multiplier`,`u`.`is_international` AS `is_international`,`c`.`nat_code` AS `nat_code`,`c`.`nat_symbol` AS `nat_symbol`,`c`.`nat_name` AS `nat_name` from (`m_units` `u` left join `m_units_countries` `c` on(((`c`.`m_unit_id` = `u`.`id`) and (`c`.`country_id` = (select `countries`.`id` from `countries` where (`countries`.`name` = 'Россия')))))) where ((`u`.`is_old` = 0) and isnull(`u`.`country_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-03 23:49:31
