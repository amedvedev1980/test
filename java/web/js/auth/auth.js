function test_auth_input(){
  var pattern = /^\w.*@\w.*$/;
  var login = $.trim($("#login").val());
  var password = $("#password").val();
  if 
  (
    login.length > 0
    && login.search(pattern) == 0
    && password.length > 0
  )
    return true;
  return false;
}
$(document).ready(function(){ 
  $("#submit").attr("disabled", "disabled");
  $("#login").blur(function(){
    //alert("Focus out");
  });
  $("#login, #password").keyup(function(){
    if (test_auth_input()){
      $("#submit").removeAttr("disabled");
    }
    else{
      $("#submit").attr("disabled", "disabled");      
    }
  });
  

});