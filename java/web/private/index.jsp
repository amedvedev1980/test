<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <div id="forecast">
    <div id="tabstrip">
      <ul>
        <li class="k-state-active">Домашний учет</li>
        <li>Планирование покупок</li>
        <li>Кулинария</li>
      </ul>
      <div>
        <%@include file="/WEB-INF/jspf/private/modules/home-record/home-record.jspf" %>  
      </div>
      <div>Блок 2</div>
      <div>Блок 3</div>
    </div>
  <style scoped>
    #forecast {
      width: 100%;
      /*height: 337px;*/
      margin: 0px;  /*30px auto;*/
      padding: 0px; /*80px 15px 0 15px;*/
      /*background: url('../../content/web/tabstrip/forecast.png') transparent no-repeat 0 0;*/
    }
    .sunny, .cloudy, .rainy {
      display: inline-block;
      margin: 20px 0 20px 10px;
      width: 128px;
      height: 128px;
      /*background: url('../../content/web/tabstrip/weather.png') transparent no-repeat 0 0;*/
    }
    .cloudy{
      background-position: -128px 0;
    }
    .rainy{
      background-position: -256px 0;
    }
    .weather {
      width: 160px;
      padding: 40px 0 0 0;
      float: right;
    }
    #forecast h2 {
      font-weight: lighter;
      font-size: 5em;
      padding: 0;
      margin: 0;
    }
    #forecast h2 span {
      background: none;
      padding-left: 5px;
      font-size: .5em;
      vertical-align: top;
    }
    #forecast p {
      margin: 0;
      padding: 0;
    }
  </style>
  <script>
    $(document).ready(function() {
      $("#tabstrip").kendoTabStrip({
        animation:	{
          open: {
            effects: "fadeIn"
          }
        }
      });
    });
  </script>
  </div>