<%@ page pageEncoding="UTF-8" %>
<!--<div class="k-block k-info-colored navinfo" >-->
<div class="k-block k-header k-state-hover navinfo" >
  <h3>Чеки</h3>
</div>
<div id="check"><!-- class="k-content">--> 
  <div id="section">
    <div id="left_pane">
      <ul id="panelbar">
        <li class="k-state-active">
          <span class="k-link k-state-selected">Ввод позиций чека<span class="k-icon k-i-note"></span></span>
          <div id="add_ext" class="configuration k-widget">
            <ul class="options">
              <li>
                <label>Наименование:
                  <input id="input_product_name" data-role="autocomplete" data-text-field="pgname" data-bind="source: pgroups, value: autoCompleteProduct, events: { select: select, change: change }" class="name"/>
                </label>
              </li>
              <li>
                <label>Марка:
                  <input id="input_product_type" data-role="autocomplete" data-text-field="bname" data-bind="source: brands, value: autoCompleteBrand" class="name"/>
                </label>
              </li>
              <li>
                <label>Количество:<input id="input_unitsinstock" type="text" placeholder="" data-bind="value: productUnitsInStock" class="k-textbox"/></label>
              </li>
              <li>
                <label>Ед. изм.:
                  <input data-role="autocomplete" data-text-field="usymb" data-bind="source: units, value: autoCompleteUnit" class="units"/>
                </label>
              </li>
              <li>
                <label class="two_cols">Цена:
                  <input id="input_product_price" type="text" placeholder="" data-bind="value: productPrice" class="k-textbox"/>
                </label>
                <input id="input_currency" data-role="autocomplete" data-text-field="currsymb" data-bind="source: currs, value: autoCompleteCurr" class="curr" tabindex="-1"/>
              </li>
              <li>
                <button id="btn_add_product" class="k-button" data-bind="click: addProduct">Добавить новый продукт</button>
              </li>
            </ul>
          </div>
        </li>
        <li>
          <span>Чеки в этом сеансе</span>
          <div id="sessChecks"></div>
          
          <script type="text/x-kendo-tmpl" id="sessChecksView">
            <div class="check-view">
              <dl>
                <dt>Чек</dt>
                <dd>#= Id#</dd>
                <dt>Позиций</dt>
                <dd>#= Check.checkNPos#</dd>
                <dt>Сумма</dt>
                <dd>#= Check.checkSumm#</dd>
                <dt>Место</dt>
                <dd>#= Check.checkPlace#</dd>
              </dl>
              <div class="cmd-buttons">
                <a class="k-button k-button-icontext" href="\\#"><span class="k-icon k-edit"></span>Редактировать</a>
                <a class="k-button k-button-icontext" href="\\#"><span class="k-icon k-delete"></span>Удалить</a>
              </div>
            </div>
          </script>
          
          <style scoped>
            .check-view dl
            {
              margin: 10px 0;
              padding: 0;
              min-width: 0;
            }
            .check-view dt, dd
            {
              float: left;
              margin: 0;
              padding: 0;
              height: 30px;
              line-height: 30px;
            }
            .check-view dt
            {
              clear: left;
              padding: 0 5px 0 15px;
              text-align: right;
              opacity: 0.6;
              width: 100px;
            }
            .cmd-buttons
            {
              text-align: right;
              padding: 5px;
              min-width: 100px;
              border-top: 1px solid rgba(0,0,0,0.1);
              -webkit-border-radius: 8px;
              -moz-border-radius: 8px;
              border-radius: 8px;
            }
          </style>
        </li>
      </ul>        
    </div>
      
    <div id="checkstrip">
      <ul>
        <li class="k-state-active">Новый чек</li>
      </ul>
      <div>
        <div id="panes">
          <div id="up_pain">
            <div style="margin-top: 10px; margin-left: 10px">
              <div class="left">
                <button id="send_check" class="k-button">Сохранить чек</button>
              </div>
              <div style="float: left; margin-left: 10px;">
                <span style ="margin-top:10px;">Дата чека: </span>
                <input id="datepicker" value="10/10/2011" style="width:150px;" />            
              </div>
              <div id="orgs" style="float: left; margin-left: 10px;">
                <span style ="margin-top:10px;">Организация: </span>
                <input id="organization"/>
              </div>          
            </div>
          </div>
          <div>
            <div id="grid"></div>
          </div>

        </div>
      </div>
    </div>
  </div>    

  <div id="log" class="k-block"><div id="dialog"></div></div>
  
  <script>        
    $(document).ready(function() {
    /*
     *  ===> GLOBAL VARS <===
     */
      var pos_count = 1;
      var today = new Date();
      /* --- SIZES PARAMS # BEGIN # --- */
      var section_width = $("#section").width();
      var addext_width = $("#add_ext").outerWidth();
      var rWidth = Math.floor(section_width - addext_width) - 30;
      /* --- SIZES PARAMS # END # --- */
      /* --- CHECK HEADER DATA # BEGIN # --- */
      var checkId = 0;
      var checkDate;
      var placeId = 0;
      var placeName = "";      
      /* --- CHECK HEADER DATA # BEGIN # --- */
      /* --- CHECK ROWS DATA # BEGIN # --- */
      var check_data = [
        // Example: { name: "Hampton Sofa", price: 989.99, unitsInStock: 39 }
      ];
      /* --- CHECK ROWS DATA # END # --- */
      /* --- GET ALL UNITS # BEGIN # --- */
      $.ajax({
        type: "POST",
        url: "private/homerecord/get_json_units?mode=1",
        data: {},
        dataType: "json",
        beforeSend: function(){
          // данный обработчик будет вызван
          // перед отправкой данного AJAX запроса
        },
        success: function(){
          // а этот при удачном завершении
        },
        error: function(msg){
          // этот при возникновении ошибки
          alert("error ");
        },
        complete: function(data){
          // и по завершению запроса (удачном или нет)
        }
      })
      .done(function( data ) {
        viewModel.set("units", data);
      });

      /* --- GET ALL UNITS # END # --- */
      
    /*
     *  ===> AUXILARY <===
     */
    
      /* --- WINDOW RESIZE BIND # BEGIN # --- */
      $(window).resize(function(){
      });
      /* --- WINDOW RESIZE BIND # END # --- */
      /* --- FIX for JSON # BEGIN # --- */
      kendo.data.ObservableArray.fn.toJSON = function() {
        return [].slice.call(this);
      }
      /* --- FIX for JSON # END # --- */
      
    /*
     *  ===> DATA <===
     */
    
      /* --- SHARED DATA SOURCE # BEGIN # --- */
      var sharedDataSource = new kendo.data.DataSource({
        data: check_data
        ,schema: {
          model: {
            fields: {
              pnum: { type: "number" },
              group_name: { type: "string" },
              brand_name: {brand: "string"},
              price: { type: "number" },              
              unitsInStock: { type: "number" },
              unit_symb: {type: "string"}
            }
          }
        }
      });
      var sessChecksSource = new kendo.data.DataSource({
        data: []
      });
      /* --- SHARED DATA SOURCE # END # --- */
      
    /*
     *  ===> SPLITTER <===
     */
    
      /* --- SPLITTER BIND # BEGIN # --- */
      $("#section").kendoSplitter({
        panes: [
          { collapsible: true, resizable: false, size: "260px" },
          { collapsible: false, scrollable: false }
        ],
        collapse: onCollapse,
        expand: onExpand
      });
      $("#panes").kendoSplitter({
        orientation: "vertical",
        panes: [
          { collapsible: true, resizable: false, size: "50px"},
          { collapsible: false, resizable: false, size: "700px", scrollable: false}
        ]
      });      
      /* --- SPLITTER BIND # END # --- */
      /* --- SPLITTER EVENTS # BEGIN # --- */
      function onCollapse(e) {
        if (e.pane.id == "left_pane"){
          $("#add_ext").hide();
        }
      }
      function onExpand(e) {
        if (e.pane.id == "left_pane"){
          $("#add_ext").show();
        }
      }
      /* --- SPLITTER EVENTS # END # --- */
      
    /*
     *  ===> LEFT PANEL BAR <===
     */
    
      /* --- LEFT PANEL BAR BIND # BEGIN # --- */
      $("#panelbar").kendoPanelBar({
        expandMode: "multiple"
      }).css({ width: "250px" }).css({ margin: "3px auto" });
      /* --- LEFT PANEL BAR BIND # END # --- */

    /*
     *  ===> RIGHT PANEL : CHECK VIEW
     */
      /* --- Check Tab Strip # BEGIN # --- */
      $("#checkstrip").kendoTabStrip({
        animation:	{
          open: {
            effects: "fadeIn"
          }
        }
      });
      /* --- Check Tab Strip # END # --- */
    /*
     *  ===> HEAD OF CHECK <===
     */   
      /* --- SAVE CHECK # BEGIN # --- */      
      $("#send_check").click(function(){        
        checkDate = Math.ceil($("#datepicker").data("kendoDatePicker").value().valueOf()/1000);
        //alert("Check date: " + checkDate.value().valueOf());
        //return 0;
        //alert(organization_acomplete.value());
        var check = {
          "checkId" : checkId,
          "checkDate": checkDate,
          "placeId": placeId,
          "placeName": organization_acomplete.value(),
          "positions" : JSON.parse(JSON.stringify(grid.dataSource.data()))
        };
        //alert(print_r(check));
        var check_sfy = JSON.stringify(check);
        $.ajax({
          type: "POST",
          url: "private/homerecord/do_savecheck",
          data: check_sfy,
          dataType: "json",
          beforeSend: function(){
            // данный обработчик будет вызван
            // перед отправкой данного AJAX запроса
          },
          success: function(){
            // а этот при удачном завершении
          },
          error: function(msg){
            // этот при возникновении ошибки
            alert("error" + msg);
          },
          complete: function(data){
            // и по завершению запроса (удачном или нет)
          }
        })
        .done(function( data ) {
          //alert( "Data Saved: " + print_r(data) + "\nresult = " + data[0].result );
          var ch_id = data[0].result;
          var ind = (ch_id > 0 ? ch_id : 0);
          var res = (ch_id > 0 ? 1 : ch_id);
          var ch_data = {
            Id: ch_id,
            Check: {
              checkNum: 234,
              checkNPos: pos_count - 1,
              checkSum: 100,
              checkPlace: organization_acomplete.value(),
              checkDate : checkDate,
              placeId: placeId,
              placeName: organization_acomplete.value(),
              positions : JSON.parse(JSON.stringify(grid.dataSource.data())),
              result: res
            }
          };
          checks[ind] = ch_data;
          // выводим чек в листер
          sessChecksSource.data().push(ch_data);
          $("#sessChecks").data("kendoListView").refresh();
          // очищаем чек!
          grid.dataSource.data([]);
          pos_count = 1;
        });
      });
      /* --- SAVE CHECK # END # --- */
      /* --- DATEPICKER(Date) BIND # BEGIN # --- */
      $("#datepicker").kendoDatePicker({
        value: today
      });
      /* --- DATEPICKER(Date) BIND # END # --- */      
      /* --- AUTOCOMPLETE(Places) BIND # BEGIN # --- */
      $("#organization").kendoAutoComplete({
        minLength: 1,
        dataTextField: "Organization",
        dataSource: {
          serverFiltering: true,
          serverPaging: false,
          transport: {
            read: "private/homerecord/get_json_organizations",
            contentType: "application/json; charset=utf-8"
          }
        },
        select: function (e) 
        { 
          if (e.item == null) return;
          var DataItem = this.dataItem(e.item.index());
          placeId = DataItem.OrganizationId;
          placeName = DataItem.Organization;
        },
        change: function(e)
        {
        }
      });
      var organization_acomplete = $("#organization").data("kendoAutoComplete");
      /* --- AUTOCOMPLETE(Places) BIND # END # --- */
      
    /*
     * ===> INPUT CHECK POSITION DIALOG <===
     */
    
      /* --- VIEWMODEL VAR # BEGIN # --- */
      var viewModel = kendo.observable({
        autoCompleteProduct: null,
        autoCompleteBrand: null,
        productPrice: 0,
        autoCompleteCurr: null,
        productUnitsInStock: 0,
        autoCompleteUnit: null,
        pgroups: new kendo.data.DataSource({
          dataType: "jsonp",
          serverFiltering: true,
          serverPaging: false,
          transport: {
            read: "private/homerecord/get_json_pgroups",
            contentType: "application/json; charset=utf-8"
          }
        }),
        brands: new kendo.data.DataSource({
          dataType: "jsonp",
          serverFiltering: true,
          serverPaging: false,
          transport: {
            read: "private/homerecord/get_json_uproducts",
            contentType: "application/json; charset=utf-8",
            parameterMap: function (data) {
              return {group_id: viewModel.getProductGroupId(), filter: data.filter.filters[0].value };
            }
          }
        }),
        currs: [
          { currsymb: "руб.", id: "1" },
          { currsymb: "коп.", id: "2" },
          { currsymb: "$", id: "3" }
        ],
        units: [],
        getProductGroupId: function(){
          var product_group = this.get("autoCompleteProduct");         
          var product_group_id = (typeof(product_group) == "object" && product_group != null? product_group.get("id") : "0");
          return product_group_id;
        },
        select: function(e) {
        },
        change: function() {
        },
        addProduct: function() {
          var product_group = this.get("autoCompleteProduct");         
          var product_group_name = (
            typeof(product_group) == "object"  
              ? (product_group == null ? "" : product_group.get("pgname"))
              : product_group);
          var product_group_id = (typeof(product_group) == "object" && product_group != null? product_group.get("id") : "0");
          if (product_group_name.length == 0) {
            alert("Ошибка! Не задано название продукта! ");
            return 0;
          }
          var productPrice = parseFloat(this.get("productPrice"));
          if (productPrice <= 0) {
            alert ("Ошибка! Цена на продукт должна быть больше нуля!");
            return 0;
          }
          
          var product_brand = this.get("autoCompleteBrand");
          var product_brand_name = (
            typeof(product_brand) == "object" 
              ? (product_brand == null ? "" : product_brand.get("bname")) 
              : product_brand);
          var product_brand_id = (typeof(product_brand) == "object" && product_brand != null ? product_brand.get("id") : "0");
          
          var product_unit = this.get("autoCompleteUnit");
          var product_unit_symb = (typeof(product_unit) == "object" ? product_unit.get("usymb") : product_unit);
          var product_unit_id = (typeof(product_unit) == "object" ? product_unit.get("id") : "0");
          
          var currency_used = this.get("autoCompleteCurr");
          var currency_symb = (typeof(currency_used) == "object" ? currency_used.get("currsymb") : currency_used);
          var price_currency_id = (typeof(currency_used) == "object" ? currency_used.get("id") : "0");
                   
          if (1){
            var data = {
              pnum: pos_count,
              group_name: product_group_name,
              group_id: product_group_id,
              brand_name: product_brand_name,
              brand_id: product_brand_id,
              price: productPrice,
              currency_id: price_currency_id,
              unitsInStock: parseFloat(this.get("productUnitsInStock")),
              unit_symb: product_unit_symb,
              unit_id: product_unit_id
            };
           
            refresh_check_data(data);
            pos_count++;
            this.set("autoCompleteProduct", "");
            this.set("autoCompleteBrand", "");
            this.set("productPrice", 0);
            this.set("productUnitsInStock", 0);
            this.set("autoCompleteUnit", "");
            $("#input_product_name").focus();
          } else {
            
          }
        },
        products: []
      });
      viewModel.autoCompleteCurr = viewModel.currs[0]; // - так можно установить по умолчанию
      /* --- VIEWMODEL VAR # END # --- */
      /* --- VIEWMODEL BIND # BEGIN # --- */
      kendo.bind($("#add_ext"), viewModel);
      /* --- VIEWMODEL BIND # END # --- */
      /* --- VIEWMODEL EVENTS # BEGIN # --- */
      function refresh_check_data(data){
        check_data.push(data);
        var grid = $("#grid").data("kendoGrid");
        var ds = grid.dataSource.data();
        ds.push(data);
        grid.refresh();
      }
      $("#btn_add_product").keydown(function(e){
        if (e.keyCode === 9){
          e.preventDefault();
          $("#input_product_name")
            .focus()
            .select();
        }
      });
     
      /* --- VIEWMODEL EVENTS # END # --- */
     
    /*
     *  ===> VIEW SESSION CHECKS
     */

    $("#sessChecks").kendoListView({
      dataSource: sessChecksSource,
      template: kendo.template($("#sessChecksView").html()),
      selectable: "single",      
      change: function(e) {
        //var index = this.select().index();
        //var dataItem = this.dataSource.at(index);
        
        //var template = kendo.template($("#template").html());
        //$("#display").html(kendo.render(template, dataItem.People));
      }
    });
    /*
     *  ===> CHECK GRID <===
     */
    
      /* --- GRID VAR # BEGIN # --- */
      var grid = $("#grid").kendoGrid({
        dataSource: sharedDataSource
        ,height: 250
        ,width: 1000
        ,scrollable: true
        ,sortable: true
        ,filterable: false
        ,editable: true
        ,resizable: true
        ,columns: [
          {
            field: "pnum",
            title: "№",
            width: "30px"
          },
          {
            field: "group_name",
            title: "Наименование",
            width: "150px"                  //  30%
          },
          {
            field: "brand_name",
            title: "Марка",
            width: "150px"                  //  25%
          },
          {
            field: "unitsInStock",
            title: "количество",
            width: "80px"                  //  15%
          },
          {
            field: "unit_symb",
            title: "ед.изм.",
            width: "60px"                  //  10%
          },
          {
            field: "price",
            title: "цена",
            width: "80px"                  //  10%
          },
          {
            command: { text: "Удалить", click: check_row_destroy },
            title: "<a class='k-button'><span class='k-icon k-i-pencil'></span></a>",
            width: "10%"
          }
        ]
      }).data("kendoGrid");
      /* --- GRID VAR # END # --- */
      /* --- GRID EVENTS # BEGIN # --- */
      function check_row_destroy(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var del_pnum = dataItem.pnum;
        // перестройка нумерации
        //var grid_ds = grid.dataSource.data();
        $.map(sharedDataSource.data().toJSON(), function (dataItem, index) {
          if (dataItem.pnum > del_pnum){
            dataItem.pnum = dataItem.pnum - 1;
          }
        })
        ////dataItem.pnum
        // удаление
        grid.dataSource.remove(dataItem);
        pos_count = pos_count - 1;
      }
      /* --- GRID EVENTS # END # --- */
    });
  </script>

  <style scoped>
    #check{
      margin: 2px 0 0 0px;
      padding: 0px 0px 0px 0px;
      border: 1px solid inherit;
      background-image: none;
      /*-moz-box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);
      -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);
      box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      border-radius: 8px;*/
      overflow: hidden;
      /*min-height: 700px;*/
      height: 90%;
    }
    #section {
      /*width: 600px;*/
      min-height: 690px;
      /*height: 90%;*/
      /*overflow: hidden;*/
      
    }
    #section .configuration
    {
      /*loat: left;*/
      /*margin: 0 10px 10px 15px;*/
      padding: 10px;
      /*border: 1px solid inherit;*/
      background-image: none;
      /*-moz-box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);*/
      /*-webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);*/
      /*box-shadow: 0 1px 1px rgba(0,0,0,0.45), inset 0 0 30px rgba(0,0,0,0.07);*/
      /*-webkit-border-radius: 8px;*/
      /*-moz-border-radius: 8px;*/
      /*border-radius: 8px;*/
      /*min-width: 170px;*/
      /*max-width: 300px;*/
      /*width: 260px;*/
    }
    #grid{
      float: left;
      width: 100%;
      /*overflow: hidden;*/
      /*
      min-width: 600px;
      max-width: 100%;
      */
    }
    #input_currency{
      width: 70px;
      float: right;
    }
    .metrotable > tbody > tr > td  {
      font-size: 12px;
    }
    .metrotable > thead > tr > th {
      font-size: 14px;
      padding-top: 0;
    }
    .metrotable > tfoot > tr > td {
      padding-right: 10px;
    }
    /*configurator*/
    .absConf .configuration
    {
      position: absolute;
      top: -1px;
      right: -1px;
      height: auto;
      margin: 0;
      z-index: 2;
    }
    .configuration-horizontal
    {
      -webkit-border-radius: 5px 5px 0 0;
      -moz-border-radius: 5px 5px 0 0;
      border-radius: 5px 5px 0 0;
      position: static;
      height: auto;
      min-height: 0;
      width: auto;
      margin-bottom: 20px;
      float:none;
    }
    .configuration-horizontal-bottom
    {
      -webkit-border-radius: 0 0 5px 5px;
      -moz-border-radius: 0 0 5px 5px;
      border-radius: 0 0 5px 5px;
      margin: 20px -21px -21px;
      position: static;
      height: auto;
      min-height: 0;
      width: auto;
      float:none;
    }
    .configuration .configHead,
    .configuration .infoHead
    {
      display: block;
      font-size: 13px;
      margin-top: 0;
      text-indent: 0;
      margin-bottom: 10px;
      padding-bottom: 5px;
      border-bottom: 1px solid #999;
      background-position: 0 -178px;
    }
    .configuration .configHead .k-icon
    {
      float: left;
    }
    .configuration .infoHead
    {
      background-position: -107px -722px;
    }
    .configuration .configTitle
    {
      font-size: 12px;
      display: block;
      line-height: 22px;
      background-position: 0 21px;
      margin-bottom: 10px;
    }
    .configuration .options
    {
      list-style:none;
      margin: 0;
      padding: 0;
    }
    .configuration button
    {
      margin: 0;
      vertical-align: middle;
    }
    .configuration .k-textbox
    {
      margin-left: 7px;
      width: 90px;
    }
    .configuration .options li { display: block; margin: 0; padding: 0.2em 0; zoom: 1; }
    .configuration .options li:after
    {
      content: "";
      display: block;
      clear: both;
      height: 0;
    }
    .configuration label,
    .configuration input
    {
      vertical-align: middle;
      /*line-height: 20px;*/
      margin-top: 0;
    }
    .configuration input
    {
      width: 40px;
    }
    .configuration input,
    .configuration select
    {
      float: right;
    }
    .configuration .k-button,
    .configuration .k-widget
    {
      margin-bottom: 3px;
    }
    .configuration .name {
      margin-top: 0px;
      width: 100%;
    }
    .configuration label {
      display: block;
      float: none;
    }
    .configuration .curr {
      width: 65px;
      float: right;
    }
    .configuration .units{
      float: right;
    }
    label.two_cols{
      width: 160px;
      /*overflow: hidden;*/
      float: left;
      
    }
    .k-pane .k-content {
      margin: 0;
      padding: 0;
    }
  /* code viewer */
  </style>
</div>