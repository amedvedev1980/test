/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package homerecord;

import dbModel.DBObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Администратор
 */
public class do_savecheck extends HttpServlet {

  private static final Logger log = Logger.getLogger(do_savecheck.class.getName());
  /**
   * Processes requests for both HTTP
   * <code>GET</code> and
   * <code>POST</code> methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    PrintWriter out = null;
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    
    HttpSession sess = request.getSession();
    Integer user_id = (Integer)sess.getAttribute("user_id");
    Integer users_group_id = (Integer)sess.getAttribute("users_group_id");
    Integer users_session_id = (Integer)sess.getAttribute("users_session_id");
    
    List<String> list=new ArrayList<>();
    int result = 0;
    StringBuffer jb = new StringBuffer();
    String line;
    DBObject db = new DBObject(user_id, users_group_id, users_session_id);
    try {
      out = response.getWriter();
      BufferedReader reader = request.getReader();
      while ((line = reader.readLine()) != null) {
        jb.append(line);
      }
      JSONObject check_data = new JSONObject(jb.toString());
      // читаем заголовок чека
      int checkID = check_data.getInt("checkId");
      DateTime datePurchase = new DateTime(check_data.getInt("checkDate"));
      int byer_user_id = (int)user_id;
      int users_place_id = check_data.getInt("placeId");
      String place_name = String.valueOf(check_data.getString("placeName"));
        
      // читаем позиции чека
      StringBuffer group_name = new StringBuffer();
      StringBuffer brand_name = new StringBuffer();
      int group_id, brand_id, unit_id, currency_id;
      Double price, unitsInStock;
        
      JSONArray positions = check_data.getJSONArray("positions");
       
      StringBuffer checkRows = new StringBuffer();
      checkRows.append("<ch>");
      for (int i = 0; i < positions.length(); i++){
        JSONObject check_position = positions.getJSONObject(i);
        /*
         * MySQL need:
         *  SET groupName = extractvalue(in_xml, '/ch/r[$iter]/g/text()');
         *  SET groupId = extractvalue(in_xml, '/ch/r[$iter]/gi');
         *  SET brandName = extractvalue(in_xml, '/ch/r[$iter]/b/text()');
         *  SET brandId = extractvalue(in_xml, '/ch/r[$iter]/bi');
         *  SET unitId = extractvalue(in_xml, '/ch/r[$iter]/ui');
         *  SET productPrice = extractvalue(in_xml, '/ch/r[$iter]/pr');
         *  SET currencyID = extractvalue(in_xml, '/ch/r[$iter]/ci');
         *  SET unitsInStock = extractvalue(in_xml, '/ch/r[$iter]/s');
         */
        checkRows.append("<r>");
        group_name.delete(0, group_name.length());
        group_name.append(check_position.getString("group_name"));
        checkRows
                .append("<g>")
                .append("<![CDATA[")
                .append(group_name)
                .append("]]>")
                .append("</g>");
        group_id = check_position.getInt("group_id");
        checkRows
                .append("<gi>")
                .append(group_id)
                .append("</gi>");
        brand_name.delete(0, brand_name.capacity());
        brand_name.append(check_position.getString("brand_name"));
        checkRows
                .append("<b>")
                .append("<![CDATA[")
                .append(brand_name)
                .append("]]>")
                .append("</b>");
        brand_id = check_position.getInt("brand_id");
        checkRows
                .append("<bi>")
                .append(brand_id)
                .append("</bi>");
        unit_id = check_position.getInt("unit_id");
        checkRows
                .append("<ui>")
                .append(unit_id)
                .append("</ui>");
        price = check_position.getDouble("price");
        checkRows
                .append("<pr>")
                .append(price)
                .append("</pr>");
        currency_id = check_position.getInt("currency_id");
        checkRows
                .append("<ci>")
                .append(currency_id)
                .append("</ci>");
        unitsInStock = check_position.getDouble("unitsInStock");
        checkRows
                .append("<s>")
                .append(unitsInStock)
                .append("</s>");
        checkRows.append("</r>");
      }
      checkRows.append("</ch>");
      
      db.start_stransaction();      
      result = db.create_user_check(byer_user_id, users_place_id, place_name, datePurchase, checkRows.toString());
      log.info ("commit");
      db.commit();
    } catch (JSONException ex) {
      log.log(Level.INFO, "JSON data loading failed! JSONException: {0}", ex);
      result = -2;
    } catch (SQLException e) {
      log.log(Level.INFO, "SQL! Inserting check data to DB failed! Exception: {0}", e);
      result = -3;      
    } catch (Exception e) {
      log.log(Level.INFO, "Inserting check data to DB failed! Exception: {0}", e);
      result = -1;
    } finally {
      // ответ на запрос AJAX отправляем в любом случае
      try{
        list.add(String.valueOf(result));
        JSONArray orgs = new JSONArray();
        Iterator it = list.iterator();
        while(it.hasNext()){
          String value = String.valueOf(it.next());
          JSONObject json = new JSONObject();
          json.put("result", value);
          orgs.put(json);
        }
        out.print(orgs);
      } catch (Exception ex) {
        log.log(Level.INFO, "JSON ouput failed! Exception: {0}", ex);
      } finally {
        try {
          db.rollback();          
          db.close();
        } catch (SQLException ex) {
          log.log(Level.INFO, "Connection not closed! SQLException: {0}", ex);
        } finally {
          out.close();
        }
      }
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP
   * <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP
   * <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}
