package homerecord;

import dbModel.DBObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Alexander Medvedev
 */
public class get_json_organizations extends HttpServlet {

  private static final Logger log = Logger.getLogger(get_json_organizations.class.getName());
  /**
   * Processes requests for both HTTP
   * <code>GET</code> and
   * <code>POST</code> methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DBObject db = null;
    PrintWriter out = null;
    JSONArray orgs = new JSONArray();
    
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    
    HttpSession sess = request.getSession();
    Integer user_id = (Integer)sess.getAttribute("user_id");
    Integer users_group_id = (Integer)sess.getAttribute("users_group_id");
    Integer users_session_id = (Integer)sess.getAttribute("users_session_id");
    
    log.log(Level.INFO, "get_json_organization :: USER_ID = {0}", (int)user_id);
    log.log(Level.INFO, "get_json_organization :: USERS_GROUP_ID = {0}", (int)users_group_id);
    
    try{
      if (user_id != null){
        String filter_text = request.getParameter("filter[filters][0][value]");
        out = response.getWriter();
        if (filter_text != null){
          db = new DBObject(user_id, users_group_id, users_session_id);
          log.log(Level.INFO, "get_json_organization :: FILTER TEXT = {0}", filter_text);
          Map<Integer, String> places = db.get_user_places(filter_text);
          if (places != null){
            Iterator<Map.Entry<Integer, String>> iter = places.entrySet().iterator();
            while (iter.hasNext()) {
              Entry<Integer, String> place= iter.next();
              JSONObject json = new JSONObject();
              json.put("OrganizationId", (int)place.getKey());
              json.put("Organization", place.getValue());
              orgs.put(json);
            }
          }
        }
      }
      log.log(Level.INFO, "get_json_organization :: SENDING JSON ARRAY{0}", orgs);
      out.print(orgs);
    } catch (JSONException ex) {
      log.log (Level.INFO, "JSONException: {0}", ex);
    }  catch (Exception e) {
      log.log(Level.INFO, "Geting list of organizations from db failed! Exception: {0}", e);
    } finally {
      db.close();
      out.close();
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP
   * <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP
   * <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /*
    try{
      JSONObject json = new JSONObject();
      json.put("Organization", "магазин");
      json.put("Organization", "ларек");
      json.put("Organization", "заправка");
      //json.toJSONArray();
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");
      response.getWriter().print("sdfsdf");
      response.getWriter().print(json);
    } catch (JSONException ex) {
      Logger.getLogger(organizations.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      processRequest(request, response);
    }
    */

    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}
