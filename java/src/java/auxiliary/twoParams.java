/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliary;

public class twoParams <T1, T2> {
    public final T1 first;
    public final T2 second;

    public twoParams(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }
}