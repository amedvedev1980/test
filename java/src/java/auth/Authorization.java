/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auth;

import auxiliary.check_email;
import auxiliary.twoParams;
import dbModel.DBObject;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Администратор
 */
/*@WebServlet(name = "Authorization", urlPatterns = {"/Authorization"})*/
public class Authorization extends HttpServlet {

  private static final Logger log = Logger.getLogger(Authorization.class.getName());

  private String getMD5(String str) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(str.getBytes());
    byte byteData[] = md.digest();
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < byteData.length; i++) {
      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

  /**
   * Processes requests for both HTTP
   * <code>GET</code> and
   * <code>POST</code> methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @SuppressWarnings("unchecked")
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    RequestDispatcher dispatcher;
    
    String login = request.getParameter("login");
    String pass = request.getParameter("password");
    if (login == null || pass == null || login.length() == 0 || pass.length() == 0) {
      // такое может прийти только "со стороны"
      // переходим обратно
      dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
      if (dispatcher != null) {
        dispatcher.forward(request, response);
        return;
      }
    }
    boolean is_email = check_email.doMatch(login);
    String email = (is_email) ? login : "";
    
    try {
      HttpSession session = request.getSession();

      DBObject db = new DBObject();
      // закрываем предыдущую сессию
      //session.invalidate();
      // проверяем есть ли такой пользователь в БД
      pass = getMD5(pass);
      if (!db.check_user(login, email, pass))
      {
        if (db.get_user_id() == null && is_email) {
          // добавляем нового пользователя в базу, если указан email
          // и осуществляем вход в систему
          login = "noname";
          db.create_user(login, email, pass, 1, 1, 1);
        }
      }
      // вход в систему
      Integer user_id = Integer.valueOf(db.get_user_id());
      if (user_id != null)
      {
        twoParams<String,String> user_info_res;
        user_info_res = db.get_user_info();
        login = user_info_res.first;
        email = user_info_res.second;
        // регистрируем сессию
        db.session();
        session.setAttribute("user", (login.indexOf("noname") == 0 ? email : login));
        session.setAttribute("users_session_id", db.get_users_session_id());
        session.setAttribute("user_id", (int)user_id);
        session.setAttribute("users_group_id", db.get_users_group_id());        
        session.setAttribute("country_id", 1);
        session.setAttribute("language_id", 1);
        session.setAttribute("currency_id", 1);
        // переходим на главную
        dispatcher = getServletContext().getRequestDispatcher("/private/index.jsp");
        if (dispatcher != null) {
          dispatcher.forward(request, response);
          return;
        }          
      }
    } catch (Exception e) {
      log.log(Level.INFO, "Exception: {0}", e);
    }
    
    // переходим обратно
    dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
    if (dispatcher != null) {
      dispatcher.forward(request, response);
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP
   * <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP
   * <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}
