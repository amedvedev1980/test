package dbModel;

import auxiliary.twoParams;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.joda.time.DateTime;

public class DBObject {
  
  private static final Logger log = Logger.getLogger(DBObject.class.getName());

  private Connection connection = null;
  private Integer user_id = null;
  private Integer users_session_id = null;
  private Integer users_group_id = null;
  private boolean transaction_on = false;
  
  public Map<Integer, String> get_users_products(int products_group_id, String filterText) throws SQLException {
    Map<Integer, String> items = new HashMap<>();
    CallableStatement sql_proc = connection.prepareCall("{ call get_user_products(?, ?, ?) }");
    sql_proc.setInt(1, this.users_group_id);
    sql_proc.setInt(2, products_group_id);
    sql_proc.setString(3, filterText);
    ResultSet rs = (ResultSet) sql_proc.executeQuery();
    while (rs.next()) {
      Integer id = Integer.valueOf(rs.getInt(1));
      String name = String.valueOf(rs.getString(2));
      items.put(id, name);
    }
    return (items.isEmpty() ? null : items);
  }
  public Map<Integer, String> get_products_groups (String filterText) throws SQLException {
    Map<Integer, String> items = new HashMap<>();
    CallableStatement sql_proc = connection.prepareCall("{ call get_products_groups(?, ?) }");
    sql_proc.setInt(1, this.users_group_id);
    sql_proc.setString(2, filterText);
    ResultSet rs = (ResultSet) sql_proc.executeQuery();
    while (rs.next()) {
      Integer id = Integer.valueOf(rs.getInt(1));
      String name = String.valueOf(rs.getString(2));
      items.put(id, name);
    }
    return (items.isEmpty() ? null : items);
  }
  public Map<Integer, String> get_units(int country_id, String filterText) throws SQLException{
    Map<Integer, String> items = new HashMap<>();
    CallableStatement sql_proc = connection.prepareCall("{ call get_units(?, ?) }");
    sql_proc.setInt(1, country_id);
    sql_proc.setString(2, filterText);
    ResultSet rs = (ResultSet) sql_proc.executeQuery();
    while (rs.next()) {
      Integer id = Integer.valueOf(rs.getInt(1));
      int parentId = rs.getInt(2);
      int unit_group_id = rs.getInt(3);
      double multiplier = rs.getDouble(4);
      boolean is_international = rs.getBoolean(5);
      String code = String.valueOf(rs.getString(6));
      String symbol = String.valueOf(rs.getString(7));
      String name = String.valueOf(rs.getString(8));
      items.put(id, symbol);
    }
    return (items.isEmpty() ? null : items);
  }
  public Map<Integer, String> get_user_places(String filterText) throws SQLException{
    CallableStatement sql_proc = connection.prepareCall("{ call get_user_places(?, ?) }");
    sql_proc.setInt(1, (int)this.users_group_id);
    sql_proc.setString(2, filterText);
    ResultSet rs = (ResultSet) sql_proc.executeQuery();
    Map<Integer, String> places = new HashMap<>();    
    while (rs.next()) {
      Integer placeId = Integer.valueOf(rs.getInt(1));
      int parentId = rs.getByte(2);
      String placeName = String.valueOf(rs.getString(3));
      if (parentId == 0) {
        places.put(placeId, placeName);
      }
    }
    return (places.isEmpty() ? null : places);
  }
  public int create_user_check(
          Integer byer_user_id, 
          Integer users_place_id, 
          String users_place_name,
          DateTime date_purchase,
          String rows_xml) throws SQLException{
    CallableStatement sql_proc = connection.prepareCall("{ call create_user_check(?, ?, ?, ?, ?, ?, ?, ?) }");
    Timestamp timeStamp = (date_purchase != null ? new Timestamp(date_purchase.getMillis()) : null);
    sql_proc.setInt(1, (int)this.users_session_id);
    sql_proc.setInt(2, (int)this.users_group_id);
    sql_proc.setInt(3, byer_user_id);
    sql_proc.setInt(4, users_place_id);
    sql_proc.setString(5, users_place_name);
    sql_proc.setTimestamp(6, timeStamp);
    sql_proc.setString(7, rows_xml);
    sql_proc.registerOutParameter(8, Types.INTEGER);
    sql_proc.execute();
    int users_check_id = sql_proc.getInt(8);
    return users_check_id;
  }
  public int create_users_session() throws SQLException{
    CallableStatement sql_proc = this.connection.prepareCall("{ call create_user_session(?, ?) }");
    sql_proc.setInt(1, (int)this.user_id);
    sql_proc.registerOutParameter(2, Types.INTEGER);
    sql_proc.execute();
    int db_users_session_id = sql_proc.getInt(2);
    return db_users_session_id;
  }
  public twoParams get_user_info() throws SQLException{
    CallableStatement sql_proc = connection.prepareCall("{ call get_user_info(?, ?, ?) }");
    sql_proc.setInt(1, (int)this.user_id);
    sql_proc.registerOutParameter(2, Types.VARCHAR);
    sql_proc.registerOutParameter(3, Types.VARCHAR);
    sql_proc.execute();
    String login = sql_proc.getString(2);
    String email = sql_proc.getString(3);
    twoParams<String,String> result = new twoParams<>(login, email);
    return result;
  }
  public void create_user(
          String login, 
          String email, 
          String password,
          int country_id,
          int language_id,
          int currency_id) throws SQLException{
    CallableStatement sql_proc = connection.prepareCall("{ call create_user(?, ?, ?, ?, ?, ?, ?) }");
    sql_proc.setString(1, login);
    sql_proc.setString(2, email);
    sql_proc.setString(3, password);
    sql_proc.setInt(4, country_id);
    sql_proc.setInt(5, language_id);
    sql_proc.setInt(6, currency_id);
    sql_proc.registerOutParameter(7, Types.INTEGER);
    sql_proc.execute();
    int db_user_id = sql_proc.getInt(7);
    if (db_user_id > 0) {
      this.user_id = Integer.valueOf(db_user_id);
    }
  }
  public boolean check_user(String login, String email, String password) throws SQLException{
    boolean result = false;
    CallableStatement sql_proc = this.connection.prepareCall("{ call check_user(?, ?, ?, ?, ?) }");
    sql_proc.setString(1, login);
    sql_proc.setString(2, email);
    sql_proc.registerOutParameter(3, Types.VARCHAR);
    sql_proc.registerOutParameter(4, Types.INTEGER);
    sql_proc.registerOutParameter(5, Types.INTEGER);
    sql_proc.execute();
    String db_password = sql_proc.getString(3);
    int db_user_id = sql_proc.getInt(4);
    if (db_user_id > 0)
    {
      this.user_id = Integer.valueOf(db_user_id);
      result = db_password.equalsIgnoreCase(password);
      int db_users_group_id =  sql_proc.getInt(5);
      if (db_users_group_id > 0) {
        this.users_group_id = Integer.valueOf(db_users_group_id);
      }
    }
    return result;
  }
  public void session() throws SQLException{
    this.users_session_id = Integer.valueOf(create_users_session());
  }
  public void start_stransaction() throws SQLException{
    this.connection.setAutoCommit(false);
    this.transaction_on = true;
  }
  public void commit() throws SQLException{
    this.connection.commit();
    this.connection.setAutoCommit(true);
    this.transaction_on = false;      
  }
  public void rollback() throws SQLException{
    if (this.transaction_on){
      this.connection.rollback();
      this.connection.setAutoCommit(true);
      this.transaction_on = false;
    }
  }
  public void close(){
    try {
      this.connection.close();
    } catch (SQLException ex) {
      log.log(Level.INFO, "SQLException: {0}", ex);
    }
  }
  public Integer get_user_id(){
    return this.user_id;
  }
  public int get_users_session_id(){
    return (int)this.users_session_id;
  }
  public int get_users_group_id(){
    return (int)this.users_group_id;
  }
  public DBObject(){
    Context ctx;
    try {
      ctx = new InitialContext();
      DataSource ds =
              (DataSource) ctx.lookup("java:comp/env/jdbc/homeorg");
      this.connection = ds.getConnection();
      this.connection.setAutoCommit(true);
    } catch (NamingException e) {
      log.log(Level.INFO, "Couldn''t build an initial context: {0}", e);
    } catch (Exception e) {
      log.log(Level.INFO, "Exception is: {0}", e);
    }
  }
  public DBObject(Integer user_id, Integer users_group_id, Integer users_session_id){
    this();
    this.user_id = user_id;
    this.users_group_id = users_group_id;
    this.users_session_id = users_session_id;
  }  

  protected void finalize () throws SQLException{
    this.close();
  }
}
