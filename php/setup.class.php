<?php
// Version:1.0

class Setup
{
  /**
   * The function that implements the execution of class actions
   * @throws Exception - if the configuration file contains errors, an exception is thrown
   */
  public function action()
  {
    try {
      $fileName = DOCA_ROOT . '/core/setup/setup.xml';
      $data = new SimpleXMLElement(file_get_contents($fileName));
      if ($actions = $data->actions) {
        foreach (array_keys(get_object_vars($actions)) as $action) {
          if ($action == 'comment') continue;
          printf("action: %s \n", $action);
          if (method_exists($this, $action)) {
            call_user_func(array($this, $action), $actions->{$action});
          } else {
            printf("error, did not finded method %s \n", $action);
          }
        }
      }
      printf(" === OK ==== \n");
    } catch (Exception $e) {
      printf("FAIL:\n %s \n", $e);
    }
  }

  private function exec($script)
  {
    $runScript = DOCA_ROOT . DIRECTORY_SEPARATOR . $script;
    printf("exec php %s:", $runScript);
    exec('php ' . $runScript, $output);
    if (is_array($output))
      printf("\n %s \n", implode(PHP_EOL, $output));
  }


  /**
   * The function deletes files and folders that are not contained
   * in registry file (.index)
   * @param $props - array with the folder names
   */
  private function index($data)
  {
    if (isset($data->path)) {
      foreach ($data->path as $path) {
        $path = (string)$path;
        printf("index %s \n", $path);
        if ($path = is_dir($path) ? $path :
          (is_dir(DOCA_ROOT . DIRECTORY_SEPARATOR . $path) ?
            DOCA_ROOT . DIRECTORY_SEPARATOR . $path : null)
        ) {
          $index = $path . DIRECTORY_SEPARATOR . '.index';
          if (file_exists($index)) {
            if ($data = json_decode(file_get_contents($index))) {
              self::test_dir($path, $data);
            }
          }
        } else {
          printf("error path: %s \n", $path);
        }
      }
    }
  }

  /**
   * Recursive function for delete files and folders if they are not
   * declared in the {$data} object properties
   * @param $path - path to current folder
   * @param $data - object with file and folder names
   */
  private function test_dir($path, $data)
  {
    $dir = opendir($path);
    while (($file = readdir($dir)) !== false) {
      if ($file != '.' && $file != '..') {
        if (!$data || !property_exists($data, $file)) {
          if (is_file($path . DIRECTORY_SEPARATOR . $file)) {
            unlink($path . DIRECTORY_SEPARATOR . $file);
          } else {
            Files::recursiveRemoveDir($path . DIRECTORY_SEPARATOR . $file);
          }
        } else {
          if (is_file($path . DIRECTORY_SEPARATOR . $file)) {
            chmod($path . DIRECTORY_SEPARATOR . $file, $data->{$file});
          } else {
            self::test_dir($path . DIRECTORY_SEPARATOR . $file, $data->{$file});
            chmod($path . DIRECTORY_SEPARATOR . $file, 0777);
          }
        }
      }
    }
  }

  /**
   * @param $data array of objects
   */
  private function references($data)
  {
    if (isset($data->reference)) {
      $items = array();
      foreach ($data->reference as $item) {
        $items[] = array_values((array)$item);
      }
      Prepare::set_refs($items);
    } else {
      foreach (array_keys(get_object_vars($data)) as $key) {
        if ($key != 'reference') {
          self::references($data->{$key});
        }
      }
    }
  }

  private function migration($folder)
  {
    printf('migration: %s' . EOLN, $folder);
    self::db_proc(DOCA_ROOT . DIRECTORY_SEPARATOR . $folder);
  }

  private function db_proc($path)
  {
    $dir = opendir($path);
    $log = array();
    try {
      if (!file_exists($path . DIRECTORY_SEPARATOR . '.norun')) {
        $log = array_merge($log, self::check_init($path));

        $_init = $path . DIRECTORY_SEPARATOR . 'init.sql';
        if (file_exists($_init)) {
          // выполняем SQL файл
          $log[] = array();
          if (!self::proc_sql(file_get_contents($_init), $log[count($log) - 1])) throw new Exception();
          @unlink($_init);
        }

        while (($file = readdir($dir)) !== false) {
          if ($file != '.' && $file != '..') {

            $newPath = $path . DIRECTORY_SEPARATOR . $file;
            if (is_dir($newPath)) {
              self::db_proc($newPath);
            } else { // FILE
              $info = pathinfo($newPath);
              if ($info['extension'] == 'sql') {
                // выполняем SQL файл
                $log[] = array();
                if (!self::proc_sql(file_get_contents($newPath), $log[count($log) - 1])) throw new Exception();
              }
            }
          }
        }

        $dir = opendir($path);
        // удаляем все файлы
        while (($file = readdir($dir)) !== false) {
          if ($file != '.' && $file != '..') {
            @unlink($path . DIRECTORY_SEPARATOR . $file);
          }
        }
        // пишем .success
        self::log_write($path . DIRECTORY_SEPARATOR . '.success', $log);
      }
    } catch (Exception $e) {
      $log[] = $e;
      // для данной папки не удалось выполнить какие-то скрипты, пишем лог
      self::log_write($path . DIRECTORY_SEPARATOR . '.fail', $log);
      throw new Exception($e);
    }
  }

  private function log_write($f, $data, $lev = 0)
  {
    if (!count($data)) return;
    if (is_string($f)) {
      $f = fopen($f, 'w');
    }
    foreach ($data as $msg) {
      if (is_array($msg)) self::log_write($f, $msg, $lev + 2);
      else {
        $lines = explode(PHP_EOL, $msg);
        foreach ($lines as $line) {
          if ($line) {
            fwrite($f, str_repeat(' ', $lev) . $line . PHP_EOL);
          }
        }
      }
    }
    if (!$lev) fclose($f);
  }

  private function base_descr($m)
  {
    return $m[1] . '/*' . $m[2] . ':' . Auxf::base64($m[3]) . '*/';
  }

  private function proc_sql($data, &$log, $curr = ';', $level = 0)
  {
    try {
      $_field = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
      if ($level == 0) {
        // del /* ... */
        $data = preg_replace('/\/\*.*\*\//Us', '', $data);

        // base64 table:descr
        $data = preg_replace_callback("/^(\s*\!?@*\s*CREATE\s+TABLE\s+({$_field})\s+.*)\-{2,} (.*)$/Um", array($this, 'base_descr'), $data);
        // base64 field:descr
        $data = preg_replace_callback("/^(\s*,?\s*({$_field})\s+.*)\-{2,} (.*)$/Um", array($this, 'base_descr'), $data);

        // удаляем комментарии
        $data = preg_replace('/\-{2,}.*$/m', '', $data);
      }
      if (preg_match('/^\s*#R\s+DROP\s+(\w+)\s+(' . $_field . ')\s*' . preg_quote($curr) . '\n?(.*)/si', $data, $matches)) {
        $ttype = mb_strtoupper(trim($matches[1]), 'UTF-8');
        $name = trim($matches[2]);
        try {
          Sql::trans(IBASE_WRITE | IBASE_CONCURRENCY | IBASE_WAIT);
          switch ($ttype) {
            case 'TABLE':
              if (!SqlMeta::drop_table($name, $log)) throw new Exception();
              break;
            case 'PROCEDURE':
              if (!SqlMeta::drop_proc($name, $log)) throw new Exception();
              break;
          }
          Sql::commit();
        } catch (Exception $e) {
          Sql::rollback();
          throw new Exception($e);
        }
        $data = trim($matches[3]);
        if ($data) {
          if (!self::proc_sql($data, $log, $curr, $level)) throw new Exception();
        }
      } elseif (preg_match('/(?<![\S\n])^\s*SET\s+TERM\s+(\S+)\s+(\S+)\n(.*)/msi', $data, $matches)) {
        $curr = $matches[1];
        $data = trim($matches[3]);
        if ($data) {
          $log[] = array();
          if (!self::proc_sql($data, $log[count($log) - 1], $curr, $level + 1)) throw new Exception();
        }
      } elseif (preg_match('/^(.*)' . preg_quote($curr) . '/Usi', $data, $matches)) {
        $sql = trim($matches[1]);

        if ($sql) {
          $continue_if_true = false;
          if (preg_match('/^\s*\?@.*/Usi', $sql)) {
            $sql = preg_replace('/^\s*\?@/Usi', '', $sql);
            $continue_if_true = true;
          }

          $continue_if_error = false;
          if (preg_match('/^\s*\!@.*/Usi', $sql)) {
            $sql = preg_replace('/^\s*\!@/Usi', '', $sql);
            $continue_if_error = true;
          }

          $ignore_other = false;
          if (preg_match('/^\s*@@.*/Usi', $sql)) {
            $sql = preg_replace('/^\s*@@/Usi', '', $sql);
            $ignore_other = true;
          }

          $ignore_error = false;
          if (preg_match('/^\s*@.*/Usi', $sql)) {
            $sql = preg_replace('/^\s*@/Usi', '', $sql);
            $ignore_error = true;
          }

          $sql = preg_replace('/' . preg_quote($curr) . '/Usi', '', $sql);

          unset($fields);
          unset($tableName);
          unset($tableDescr);
          unset($trigger);
          $generator = $forTable = $idfield = null;
          if (preg_match('/^\s*CREATE TABLE\s+(.*)\(/Usi', $sql, $tname)) {
            $tableName = trim($tname[1]);
            //$log[] = 'TABLE: ' . $tableName;
            // преобразование VARCHAR(*) -> VARCHAR(*) CHARACTER SET WIN1251 COLLATE PXW_CYRL
            $sql = preg_replace('/VARCHAR\s*\((.*)\)/Usi', 'VARCHAR($1) CHARACTER SET WIN1251 COLLATE PXW_CYRL', $sql);

            // describes
            $_field = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
            if (preg_match("/\s*CREATE\s+TABLE\s.*\/\*({$_field})\:(.*)\*\//m", $sql, $matches)) {
              $tableDescr = Auxf::unbase64($matches[2]);
            }

            if (preg_match_all("/\/\*({$_field})\:(.*)\*\//m", $sql, $matches)) {
              foreach ($matches[1] as $key => $field) {
                $fields[$field] = Auxf::unbase64($matches[2][$key]);
                // $log[] = $field . ' = ' . $fields[$field];
              }
            }
            // del describes
            $sql = preg_replace('/\/\*.*\*\//Us', '', $sql);

            // #commands for fields
            $autoinc = null;
            if (preg_match("/^\s*,?\s*({$_field})\s+.* #AUTOINC.*$/Um", $sql, $matches)) {
              $autoinc = $matches[1];
              $sql = preg_replace('/#AUTOINC/', '', $sql);
            }

            $sql = array($sql);
            $sql[] = 'GRANT ALL ON ' . $tableName . ' TO PUBLIC';

            if ($autoinc) {
              $trigger = 'GENERATE_' . $tableName;
              $forTable = $tableName;
              $idField = $autoinc;
              $generator = $tableName . '_GEN';
            }
          } // end create table analys

          // autoincrement
          if (isset($trigger) || (!is_array($sql) && preg_match('/AUTOINC\s*\((.*),(.*),(.*),(.*)\)/', $sql, $matches))) {
            if (!isset($trigger)) {
              $trigger = trim($matches[1]);
              $forTable = trim($matches[2]);
              $idField = trim($matches[3]);
              $generator = trim($matches[4]);
            }
            $sql = is_array($sql) ? $sql : array();
            $sql[] = '@DROP GENERATOR ' . $generator;
            $sql[] = 'CREATE GENERATOR ' . $generator;
            $sql[] = <<<SQL
CREATE TRIGGER {$trigger} FOR {$forTable} ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp INTEGER;
BEGIN
  IF (NEW.{$idField} IS NULL) THEN
    NEW.{$idField}=GEN_ID({$generator},1);
  ELSE BEGIN
    tmp = GEN_ID({$generator},0);
    IF (tmp < NEW.{$idField}) THEN tmp = GEN_ID({$generator}, NEW.{$idField}-tmp);
  END
END
SQL;
          }

          if (!is_array($sql)) $sql = array($sql);

          // execute SQL
          foreach ($sql as $cmd) {
            $ignore_error_cmd = false;
            if ($cmd[0] == '@') {
              $cmd = substr($cmd, 1);
              $ignore_error_cmd = true;
            }
            $log[] = str_repeat('-', 80) . PHP_EOL . $cmd . PHP_EOL;

            try {
              Sql::trans(IBASE_WRITE | IBASE_CONCURRENCY | IBASE_WAIT);
              Sql::execute($cmd);
              $log[] = 'commit';
              Sql::commit();
              $log[] = '-- success';
              if ($continue_if_error) return true;
            } catch (Exception $e) {
              Sql::rollback();
              $log[] = '-- fail';
              if ($err = ibase_errmsg()) {
                $log[] = '/* ignore-error: ' . $err . '*/';
              }
              if (!$ignore_error && !$ignore_error_cmd) {
                if (!$continue_if_error) {
                  if (!$ignore_other) {
                    if ($continue_if_true) return true;
                    throw new Exception('ibase_errmsg: ' . ibase_errmsg() . PHP_EOL . $e);
                  } else {
                    return true;
                  }
                }
              }
            }
          }

          if (isset($fields) && isset($tableName)) {
            if ($rows = Sql::get_query(
              <<<SQL
              SELECT RDB\$FIELD_NAME AS FNAME, RDB\$FIELD_SOURCE AS FSOURCE
              FROM RDB\$RELATION_FIELDS
              WHERE RDB\$RELATION_NAME = '{$tableName}'
SQL
            )) {
              try {
                // table describe
                if (isset($tableDescr)) {
                  Sql::trans(IBASE_WRITE | IBASE_CONCURRENCY | IBASE_WAIT);
                  $log[] = "UPDATE RDB\$RELATIONS SET RDB\$DESCRIPTION='{$tableDescr}' WHERE RDB\$RELATION_NAME='{$tableDescr}'";
                  Sql::execute('UPDATE RDB$RELATIONS SET RDB$DESCRIPTION=? WHERE RDB$RELATION_NAME=?'
                    , $tableDescr, $tableName);
                  $log[] = 'commit';
                  Sql::commit();
                  $log[] = '-- success';
                }

                foreach ($rows as $row) {
                  $row->FNAME = trim($row->FNAME);
                  $row->FSOURCE = trim($row->FSOURCE);
                  if (array_key_exists($row->FNAME, $fields)) {
                    $cmd = 'UPDATE RDB$RELATION_FIELDS SET RDB$DESCRIPTION = \'' . $fields[$row->FNAME] .
                      '\' WHERE RDB$FIELD_NAME = \'' . $row->FNAME . '\' AND RDB$RELATION_NAME = \'' . $tableName . '\'';
                    $log[] = $cmd;
                    Sql::trans(IBASE_WRITE | IBASE_CONCURRENCY | IBASE_WAIT);
                    Sql::execute($cmd);
                    /*
                    Sql::execute('UPDATE RDB$RELATION_FIELDS SET RDB$DESCRIPTION = ? WHERE RDB$FIELD_NAME=? AND RDB$RELATION_NAME=?'
                      , $fields[$row->FNAME], $row->FNAME, $tableName);//$row->FSOURCE);
                    */
                    $log[] = 'commit';
                    Sql::commit();
                    $log[] = '-- success';
                  }
                }
              } catch (Exception $e) {
                Sql::rollback();
                $log[] = '-- fail';
                throw new Exception('ibase_errmsg: ' . ibase_errmsg() . PHP_EOL . $e);
              }
            }
          }
        }
        $data = trim(preg_replace('/^(.*)' . preg_quote($curr) . '/Usi', '', $data));
        if ($data) {
          $log[] = array();
          if (!self::proc_sql($data, $log[count($log) - 1], $curr, $level + 1)) throw new Exception();
        }
      }
    } catch (Exception $e) {
      $log[] = $e;
      return false;
    }
    return true;
  }

  private function check_init($path)
  {
    $log = array();
    $name = $path . DIRECTORY_SEPARATOR . '.init.json';
    if (file_exists($name)) {
      $data = json_decode(file_get_contents($name));
      foreach (get_object_vars($data) as $key => $vals) {
        switch ($key) {
          case 'drop':
            array_push($log, self::drop($vals));
            break;
        }
      }
    }
    return $log;
  }

  function drop($data)
  {
    $log = array();
    try {
      Sql::trans(IBASE_WRITE | IBASE_CONCURRENCY | IBASE_NOWAIT);
      foreach (get_object_vars($data) as $key => $vals) {
        switch ($key) {
          case 'tables':
            foreach ($vals as $table) {
              if (!SqlMeta::drop_table($table, $log)) throw new Exception();
            }
            break;
          case 'procedures':
            foreach ($vals as $proc) {
              if (!SqlMeta::drop_proc($proc, $log)) throw new Exception();
            }
            break;
        }
      }
      Sql::commit();
    } catch (Exception $e) {
      Sql::rollback();
      return $log;
    }
    return $log;
  }

}