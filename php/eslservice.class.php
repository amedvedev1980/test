<?php
// Version:1.0
// тестовая версия взаимодейcтвия с сервисом ФСС ЭЛН

class EslService
{
  private $cfg;
  private $xmode;
  private $service;
  private $user;
  private $patient;
  private $lpu;
  private $dis;
  private $lnCode;        // REQ, номер больничного
  private $lnCodePrev;    // OPT, номер предыдущего больничного
  private $primaryFlag;   // REQ, первичный или продление (1 или 0)
  private $duplicate;     // REQ, 1 - дубликат, 0 - оригинал
  private $lnDate;        // REQ, дата выдачи
  private $publicKey;
  private $privateKey;
  private $password;

  public function __construct(Disability $dis)
  {
    $this->cfg = new Config('SickList');
    $this->user = new User($dis->ID_USER);
    $this->patient = new Patient($dis->ID_PAT);
    $this->dis = $dis;
    $this->lnCode = $dis->ID;
    $this->lnDate = $dis->LN_DATE;
    $this->primaryFlag = $dis->PRIMARY_FLAG;
    $this->duplicate = $dis->DUPLICATE_FLAG;
    $this->lnCodePrev = null;
    $this->lpu = new Lpu();
    $this->xmode = false;
  }

  public function set_ext()
  {
    $this->xmode = true;
  }

  public function set_lnCode($code)
  {
    $this->lnCode = $code;
  }

  public function set_lnCodePrev($code)
  {
    $this->lnCodePrev = $code;
  }

  public function set_duplicate()
  {
    $this->duplicate = 1;
  }

  public function set_primary($primary = true)
  {
    $this->primaryFlag = $primary;
  }

  public function set_lnDate()
  {
    $this->lnDate = date('Y-m-d');
  }

  public function connect()
  {
    $fss = $this->cfg->get_item('FSS_publicKey');
    if (!file_exists($fss)) throw new Exception('Не установлен публичный сертификат ФСС');
    $private = $this->cfg->get_item('MO_privateKey');
    $private = basename($private);
    $privatePath = '/root/.itcs/vipnet-csp/containers/' . $private;
    if (!file_exists($privatePath))
      //throw new Exception('Не установлен контейнер с приватным ключом МО');
      $this->privateKey = $private;
    $public = $this->cfg->get_item('MO_publicKey');
    if (!file_exists($public)) throw new Exception('Не установлен публичный сертификат МО');
    $this->publicKey = $public;
    $passw = $this->cfg->get_item('MO_password');
    $this->password = $passw;
    $this->service = new FSSService($this->cfg->get_item('wsdl'),
      array('trace' => true));
    $this->service->set_certificate($private, $public, $passw);
    $this->service->set_encrypt();
  }

  public function del()
  {
    self::connect();
    $LPU_OGRN = $this->dis->LPU_OGRN;
    if (empty($LPU_OGRN)) throw new Exception('Не задан ОГРН МО.:' . $LPU_OGRN);
    $dis = $this->dis;
    $param = new DisableRequest();
    $param->ogrn = $LPU_OGRN;
    $param->lnCode = $dis->ID;
    $param->snils = $dis->SNILS;
    $param->reasonCode = '010';
    $param->reason = 'undefined';
    $bodyAttr = new SoapExt('OGRN_' . $LPU_OGRN, SignedSoapClient::WSU_NS);
    $bodyAttr->set_wsse();
    $bodyAttr->set_actor('http://eln.fss.ru/actor/mo/' . $LPU_OGRN);
    $bodyAttr->set_public_key($this->publicKey);
    $bodyAttr->set_private_key($this->privateKey);
    $bodyAttr->set_password($this->password);
    $this->service->body_attr['Id'] = $bodyAttr;
    $this->service->set_ogrn($LPU_OGRN);
    $ret = $this->service->disableLn($param);
    if (isset($ret['FileOperationsLnUserDisableLnOut']['STATUS']) &&
      $ret['FileOperationsLnUserDisableLnOut']['STATUS'] == 1) {
      $result['message'] = 'ЭЛН отменен';
      $this->dis->OLD_MARK = 1;
      $this->dis->save();
    } else {
      $result['error'] = 'Ошибка';
    }
    return $result;
  }

  public function getLnData($lnCode = null, $snils = null)
  {
    self::connect();
    $LPU_OGRN = $this->cfg->get_item('LPU_OGRN');
    if (empty($LPU_OGRN)) throw new Exception('Не задан ОГРН МО.:' . $LPU_OGRN);
    $dis = $this->dis;
    $param = new GetLnRequest();
    $param->ogrn = $LPU_OGRN;
    $param->lnCode = ($lnCode ? $lnCode : $dis->ID);
    $param->snils = ($snils ? $snils : $dis->SNILS);
    $bodyAttr = new SoapExt('OGRN_' . $LPU_OGRN, SignedSoapClient::WSU_NS);
    $bodyAttr->set_wsse();
    $bodyAttr->set_actor('http://eln.fss.ru/actor/mo/' . $LPU_OGRN);
    $bodyAttr->set_public_key($this->publicKey);
    $bodyAttr->set_private_key($this->privateKey);
    $bodyAttr->set_password($this->password);
    $this->service->body_attr['Id'] = $bodyAttr;
    $this->service->set_ogrn($LPU_OGRN);
    $result = $this->service->getLNData($param);
    return $result;
  }

  public function get_ln()
  {
    $result = null;
    self::connect();
    $LPU_OGRN = $this->dis->LPU_OGRN;
    if (empty($LPU_OGRN)) throw new Exception('Не задан ОГРН МО.:' . $LPU_OGRN);
    $param = new newLNNumRange();
    $param->ogrn = $LPU_OGRN;
    $param->cntLnNumbers = 1;
    $bodyAttr = new SoapExt('OGRN_' . $LPU_OGRN, SignedSoapClient::WSU_NS);
    $bodyAttr->set_wsse();
    $bodyAttr->set_actor('http://eln.fss.ru/actor/mo/' . $LPU_OGRN);
    $bodyAttr->set_public_key($this->publicKey);
    $bodyAttr->set_private_key($this->privateKey);
    $bodyAttr->set_password($this->password);
    $this->service->body_attr['Id'] = $bodyAttr;
    $this->service->set_ogrn($LPU_OGRN);
    $this->service->getNewLNNumRange($param);
    $ret = $this->service->__getLastResponse();
    $ret = new XmlParser($ret);
    $ret = $ret->getOutput();
    if (isset($ret['S:Envelope']['S:Body']['ns1:getNewLNNumRangeResponse']['ns1:fileOperationsLnUserGetNewLNNumRangeOut'])) {
      $ares = &$ret['S:Envelope']['S:Body']['ns1:getNewLNNumRangeResponse']['ns1:fileOperationsLnUserGetNewLNNumRangeOut'];
      if (isset($ares['ns1:STATUS']) && isset($ares['ns1:DATA']['ns1:LNNum'])) {
        $status = $ares['ns1:STATUS'];
        $value = $ares['ns1:DATA']['ns1:LNNum'];
        if ($status == 1 && is_numeric($value) && $value > 0) {
          $result = $value;
        }
      }
    }
    return $result;
  }

  public function send()
  {
    $result = null;
    $dis = $this->dis;
    $isElnOpen = false;
    if ($dis->MAININFO_STATUS == 1) {
      $hash = null;
      $data = self::getLnData();
      if (isset($data['FileOperationsLnUserGetLNDataOut']['DATA']['OUT_ROWSET']['ROW'])) {
        $data = $data['FileOperationsLnUserGetLNDataOut']['DATA']['OUT_ROWSET']['ROW'];
        $hash = isset($data['LN_HASH']) ? $data['LN_HASH'] : null;
      }
      if (!$hash) {
        throw new Exception('Не удалось получить информацию из ФСС. Продление не возможно. Посторите попытку позже или обратитесь к администратору.');
      }
    } else {
      $isElnOpen = true;
    }
    self::connect();
    try {
      $blocksSended = array();
      if ($this->lnCode === NULL) throw new Exception('не задан номер больничного');
      if ($this->primaryFlag === NULL) throw new Exception('не задан тип записи: первичный или продолжение');
      if ($this->lnDate === NULL) throw new Exception('не задана дата выдачи ЛН');
      $param = new prParseFilelnlpu($this->user);
      $param->request->ogrn = $this->cfg->get_item('LPU_OGRN');
      $pXmlFile = $param->request->pXmlFile;
      $row = $pXmlFile->ROWSET->add_row();
      $row->set_id($dis->ID, $dis->LPU_OGRN);
      $row->Id->set_public_key($this->publicKey);
      $row->Id->set_private_key($this->privateKey);
      $row->Id->set_password($this->password);
      $row->LN_STATE = '000';
      $isRowFill = false;

      // CLOSING_STATUS
      if ($dis->CLOSING_STATUS != 1) {
        if ($dis->RETURN_DATE_LPU > 0 || $dis->MSE_RESULT ||
          $dis->OTHER_STATE_DT > 0 || $dis->OTHER_STATE_DT) {
          $row->LN_RESULT = new LN_RESULT();
          $lnResult = $row->LN_RESULT;

          $lnResult->set_id($dis->ID);

          if ($dis->CLOSE_DOC_ID_USER) {
            $doctor = new User($dis->CLOSE_DOC_ID_USER);
            $publicKey = $doctor->get_public_key();
            $privateKey = $doctor->get_private_key();
            $lnResult->Id->set_public_key($publicKey);
            $lnResult->Id->set_private_key($privateKey);
            $lnResult->Id->set_password($dis->CLOSE_DOC_PIN);
          }

          if ($dis->RETURN_DATE_LPU > 0) {
            $lnResult->RETURN_DATE_LPU = Date::fdate('Y-m-d', $dis->RETURN_DATE_LPU);
          }

          if ($dis->MSE_RESULT) {
            $lnResult->MSE_RESULT = $dis->MSE_RESULT;
          }

          if ($dis->OTHER_STATE_DT > 0) {
            $lnResult->OTHER_STATE_DT = Date::fdate('Y-m-d', $dis->OTHER_STATE_DT);
          }

          if ($dis->NEXT_LN_CODE) {
            $lnResult->NEXT_LN_CODE = $dis->NEXT_LN_CODE;
          }

          $isRowFill = true;
          $dis->CLOSING_STATUS = 1;
          if ($dis->RETURN_DATE_LPU) {
            $dis->LN_STATE = '030';
          } elseif ($dis->NEXT_LN_CODE) {
            $dis->LN_STATE = '020';
          } else {
            throw new Exception('Ошибка в блоке закрытия ЭЛН.');
          }
          $blocksSended[] = 'close';
        }
      }
      // CLOSING_STATUS END

      // MAININFO_STATUS
      if ($dis->MAININFO_STATUS != 1) {
        $row->LN_CODE = $dis->ID;
        $row->PRIMARY_FLAG = $dis->PRIMARY_FLAG;
        $row->DUPLICATE_FLAG = $dis->DUPLICATE_FLAG;
        $row->LN_DATE = Date::fdate('Y-m-d', $dis->LN_DATE);
        if ($dis->PREV_LN_CODE) {
          $row->PREV_LN_CODE = $dis->PREV_LN_CODE;
        }
        if (!($row->REASON1 = $dis->REASON1)) {
          throw new Exception('не указана причина нетрудоспособности');
        }
        if ($dis->REASON2) {
          $row->REASON2 = $dis->REASON2;
        }
        if ($dis->REASON3) {
          $row->REASON3 = $dis->REASON3;
        }
        if ($dis->DIAGNOS) {
          $row->DIAGNOS = trim($dis->DIAGNOS);
        }
        $dis->MAININFO_STATUS = 1;
        $dis->LN_STATE = '010';
        $blocksSended[] = 'main_data';
        $isRowFill = true;
      } else {
        $row->LN_STATE = '000';
        $row->LN_HASH = $hash;
        $row->LN_CODE = $dis->ID;
        $row->PRIMARY_FLAG = $dis->PRIMARY_FLAG;
        $row->DUPLICATE_FLAG = $dis->DUPLICATE_FLAG;
        $row->LN_DATE = Date::fdate('Y-m-d', $dis->LN_DATE);
        $row->REASON1 = $dis->REASON1;

      }
      // MAININFO_STATUS END

      // INSURED_STATUS
      if ($dis->INSURED_STATUS != 1) {

        if (!($row->SURNAME = $dis->SURNAME)) {
          throw new Exception('нет фамилии пациента');
        }
        if (!($row->NAME = $dis->NAME)) {
          throw new Exception('нет имени пациента');
        }
        if ($dis->PATRONIMIC) {
          $row->PATRONIMIC = $dis->PATRONIMIC;
        }
        if (($row->GENDER = $dis->GENDER) === NULL) {
          throw new Exception('не указан пол пациента');
        }
        if (!($row->BIRTHDAY = $dis->BIRTHDAY)) {
          throw new Exception('нет даты рождения пациента');
        }
        $row->BIRTHDAY = Date::fdate('Y-m-d', $dis->BIRTHDAY);
        if (!($row->SNILS = $dis->SNILS)) {
          throw new Exception('нет СНИЛС пациента');
        }
        if ($dis->LPU_EMPLOYER) {
          $row->LPU_EMPLOYER = $dis->LPU_EMPLOYER;
        }
        if (is_numeric($dis->BOZ_FLAG) && ($dis->BOZ_FLAG >= 0)) {
          $row->BOZ_FLAG = $dis->BOZ_FLAG;     // Состоит на учете в государственных учреждениях службы занятости
          //    1-состоит
          //    0-нет
        }
        if (is_numeric($dis->LPU_EMPL_FLAG) && ($dis->LPU_EMPL_FLAG >= 0)) {
          $row->LPU_EMPL_FLAG = $dis->LPU_EMPL_FLAG;
        }
        if ($dis->PARENT_CODE) {
          $row->PARENT_CODE = $dis->PARENT_CODE;  // Листок нетрудоспособности: Номер ЛН, предъявляемого на основном месте работы
        }
        $dis->INSURED_STATUS = 1;
        $blocksSended[] = 'patient';
        $isRowFill = true;
      } else {
        $row->SURNAME = $dis->SURNAME;
        $row->NAME = $dis->NAME;
        if ($dis->PATRONIMIC) {
          $row->PATRONIMIC = $dis->PATRONIMIC;
        }
        $row->GENDER = $dis->GENDER;
        $row->BIRTHDAY = Date::fdate('Y-m-d', $dis->BIRTHDAY);
        $row->SNILS = $dis->SNILS;
        $row->BOZ_FLAG = $dis->BOZ_FLAG;
        if ($dis->LPU_EMPLOYER) {
          $row->LPU_EMPLOYER = $dis->LPU_EMPLOYER;
        }
        if (is_numeric($dis->LPU_EMPL_FLAG) && ($dis->LPU_EMPL_FLAG >= 0)) {
          $row->LPU_EMPL_FLAG = $dis->LPU_EMPL_FLAG;
        }
      }
      // INSURED END

      // REQUISITES_STATUS
      if ($dis->REQUISITES_STATUS != 1) {
        if (!($row->LPU_OGRN = $dis->LPU_OGRN)) {
          throw new Exception('нет ОГРН ЛПУ');
        }
        if ($dis->LPU_NAME) {
          $row->LPU_NAME = $dis->LPU_NAME;
        }
        if ($dis->LPU_ADDRESS) {
          $row->LPU_ADDRESS = $dis->LPU_ADDRESS;
        }
        $dis->REQUISITES_STATUS = 1;
        $blocksSended[] = 'requisites';
        $isRowFill = true;
      } else {
        $row->LPU_OGRN = $dis->LPU_OGRN;
        $row->LPU_NAME = $dis->LPU_NAME;
        $row->LPU_ADDRESS = $dis->LPU_ADDRESS;
      }
      // REQUISITES END

      // SANATORIUM_STATUS
      if ($dis->SANATORIUM_STATUS != 1) {
        if (($dis->DATE1 > 0) && ($dis->DATE2 > 0) &&
          !empty($dis->VOUCHER_NO) && !empty($dis->VOUCHER_OGRN)) {
          if ($dis->DATE1) {
            $row->DATE1 = Date::fdate('Y-m-d', $dis->DATE1);
          }
          if ($dis->DATE2) {
            $row->DATE2 = Date::fdate('Y-m-d', $dis->DATE2);
          }
          if ($dis->VOUCHER_NO) {
            $row->VOUCHER_NO = $dis->VOUCHER_NO;
          }
          if ($dis->VOUCHER_OGRN) {
            $row->VOUCHER_OGRN = $dis->VOUCHER_OGRN;
          }
          $dis->SANATORIUM_STATUS = 1;
          $blocksSended[] = 'sanatorium';
          $isRowFill = true;
        }
      }
      // SANATORIUM_STATUS END

      // CARING_STATUS
      // ToDo: переделать на таблицу
      if ($dis->CARING_STATUS != 1) {
        if (($dis->SERV1_AGE > 0 || $dis->SERV1_MM > 0) &&
          ($dis->SERV1_RELATION_CODE > 0) && !empty($dis->SERV1_FIO)) {
          if ($dis->SERV1_AGE) {
            $row->SERV1_AGE = $dis->SERV1_AGE;
          }
          if ($dis->SERV1_MM) {
            $row->SERV1_MM = $dis->SERV1_MM;
          }
          if ($dis->SERV1_RELATION_CODE) {
            $row->SERV1_RELATION_CODE = $dis->SERV1_RELATION_CODE;
          }
          if ($dis->SERV1_FIO) {
            $row->SERV1_FIO = $dis->SERV1_FIO;
          }
          if ($dis->SERV2_AGE) {
            $row->SERV2_AGE = $dis->SERV2_AGE;
          }
          if ($dis->SERV2_MM) {
            $row->SERV2_MM = $dis->SERV2_MM;
          }
          if ($dis->SERV2_RELATION_CODE) {
            $row->SERV2_RELATION_CODE = $dis->SERV2_RELATION_CODE;
          }
          if ($dis->SERV2_FIO) {
            $row->SERV2_FIO = $dis->SERV2_FIO;
          }
          $dis->CARING_STATUS = 1;
          $blocksSended[] = 'caring';
          $isRowFill = true;
        }
      }
      // CARING_STATUS END

      // PREGNANCY_STATUS
      if ($dis->PREGNANCY_STATUS != 1) {
        if (is_numeric($dis->PREGN12W_FLAG) && ($dis->PREGN12W_FLAG >= 0)) {
          $row->PREGN12W_FLAG = $dis->PREGN12W_FLAG;
          $dis->PREGNANCY_STATUS = 1;
          $blocksSended[] = 'gestation';
          $isRowFill = true;
        }
      }
      // PREGNANCY_STATUS END

      // HOSPITAL_STATUS
      if ($dis->HOSPITAL_STATUS != 1) {
        if ($dis->HOSPITAL_DT1 > 0 && $dis->HOSPITAL_DT2 > 0) {
          if ($dis->HOSPITAL_DT1) {
            $row->HOSPITAL_DT1 = Date::fdate('Y-m-d', $dis->HOSPITAL_DT1);
          }
          if ($dis->HOSPITAL_DT2) {
            $row->HOSPITAL_DT2 = Date::fdate('Y-m-d', $dis->HOSPITAL_DT2);
          }
          $dis->HOSPITAL_STATUS = 1;
          $blocksSended[] = 'hospital';
          $isRowFill = true;
        }
      }
      // HOSPITAL_STATUS END

      // VIOLATION_STATUS
      if ($dis->VIOLATION_STATUS != 1) {
        if ($dis->VIOL_CODE && $dis->VIOL_DATE && $dis->VIOL_SIGN && $dis->VIOL_PIN) {
          if ($vUser = new User($dis->VIOL_SIGN)) {

            $row->HOSPITAL_BREACH = new HOSPITAL_BREACH();
            $row->HOSPITAL_BREACH->HOSPITAL_BREACH_CODE = $dis->VIOL_CODE;
            $row->HOSPITAL_BREACH->HOSPITAL_BREACH_DT = Date::fdate('Y-m-d', $dis->VIOL_DATE);

            $row->HOSPITAL_BREACH->setup($dis->ID);
            $publicKey = $vUser->get_public_key();
            $privateKey = $vUser->get_private_key();
            $row->HOSPITAL_BREACH->sign($publicKey, $privateKey, $dis->VIOL_PIN);

            $dis->VIOLATION_STATUS = 1;
            $blocksSended[] = 'violation';
            $isRowFill = true;
          }
        }
      }
      // VIOLATION_STATUS END

      // BUREAU_STATUS
      if ($dis->BUREAU_STATUS != 1) {
        if ($dis->MSE_DT1) {
          $row->MSE_DT1 = Date::fdate('Y-m-d', $dis->MSE_DT1);
          $row->MSE_DT2 = Date::fdate('Y-m-d', $dis->MSE_DT2);
          $row->MSE_DT3 = Date::fdate('Y-m-d', $dis->MSE_DT3);
          $row->MSE_INVALID_GROUP = $dis->MSE_INVALID_GROUP ? $dis->MSE_INVALID_GROUP : '';
          $dis->BUREAU_STATUS = 1;
          $blocksSended[] = 'expertise';
          $isRowFill = true;
        }
      }
      // BUREAU_STATUS END

      $items = $dis->disabilityitem();
      $periodsNum = 0;
      foreach ($items as $item) {
        $cPos = $item->POS;

        $fullperiod = $row->TREAT_PERIODS->add();
        $period = $fullperiod->TREAT_PERIOD;
        $period->setup($dis->ID, $cPos);

        $doctor = new User($item->ID_USER);
        $publicKey = $doctor->get_public_key();
        $privateKey = $doctor->get_private_key();
        $password = $item->PIN;

        if ($item->LN_STATE != 1 && !$item->FROM_FSS) { // еще не отправленный период
          $item->LN_STATE = 1;
          $isRowFill = true;
          if ($dis->LN_STATE == '010' && !$isElnOpen) {
            $dis->LN_STATE = '020';
          }
          $period->sign($publicKey, $privateKey, $password);
          $item->ACTOR = $period->Id->get();

          if ($item->VK_STATUS == 2) {
            if (strlen($item->VK_PIN) > 0) {
              $fullperiod->setup($dis->ID, $cPos);
              $vk_doctor = new User($item->ID_USER_VK);
              $publicKey = $vk_doctor->get_public_key();
              $privateKey = $vk_doctor->get_private_key();
              $fullperiod->sign($publicKey, $privateKey, $item->VK_PIN);

              $item->VK_PIN = '';
            }
          }
        } else {
          if (!$item->FROM_FSS) {
            $period->Id->set_binary_security_token($item->BINARY_SECURITY_TOKEN);
            $period->Id->set_digest_value($item->DIGEST_VALUE);
            $period->Id->set_signature_value($item->SIGNATURE_VALUE);
          }
          $period->Id->set_has_sign();
        }

        if ($item->VK_STATUS == 2) {
          $fullperiod->TREAT_CHAIRMAN_ROLE = $item->USER_VK_ROLE;
          $fullperiod->TREAT_CHAIRMAN = $item->USER_VK;
        }

        $period->TREAT_DOCTOR = (is_object($doctor) ? $doctor->fio() : '');
        $period->TREAT_DOCTOR_ROLE = (is_object($doctor) ? $doctor->get_role($item->ID_USER_ROLE) : '');
        $period->TREAT_DT1 = Date::fdate('Y-m-d', $item->DATE_BEGIN);
        $period->TREAT_DT2 = Date::fdate('Y-m-d', $item->DATE_END);
        $periodsNum++;
        //}
      }
      if (!$periodsNum) {
        throw new Exception("не задан период больничного для отправки");
      }

      $this->service->set_ogrn($dis->LPU_OGRN);
      $this->service->set_eln($dis->ID);

      $this->service->add_ns('ds', 'http://www.w3.org/2000/09/xmldsig#');
      $this->service->add_ns('wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');
      $this->service->add_ns('wsu', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');
      $this->service->add_ns('xsd', 'http://www.w3.org/2001/XMLSchema');

      if (!$isRowFill) {
        throw new Exception("Нет данных для отправки!");
      }
      $ret = $this->service->prParseFilelnlpu($param);

      foreach ($items as $item) {
        if ($ext = SoapExt::get_wsse_node($item->ACTOR)) {
          $item->BINARY_SECURITY_TOKEN = $ext->get_binary_security_token();
          $item->SIGNATURE_VALUE = $ext->get_signature_value();
          $item->DIGEST_VALUE = $ext->get_digest_value();
        }
      }

      if (isset($ret['WSResult'])) {
        $result = new StdClass;
        if ($ret['WSResult']['STATUS'] == 1) {
          $dis->save();
          $items->save();
          $result->type = 'success';
          $result->blocks = $blocksSended;
          $result->ln_state = $dis->LN_STATE;
        } else {
          $result->type = 'error';
          $result->err_mess = '';
          $result->err_code = array();
          $row = $ret['WSResult']['INFO']['ROWSET']['ROW'];
          if ((count($row['ERRORS']['ERROR']) > 0) && (isset($row['ERRORS']['ERROR'][0]))) {
            foreach ($row['ERRORS']['ERROR'] as $vals) {
              $result->err_mess .= PHP_EOL . $vals['ERR_MESS'];
              $result->err_code[] = $vals['ERR_CODE'];
            }
          } else {
            $result->err_mess = $row['ERRORS']['ERROR']['ERR_MESS'];
            $result->err_code[] = $row['ERRORS']['ERROR']['ERR_CODE'];
          }
          //  error
        }
      }
      $this->service->soapDebug();
    } catch (SoapFault $fault) {
      $this->service->soapDebug();
      echo '<p>Error message: ', $fault->getMessage();
    }
    return $result;
  }

}