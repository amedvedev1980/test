<?php
// Version:1.0
// Тестовая версия. Скрипт подготовки данных для взаимодействия с сервисами.

Class MonitoringPrepare
{
  /**
   * @var DOMDocument
   */
  private $dom;
  /**
   * @var DOMXpath xp
   */
  private $xp;
  public static $simple = array('string', 'long', 'date', 'time', 'boolean', 'decimal');

  public $map;
  public $types;

  public function update_meta()
  {
    Sql::trans();
    try {
      $dicts = new DoRodVariant('*');
      if (
      $id = $dicts->add_from_xml(DOCA_ROOT . '/monitoring/prepare/nsi/F011.xml',
        'F011',
        'Классификатор типов документов, удостоверяющих личность')
      ) {
        $params = new DoRodParam("NAME_ENG='document_type_code'");
        foreach ($params as $param) {
          $param->FORM_FIELD_TYPE = 'select';
          $param->ID_DICT = $id;
          $param->DICT_CODE = 'F011';
          $param->DICT_TITLE = 'Классификатор типов документов, удостоверяющих личность';
        }
        $params->save();
      }
      Sql::commit();
    }
    catch (Exception $e){
      Sql::rollback();
      throw new Exception($e);
    }
  }

  /**
   * Парсим файлы wsdl и заполняем начальную структуру таблицы DO_ROD_PARAM
   */
  public function process_wsdl()
  {
    $dir = DOCA_ROOT . '/monitoring/prepare';
    $dh = opendir($dir);
    $files = array();
    while (($file = readdir($dh)) !== false) {
      if (is_file($dir . DIRECTORY_SEPARATOR . $file) && $file != '.' && $file != '..') {
        if (self::end_explode('.', $file) == 'wsdl') {
          $files[$file] = $dir . DIRECTORY_SEPARATOR . $file;
        }
      }
    }
    closedir($dh);
    sort($files);

    foreach ($files as $file => $fileName) {
      self::parse_wsdl($fileName);
      Sql::trans();
      try {
        self::save_db();
        Sql::commit();
      }
      catch (Exception $e){
        Sql::rollback();
        throw new Exception($e);
      }
    }

  }

  /**
   * Парсим указанный файл $fileName и заполняем массивы $this->map и $this->>types
   * @param $fileName
   * @param bool $is_request
   * @return bool|null
   */
  public function parse_wsdl($fileName, $is_request = true)
  {
    $result = null;
    unset($this->types);
    unset($this->map);
    if (is_file($fileName)) {
      $xml = file_get_contents($fileName);
      if ($result = self::process($xml, $is_request, $map, $types)) {
        $this->map = $map;
        $this->types = $types;
      }
    }
    return $result;
  }

  /**
   * Сохранение текущей структуры wsdl в базу данных
   */
  public function save_db()
  {
    self::save_map($this->map);
  }

  public function nationality()
  {
    Sql::trans();
    $dictName = 'rbOKIN_Nationality';
    $locale = setlocale(LC_ALL, 0);
    setlocale (LC_ALL, "ru_RU.UTF-8", 'ru_RU');
    try {
      $path = DOCA_ROOT . '/monitoring/prepare/meta/rbOKIN_Nationality.csv';
      if ($f = fopen($path, 'r')) {
        $dv = new DoRodVariant('*');
        if (($res = $dv->find('NAME', $dictName)) === null) {
          $dv->add();
          $dv->NAME = $dictName;
        }
        $dv->TYPE = 'unknown';
        $dv->DESCR = 'Справочник национальностей';
        $dv->save();

        $idVar = $dv->ID_VAR;
        $dvVals = $dv->dorodvariantvals();
        while (($row = fgetcsv($f)) !== false) {
          $code = $row[0];
          $name = $row[1];
          if ($code == 'code') continue;
          if (($res = $dvVals->find('CODE', $code)) === null) {
            $dvVals->add();
            $dvVals->ID_VAR = $idVar;
            $dvVals->CODE = $code;
            $dvVals->TITLE = $name;
            $dvVals->save();
          }

        }
      }
      Sql::commit();
    } catch (Exception $e) {
      setlocale(LC_ALL, $locale);
      Sql::rollback();
      throw new Exception($e);
    }
    setlocale(LC_ALL, $locale);
  }

  public function measure_revision()
  {
    Sql::trans();
    $measureDName = 'measuresList';
    $locale = setlocale(LC_ALL, 0);
    setlocale (LC_ALL, "ru_RU.UTF-8", 'ru_RU');
    try {
      $path = DOCA_ROOT . '/monitoring/prepare/meta/measures_list.csv';
      if ($f = fopen($path, 'r')) {
        $dv = new DoRodVariant('*');
        if (($res = $dv->find('NAME', $measureDName)) === null) {
          $dv->add();
          $dv->NAME = $measureDName;
        }
        $dv->TYPE = 'unknown';
        $dv->DESCR = 'Справочник мероприятий';
        $dv->save();

        $idVar = $dv->ID_VAR;
        $dvVals = $dv->dorodvariantvals();
        $parents = array();
        while (($row = fgetcsv($f)) !== false) {
          $type = $row[0];
          $code = $row[1];
          $title = $row[2];
          if ($type == 'name') continue;
          if (($res = $dvVals->find('CODE', $code)) === null) {
            if (!array_key_exists($type, $parents)) {
              $dvVals->add();
              $dvVals->ID_VAR = $idVar;
              $dvVals->TITLE = $type;
              $dvVals->save();
              $parents[$type] = $dvVals->ID_VAR_VALS;
            }
            $dvVals->add();
            $dvVals->ID_VAR = $idVar;
            $dvVals->ID_PARENT = $parents[$type];
            $dvVals->CODE = $code;
            $dvVals->TITLE = $title;
            $dvVals->save();
          }

        }
      }
      Sql::commit();
    } catch (Exception $e) {
      setlocale(LC_ALL, $locale);
      Sql::rollback();
      throw new Exception($e);
    }
    setlocale(LC_ALL, $locale);
  }

  public function dict_revision()
  {
    Sql::trans();
    $locale = setlocale(LC_ALL, 0);
    setlocale (LC_ALL, "ru_RU.UTF-8", 'ru_RU');
    try {
      $path = DOCA_ROOT . '/monitoring/prepare/meta/csv';
      $dicts = array();

      $f = fopen($path . '/index.csv', 'r');
      while (($row = fgetcsv($f)) !== false) {
        $name = substr(strstr($row[0], '.'), 1);
        $dicts[$name] = new StdClass;
        $dicts[$name]->nums = $row[1];
        $dicts[$name]->title = $row[2];
      }
      fclose($f);
      $dv = new DoRodVariant('*');
      $dh = opendir($path);
      while (($file = readdir($dh)) !== false) {
        if (is_file($path . DIRECTORY_SEPARATOR . $file) && $file != '.' && $file != '..' && $file != 'index.csv') {
          if (self::end_explode('.', $file) == 'csv') {
            $fileName = $path . DIRECTORY_SEPARATOR . $file;
            $name = explode('.', $file);
            $name = $name[1];
            if (!isset($dicts[$name])) continue;
            $f = fopen($fileName, 'r');
            $head = null;
            $idVar = null;
            while (($row = fgetcsv($f)) !== false) {
              if (!$head) {
                $head = array_flip($row);

                if (array_key_exists('code', $head)) {
                  $iCode = $head['code'];
                  $type = 'federal';
                } else {
                  $iCode = $head['regionalCode'];
                  $type = 'regional';
                }
                $iName = $head['name'];
                if (($res = $dv->find('NAME', $name)) === null) {
                  $dv->add();
                  $dv->NAME = $name;
                }
                $dv->TYPE = $type;
                $dv->DESCR = $dicts[$name]->title;
                $dv->save();
                $idVar = $dv->ID_VAR;
                $dvVals = $dv->dorodvariantvals();
              } else {
                $flag = ($res = $dvVals->find('VAL', $row[$iName])) === null;
                if ($flag || (!$flag && !$dvVals->CODE)) {
                  if ($flag) $dvVals->add();
                  $dvVals->ID_VAR = $idVar;
                  $dvVals->CODE = $row[$iCode];
                  $dvVals->TITLE = $row[$iName];
                  $dvVals->ID = $row[$head['ID']];
                  $dvVals->save();
                }
              }
            }
            fclose($f);
          }
        }
      }
      Sql::commit();
    } catch (Exception $e) {
      setlocale(LC_ALL, $locale);
      Sql::rollback();
      throw new Exception($e);
    }
    setlocale(LC_ALL, $locale);
  }

  public function dict_reload($name, $title = null)
  {
    $locale = setlocale(LC_ALL, 0);
    setlocale (LC_ALL, "ru_RU.UTF-8", 'ru_RU');
    Sql::trans();
    try {
      $fileName = DOCA_ROOT . '/monitoring/prepare/meta/csv/ent_risar.' . $name . '.csv';
      $f = fopen($fileName, 'r');
      $head = null;
      $idVar = null;
      while (($row = fgetcsv($f)) !== false) {
        if (!$head) {
          $head = array_flip($row);
          if (array_key_exists('code', $head)) {
            $iCode = $head['code'];
            $type = 'federal';
          } else {
            $iCode = $head['regionalCode'];
            $type = 'regional';
          }
          $iName = $head['name'];
          $dv = new DoRodVariant('*');
          if (($res = $dv->find('NAME', $name)) === null) {
            $dv->add();
            $dv->NAME = $name;
          }
          $dv->TYPE = $type;
          if ($title) {
            $dv->DESCR = $title;
          }
          $dv->save();
          $idVar = $dv->ID_VAR;
          $dvVals = $dv->dorodvariantvals();
        } else {
          $flag = ($res = $dvVals->find('ID', $row[$head['ID']])) === null;

          if ($flag || (!$flag && $dvVals->CODE != $row[$iCode])) {
            if ($flag) $dvVals->add();
            $dvVals->ID_VAR = $idVar;
            $dvVals->CODE = $row[$iCode];
            $dvVals->TITLE = $row[$iName];
            $dvVals->ID = $row[$head['ID']];
            $dvVals->save();
          }
        }
      }
      fclose($f);
      Sql::commit();
    } catch (Exception $e) {
      Sql::rollback();
      setlocale(LC_ALL, $locale);
      throw new Exception($e);
    }
    setlocale(LC_ALL, $locale);

  }

  public function fix_id_dict()
  {
    Sql::trans();
    try {
      $p = new DoRodParam('DICT_CODE IS NOT NULL AND ID_DICT IS NULL');
      /**
       * @var DoRodParam $item
       */
      foreach ($p as $item) {
        $idDict = DoRodVariant::get_id_by_name($item->DICT_CODE);
        $item->ID_DICT = $idDict;
        $item->save();
      }
      Sql::commit();
    }
    catch (Exception $e){
      Sql::rollback();
      throw new Exception($e);
    }
  }

  /**
   * Сохранение структуры <map> в базу данных
   * @param $map
   * @param null $idParent
   * @param null $parentName
   * @return int|null
   * @throws Exception
   */
  public function save_map($map, $idParent = null, $parentName = null)
  {
    $result = null;
    if (is_array($map)) {
      $param = new DoRodParam();
      $opers = array();
      $fors = array();
      foreach ($map as $name => $vals) {
        if (!is_object($vals)) {
          throw new Exception('неверный массив <map>, ожидается объект');
        }
        $param->add();
        $param->ID_PAR = null;
        $param->NAME_ENG = isset($vals->name) ? $vals->name : $name;
        $param->STRUCT_TYPE = $vals->tag;
        $param->ID_PARENT = $idParent;
        $param->ORDER_NUM = isset($vals->order) ? $vals->order : null;

        switch ($vals->tag) {
          case 'operation':
            $param->TYPE = null;
            $param->OBJECT_CALL = $vals->location;
            $param->DESCRIPTION = isset($vals->documentation) ? $vals->documentation : null;
            $param->TITLE = isset($vals->title) && !empty($vals->title) ? $vals->title : $param->DESCRIPTION;
            $param->VISIBLE = (int)$vals->visible;
            if (isset($vals->for) && !empty($vals->for)) {
              if (isset($opers[$vals->for])) {
                $param->FOR_ID_PAR = $opers[$vals->for];
              }
            }
            foreach ($vals->methods as $method) {
              $method = 'METHOD_' . strtoupper($method);
              $param->{$method} = 1;
            }
            $param->MULTI_MODE = isset($vals->multi) ? $vals->multi : null;
            $param->save();

            $idPar = $param->ID_PAR;

            if (isset($vals->for) && !empty($vals->for)) {
              if (!isset($opers[$vals->for])) {
                $fors[$vals->for] = $idPar;
              }
            } else {
              $opers[$param->NAME_ENG] = $idPar;
              if (isset($fors[$param->NAME_ENG])) {
                $_param = new DoRodParam('ID_PAR=?', $fors[$param->NAME_ENG]);
                $_param->FOR_ID_PAR = $idPar;
                $_param->save();
              }
            }

            // добавляем input и output секции для операции
            if (isset($vals->input) && isset($vals->output)) {
              $param->add();
              $param->ID_PARENT = $idPar;
              $param->NAME_ENG = 'input';
              $param->STRUCT_TYPE = 'message';
              $param->save();
              $idPar_input = $param->ID_PAR;

              self::save_map($vals->input, $idPar_input, null);
              $param->add();
              $param->ID_PARENT = $idPar;
              $param->NAME_ENG = 'output';
              $param->STRUCT_TYPE = 'message';
              $param->save();

              $idPar_output = $param->ID_PAR;
              self::save_map($vals->output, $idPar_output, null);
            } else {
              throw new Exception('неверный массив <map>, отсутсвует элемент input/output у операции ' . $name);
            }
            break;
          case 'input':
          case 'output':
            $param->STRUCT_TYPE = 'part';
            $param->TYPE = ($vals->type ? $vals->type : $vals->element);
            $param->save();
            $idPar = $param->ID_PAR;

            if ($vals->type) {
              $param->TYPE_ID = self::save_map($vals->typeRel, $idPar, $param->TYPE);
            } else {
              $param->TYPE_ID = self::save_map($vals->elementRel, $idPar, $param->TYPE);
            }
            $param->save();
            break;
          case 'element':
            $param->TYPE = ($vals->type ? $vals->type : $vals->element);
            if (is_null($vals->minOccurs)) {
              $param->REQUIRED = 1;
            }
            $param->save();
            $idPar = $param->ID_PAR;
            $text = '';
            if (isset($vals->annotation->documentation)) {
              $text = $vals->annotation->documentation;
              $param->DESCRIPTION = $text;
            }
            $selectType = null;
            // Это простой тип
            if ($vals->type && in_array($vals->type, self::$simple)) {
              $dict = null;
              $selectType = 'input';

              $varPtrn = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
              $pattern = "/\:var\s+({$varPtrn})/i";
              if (preg_match($pattern, $text, $matches)){
                $text = preg_replace($pattern, '', $text);
                $param->VAR_NAME = $matches[1];
              }

              $pattern = "/\:reflect\s+({$varPtrn})/i";
              if (preg_match($pattern, $text, $matches)){
                $text = preg_replace($pattern, '', $text);
                $param->REFLECT_TO = $matches[1];
              }

              $pattern = "/\:\s*hidden/i";
              if (preg_match($pattern, $text, $matches)){
                $text = preg_replace($pattern, '', $text);
                $param->VISIBLE = 0;
              }
              else
                $param->VISIBLE = 1;

              $pattern = "/\:\s*readonly/i";
              if (preg_match($pattern, $text, $matches)){
                $text = preg_replace($pattern, '', $text);
                $param->READONLY = 1;
              }
              else
                $param->READONLY = 0;

              if (preg_match('/^(.*)\:list\s(.*)$/iU', $text, $matches)) {
                $param->OBJECT_LIST = $matches[2];
                $param->DESCRIPTION = $matches[1];
                $param->TITLE = $matches[1];
                $selectType = 'select';
              }
              else {
                if (preg_match('/^(.*)\:object\s(.*)$/iU', $text, $matches)) {
                  $param->OBJECT_CALL = $matches[2];
                  $text = $matches[1];
                }

                if ($vals->type == 'boolean') {
                  $selectType = 'checkbox';
                  if (empty($param->TITLE)) {
                    $param->TITLE = $param->DESCRIPTION;
                  }
                } elseif ($vals->type == 'string' && isset($vals->maxLength) && $vals->maxLength > 1000) {
                  $selectType = 'textarea';
                } elseif ($text) {
                  $param->TITLE = $text;
                  $text = str_replace("\n", '', $text);
                  if (preg_match('/^(.*)(,.*МКБ-.*)$/', $text, $matches)) {
                    $param->TITLE = trim($matches[1]);
                    preg_match('/^.*(МКБ-.*)$/', $matches[2], $matches_0);
                    if (isset($matches_0[1])) {
                      $param->DICT_TITLE = $matches_0[1];
                    }
                  } elseif (preg_match('/^(.*)\:code\s(.*)$/U', $text, $matches)) {
                    $param->DICT_CODE = trim($matches[2]);
                    $param->TITLE = $matches[1];
                    $param->DESCRIPTION = $matches[1];
                    $param->DICT_FIELD = 'CODE';
                    $param->DICT_TABLE = 'DO_ROD_VARIANT_VALS';
                    $selectType = 'select';
                  } elseif (preg_match('/^(.*)\:title\s(.*)$/U', $text, $matches)) {
                    $param->DICT_CODE = trim($matches[2]);
                    $param->TITLE = $matches[1];
                    $param->DESCRIPTION = $matches[1];
                    $param->DICT_FIELD = 'TITLE';
                    $param->DICT_TABLE = 'DO_ROD_VARIANT_VALS';
                    $selectType = 'select';
                  } elseif (preg_match("/^([0-9а-яА-ЯёЁ\W\,]+)\s([a-zA-Z_]+\d*)$/U", $text, $matches)) {
                    if (isset($matches[2])) {
                      $dict = trim($matches[2]);
                      if ($dict != 'f003') {
                        $text = $matches[1];
                        $text = preg_replace('/\,\s?справ.*/', '', $text);
                        $text = preg_replace('/\,\s?код из справ.*/', '', $text);
                        $param->TITLE = $text;
                        $param->DICT_CODE = $dict;
                        $param->DICT_TITLE = $text;
                        $param->DICT_TABLE = 'DO_ROD_VARIANT_VALS';
                        $param->DICT_FIELD = 'TITLE';

                        $selectType = 'select';
                      }
                    }
                  }
                  elseif (preg_match('/(.*)Если (.*) - передать (\d+).*Если нет - (\d+)/', $text, $matches)) {
                    $param->TITLE = $matches[2];
                    $selectType = 'checkbox';
                    if (property_exists($vals, 'restriction')) {
                      $vals->restriction = null;
                    }
                  }
                  elseif (preg_match('/(.*): (\d+.*)/', $text, $matches)) {
                    $param->TITLE = $matches[1];
                    $param->DICT_TABLE = 'DO_ROD_VARIANT_VALS';
                    $param->DICT_FIELD = 'VAL';
                    $tmp = explode(',', $matches[2]);
                    $selectType = 'select';
                    if (count($tmp) == 2) {
                      $selectType = 'radio';  //  ##1
                    }
                    $vals->restriction = array();
                    foreach ($tmp as $val) {
                      $key = explode('-', $val);
                      if (count($key) == 2) {
                        $val = trim($key[0]);
                        $key = trim($key[1]);
                        $vals->restriction[$key] = $val;
                      }
                    }
                  }
                }
                if (isset($vals->maxLength) && is_numeric($vals->maxLength)) {
                  $param->TYPE_SIZE = $vals->maxLength;
                }
                if (isset($vals->restriction) && is_array($vals->restriction)) {

                  $dict = $dict ? $dict : $vals->name;

                  $param->STRUCT_TYPE = 'variant';
                  $dVar = new DoRodVariant('*');
                  $index = $dVar->find('NAME', $dict);
                  if ($index === null) {
                    $dVar->add();
                    $dVar->NAME = $dict;
                    $dVar->save();

                    $dVals = $dVar->dorodvariantvals();

                    foreach ($vals->restriction as $key => $val) {
                      $dVals->add();

                      $dVals->ID_VAR = $dVar->ID_VAR;
                      $dVals->VAL = $val;
                      if (!is_numeric($key)) {
                        $dVals->TITLE = $key;
                      }
                    }
                    $dVals->save();

                  }
                  $param->ID_DICT = $dVar->ID_VAR;
                  if ($selectType == 'input') {
                    $selectType = 'select';
                    $param->DICT_FIELD = 'VAL';
                    $param->DICT_TABLE = 'DO_ROD_VARIANT_VALS';
                  }
                }
              }
            } elseif ($vals->type) {
              $param->TYPE_ID = self::save_map($vals->typeRel, $idPar, $param->TYPE);
            } else {
              throw new Exception('So many elements?');
              $param->TYPE_ID = self::save_map($vals->elementRel, $idPar, $param->TYPE);
            }
            if (isset($vals->maxOccurs) && $vals->maxOccurs == 'unbounded') {
              $param->STRUCT_TYPE = 'array';
            }
            $param->FORM_FIELD_TYPE = $selectType;
            $param->save();
            break;
          case 'complexType':
            break;
        }
      }
    } elseif (is_object($map)) {
      switch ($map->tag) {
        case 'element':
          $param = new DoRodParam();
          $param->ID_PAR = null;
          $param->NAME_ENG = $parentName;
          $param->STRUCT_TYPE = $map->tag;
          if (isset($map->annotation->documentation)) {
            $text = $map->annotation->documentation;
            $param->DESCRIPTION = $text;
          }
          $param->save();
          $result = $idPar = $param->ID_PAR;
          if ($map->sequence) {
            self::save_map($map->sequence, $idPar, null);
          }
          break;
        case 'complexType':
          $param = new DoRodParam();
          $param->ID_PAR = null;
          $param->NAME_ENG = $parentName;
          $param->STRUCT_TYPE = $map->tag;
          if (isset($map->annotation->documentation)) {
            $text = $map->annotation->documentation;
            $text = str_replace("\n", '', $text);
            preg_match("/^([0-9a-zA-Z\W]*)([0-9а-яА-ЯёЁ\W]+)$/U", $text, $matches);
            if (isset($matches[2])) {
              $text = trim($matches[2]);
              if (!empty($text)) {
                $text = preg_replace('/\s{2,}/', '', $text);
                $param->DESCRIPTION = $text;
              }
            } else {
              $param->DESCRIPTION = $text;
            }
          }
          $param->save();
          $result = $idPar = $param->ID_PAR;
          if ($map->sequence) {
            self::save_map($map->sequence, $idPar, null);
          }
          break;
      }
    } else {
      throw new Exception('неверный массив <map>');
    }
    return $result;
  }

  private function dict_prepares()
  {
    $path = '//s:complexType/s:sequence/s:element[@maxOccurs="unbounded"]/s:simpleType/s:restriction/s:enumeration[1]';
    if ($items = self::get_nodes($path)) {
      for ($i = 0; $i < $items->length; $i++) {
        $item = $items->item($i);
        $complex = $item->parentNode->parentNode->parentNode->parentNode->parentNode;
        $target = $item->parentNode->parentNode->parentNode;
        $name = $complex->getAttribute('name');
        if (preg_match('/Array.*/', $name)) {
          if (!($res = self::get_nodes('s:annotation', $complex))) {
            $path = '//s:element[@type="s0:' . $name . '"]';
            if ($elements = self::get_nodes($path)) {
              $element = $elements->item(0);
              if ($doc = self::get_nodes('s:annotation/s:documentation', $element)) {
                $doc = $doc->item(0)->nodeValue;
                // create
                $annot = $this->dom->createElementNS('http://www.w3.org/2001/XMLSchema', 's:annotation');
                $docum = $this->dom->createElementNS('http://www.w3.org/2001/XMLSchema', 's:documentation', $doc);
                $annot->appendChild($docum);
                $target->appendChild($annot);
              }
            }
          }

        }
      }
    }
  }

  private function set_xp()
  {
    $this->xp = new DOMXPath($this->dom);
    $this->xp->registerNamespace('xmlns', 'http://schemas.xmlsoap.org/wsdl/');
    $this->xp->registerNamespace('xmlns:SOAP-ENC', 'http://schemas.xmlsoap.org/soap/encoding/');
    $this->xp->registerNamespace('xmlns:mime', 'http://schemas.xmlsoap.org/wsdl/mime/');
    $this->xp->registerNamespace('xmlns:s', 'http://www.w3.org/2001/XMLSchema');
    $this->xp->registerNamespace('xmlns:s0', 'http://tempuri.org');
    $this->xp->registerNamespace('xmlns:soap', 'http://schemas.xmlsoap.org/wsdl/soap/');
    $this->xp->registerNamespace('xmlns:wsdl', 'http://schemas.xmlsoap.org/wsdl/');
    $this->xp->registerNamespace('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  }

  /**
   * Парсим $xml и возвращаем массив операций $map и массив типов $types
   * Возвращаем количество операций или null
   * @param $xml - текст wsdl, который парсится
   * @param $is_request - true если wsdl описывает отправку данных
   * @param array $map - результирующий массив описания операций wsdl
   * @param array $types - результирующий массив типов описанных в wsdl
   * @return int|null - возвращаем количество операций
   * @throws Exception
   */
  private function process($xml, $is_request, &$map, &$types)
  {
    $this->dom = new DOMDocument('1.0', 'utf-8');
    $res = $this->dom->loadXML($xml);
    if (!$res) {
      throw new Exception('Не удалось распознать XML.');
    }

    self::set_xp();

    self::dict_prepares();

    self::set_xp();
    // составляем массив $map из wsdl операций и их частей
    // в массив $need записываем имена необходимых элементов и комплексных типов,
    // кроме простейших: string, long, boolean, date
    $map = array();
    $needC = array();
    $needEl = array();

    $wsdl = null;
    $path = '//xmlns:definitions/xmlns:service/xmlns:port/soap:address';
    if ($addr = self::get_nodes($path)) {
      $wsdl = $addr->item(0)->getAttribute('location');
    }
    if (!$wsdl) throw new Exception('Не найдент адрес сервиса!');

    $operations = self::get_operations();
    foreach ($operations as $operation) {
      $operationName = $operation->getAttribute('name');

      $map[$operationName] = new StdClass;
      $map[$operationName]->tag = 'operation';
      if ($documentation = self::get_nodes('xmlns:documentation', $operation)) {
        $map[$operationName]->documentation = $documentation->item(0)->textContent;
      }
      $map[$operationName]->input = array();
      $map[$operationName]->output = array();
      $map[$operationName]->location = $wsdl;
      $map[$operationName]->title = $operation->getAttribute('title');
      $map[$operationName]->for = $operation->getAttribute('for');
      $map[$operationName]->order = $operation->getAttribute('order');
      $map[$operationName]->multi = $operation->getAttribute('multi');
      $visible = $operation->getAttribute('visible');
      $visible = empty($visible) ? 'true' : $visible;
      $map[$operationName]->visible = filter_var($visible, FILTER_VALIDATE_BOOLEAN);
      $map[$operationName]->disabled = filter_var($operation->getAttribute('disabled'), FILTER_VALIDATE_BOOLEAN);

      $methods = $operation->getAttribute('method');
      $methods = empty($methods) ? 'add' : $methods;
      $methods = explode(',', $methods);
      $map[$operationName]->methods = $methods;

      foreach (array('input', 'output') as $opType) {
        if ($oPart = self::get_nodes('xmlns:' . $opType, $operation)) {
          $oPart = $oPart->item(0);
          if ($message = self::end_explode(':', $oPart->getAttribute('message'))) {
            if ($parts = self::get_nodes("//xmlns:definitions/xmlns:message[@name='{$message}']/xmlns:part")) {
              for ($p = 0; $p < $parts->length; $p++) {
                $part = $parts->item($p);
                $map[$operationName]->{$opType}[$p] = new StdClass;
                $mapPart = &$map[$operationName]->{$opType}[$p];
                $mapPart->tag = $opType;
                $mapPart->name = $part->getAttribute('name');
                $mapPart->type = self::end_explode(':', $part->getAttribute('type'));
                $mapPart->typeRel = null;
                if (empty($mapPart->type)) {
                  $mapPart->element = self::end_explode(':', $part->getAttribute('element'));
                  $mapPart->elementRel = null;
                  $needEl[$mapPart->element][] = &$mapPart;
                } else {
                  if (!in_array($mapPart->type, self::$simple)) {
                    $needC[$mapPart->type][] = &$mapPart;
                  }
                }
              }
            }
          }
        }
      }

    }

    $need = array();
    $need[] = $needEl;
    $need[] = $needC;

    // составляем массив комплексных типов
    $types = array();
    do {
    } while ($res = self::get_types($need, $types));

    return count($map) > 0 ? count($map) : null;
  }

  /**
   * Парсим типы из $this->xp в массив $types, и заполняем $map
   * возвращаем true(int > 0), если за проход были добавлены новые типы
   * если false, то значит парсить больше нечего
   * @param array $need
   * @param array $types
   * @return null
   * @throws Exception
   */
  private function get_types(array &$need, array &$types)
  {
    $result = null;
    foreach ($need as $nType => $needP) {
      foreach ($needP as $typeName => $recipients) {
        if (!isset($types[$nType]) || !array_key_exists($typeName, $types[$nType])) {
          $query = '//xmlns:definitions/xmlns:types/s:schema/';
          if ($nType) {
            $query .= "s:complexType[@name='{$typeName}']";
          } else {
            $query .= "s:element[@name='{$typeName}']/s:complexType";
          }

          if ($complexType = self::get_nodes($query)) {
            $complexType = $complexType->item(0);

            $types[$nType][$typeName] = new StdClass;
            if ($nType) {
              $types[$nType][$typeName]->tag = 'complexType';
            } else {
              $types[$nType][$typeName]->tag = 'element';
            }
            if ($documentation = self::get_nodes('s:annotation/s:documentation', $complexType)) {
              $types[$nType][$typeName]->annotation = new StdClass;
              $types[$nType][$typeName]->annotation->documentation = $documentation->item(0)->textContent;
            }
            $types[$nType][$typeName]->sequence = array();
            $sequence = &$types[$nType][$typeName]->sequence;

            if ($elements = self::get_nodes('s:sequence/s:element', $complexType)) {
              for ($i = 0; $i < $elements->length; $i++) {
                $element = $elements->item($i);
                $sequence[$i] = new StdClass;
                $sequence[$i]->tag = 'element';
                $sequence[$i]->name = $element->getAttribute('name');
                $sequence[$i]->type = self::end_explode(':', $element->getAttribute('type'));
                $sequence[$i]->typeRel = null;
                $minOccurs = $element->getAttribute('minOccurs');
                $sequence[$i]->minOccurs = ($minOccurs == '' ? null : $minOccurs);
                $sequence[$i]->maxOccurs = $element->getAttribute('maxOccurs');
                $sequence[$i]->nillable = $element->getAttribute('nillable');
                $sequence[$i]->dict = $element->getAttribute('dictName');
                $sequence[$i]->title = $element->getAttribute('title');

                if (empty($sequence[$i]->type)) {
                  if ($restriction = self::get_nodes('s:simpleType/s:restriction', $element)) {
                    $restriction = $restriction->item(0);
                    $sequence[$i]->type = self::end_explode(':', $restriction->getAttribute('base'));
                    if (!in_array($sequence[$i]->type, self::$simple)) {
                      throw new Exception('unknown error... code 001 ');
                    }
                    if ($maxLength = self::get_nodes('s:maxLength', $restriction)) {
                      $maxLength = $maxLength->item(0);
                      $sequence[$i]->maxLength = $maxLength->getAttribute('value');
                    }
                    if ($enumerations = self::get_nodes('s:enumeration', $restriction)) {
                      $sequence[$i]->restriction = array();
                      for ($j = 0; $j < $enumerations->length; $j++) {
                        $enumeration = $enumerations->item($j);
                        $sequence[$i]->restriction[] = $enumeration->getAttribute('value');
                      }
                    }
                  }
                }

                if ($documentation = self::get_nodes('s:annotation/s:documentation', $element)) {
                  $sequence[$i]->annotation = new StdClass;
                  $sequence[$i]->annotation->documentation = $documentation->item(0)->textContent;
                }
                if (!in_array($sequence[$i]->type, self::$simple)) {
                  $need[1][$sequence[$i]->type][] = &$sequence[$i];
                }
              }
            }
          } else {
            throw new Exception('ошибка обработки...');
          }
          $result++;
        }
        foreach ($recipients as $recipient) {
          if ($nType) {
            $recipient->typeRel = &$types[$nType][$typeName];
          } else {
            $recipient->elementRel = &$types[$nType][$typeName];
          }
        }
      }
    }
    return $result;
  }

  /**
   * Возвращаем все команды(operation) текущего wsdl в виде массива DOMElement
   * @return DOMElement[]|DOMElement
   * @throws Exception
   */
  private function get_operations()
  {
    $result = array();
    $query = '//xmlns:definitions/xmlns:portType/xmlns:operation';
    if ($items = self::get_nodes($query)) {
      for ($i = 0; $i < $items->length; $i++) {
        $result[$i] = $items->item($i);
      }
    } else {
      throw new Exception('no operations...');
    }
    return $result;
  }

  /**
   * Выполняем XPath запрос, если данных нет, то возвращаем null
   * @param $query
   * @param $parent
   * @return DOMNodeList
   */
  private function get_nodes($query, $parent = null)
  {
    $result = null;
    if ($parent) {
      $xQuery = $this->xp->query($query, $parent);
    } else {
      $xQuery = $this->xp->query($query);
    }
    if ($xQuery->length) {
      $result = $xQuery;
    }
    return $result;
  }

  /**
   * Возвращаем последний элемент после разбития по разделителю delimiter
   * @param $delimiter
   * @param $str
   * @return mixed
   */
  private function end_explode($delimiter, $str)
  {
    $tmp = explode($delimiter, $str);
    return end($tmp);
  }
}